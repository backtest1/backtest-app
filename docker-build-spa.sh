#! /usr/bin/env bash
set -e

REGISTRY="registry.gitlab.com/softsquare_dpu/registry"
IMAGE_NAME="sss-front"
COMMIT_SHORT_SHA="$(git rev-parse --short HEAD)"

# URL sss.ssru.ac.th
# cd Web/spa;
# npm ci
# npm run build-docker
# cd ../..

docker build --rm -f "Web/spa/docker/Dockerfile" -t ${IMAGE_NAME} \
-t ${REGISTRY}/${IMAGE_NAME} \
-t ${IMAGE_NAME}:${COMMIT_SHORT_SHA} \
-t ${REGISTRY}/${IMAGE_NAME}:${COMMIT_SHORT_SHA} \
Web/spa
# -t ${REGISTRY}/${IMAGE_NAME}:${TAG_DATETIME} \
# -t ${IMAGE_NAME}:${TAG_DATETIME} 