FROM node:14-alpine as base

LABEL MAINTAINER siritas@gmail.com

RUN echo fs.inotify.max_user_watches=524288 >> /etc/sysctl.conf
RUN apk add --update git bash openssh python && \
  rm -rf /var/lib/apt/lists/* && \
  rm /var/cache/apk/*

ENV NPM_CONFIG_LOGLEVEL info
RUN npm install -g @angular/cli

FROM base as builder

ARG TARGET_ENV
ENV TARGET_ENV_CMD=${TARGET_ENV}

WORKDIR /source

COPY package*.json ./

ENV NPM_COMFIG_AUDIT=false
ENV NPM_COMFIG_PROGRESS=false
ENV NODE_OPTIONS="--max-old-space-size=8192"

RUN npm ci --loglevel=error

WORKDIR /

RUN mkdir ng-app&& \
  mv /source/node_modules /ng-app/node_modules

WORKDIR /ng-app

COPY . ./

RUN node timestamp.js

RUN npm run ${TARGET_ENV_CMD}

RUN npm run fix-ngsw

FROM caddy:2.0.0-alpine as runtime

COPY docker/Caddyfile /etc/caddy/Caddyfile
COPY --from=builder /ng-app/dist/spa/ /usr/share/caddy/
