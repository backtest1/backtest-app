import { version } from 'src/version';

export const environment = {
  production: true,
  apiUrl: '/api/',
  authUrl: 'https://idp-slcm.dpu.ac.th',
  reportUrl: 'https://rpt-slcm.dpu.ac.th/report',
  timeStamp: version.timeStamp,
};
