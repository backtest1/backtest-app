import { version } from 'src/version';

// DPU UAT


export const environment = {
  production: true,
  apiUrl: '/api/',
  authUrl: 'https://id.slcm.dpu.ac.th',
  reportUrl: 'https://rpt.slcm.dpu.ac.th/report',
  timeStamp: version.timeStamp,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
