import { version } from 'src/version';

// # to remove use .test.
export const environment = {
  production: true,
  apiUrl: '/api/',
  authUrl: 'https://idpdpu.softsquare.ga',
  reportUrl: 'https://rptdpu.softsquare.ga/report',
  timeStamp: version.timeStamp,
};
