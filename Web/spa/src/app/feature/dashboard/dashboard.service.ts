import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface ApprovePermission {
  ApproveContract: boolean,
  ApprovePayment: boolean
}

@Injectable()
export class DashboardService {

  constructor(private http: HttpClient) { }

  getMaster(page) {
    return this.http.get<any>('dashboard/master/' + page);
  }

  getDependencyMaster(name, params) {
    return this.http.get<any>('dashboard/dependency', { params: Object.assign({ name: name }, params) });
  }

  getEnrollments(search: any, page: any) {
    const filter = Object.assign(search, page);
    filter.sort = page.sort || 'enCode'
    return this.http.get<any>('dashboard', { params: filter });
  }

  getEnrollmentsNoReport(search: any, page: any) {
    const filter = Object.assign(search, page);
    filter.sort = page.sort || 'enCode'
    return this.http.get<any>('dashboard/noReport', { params: filter });
  }

//   getGenerateData(params) {
//     return this.http.get<Gen[]>('endt06/genData', { params: params } );
// }

  getCharts(params) {
    return this.http.get<any>('dashboard/charts', { params: params } );
  }

  getStudentsSearch(keyword, year, semester, calendarId, facCode, programCode, enCode, form) {
    return this.http.get<any>('dashboard/auto', {
      params: {
        name: 'student-search',
        keyword: keyword,
        year:year,
        semester:semester,
        calendarId:calendarId,
        facCode:facCode,
        programCode:programCode,
        enCode: enCode,
        form: form
      }
    });
  }
  
}
