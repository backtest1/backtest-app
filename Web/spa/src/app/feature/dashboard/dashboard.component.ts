import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { FormBuilder, FormGroup, AbstractControl, Validators, MaxLengthValidator } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Page, SubscriptionDisposer, FormUtilService } from '@app/shared';
import { Subscription, interval, merge } from 'rxjs';
import { finalize, takeUntil, map, count } from 'rxjs/operators';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import * as Chart from 'chart.js';
import { max } from 'date-fns';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends SubscriptionDisposer implements OnInit {
  private updateSubscription: Subscription;
  
  page = new Page();
  pageNoReport = new Page();
  master = { 
    years:[], semesters:[],facs:[],
    enrollments:[],calendars:[],programs:[],
    autoRefreshRate:null,
    enrollmentDashBoard:null,
    typeLevel:null,
    statuses:[],
    retrospectiveYears:[],
    color:[]
  }
  dirty: boolean = false;
  newDataFac =  [];
  newDataPro =  [];
  years = [];
  semesters = [];
  calendars = [];
  programs = [];
  ddlEnrollmentNoPayment = [];

  yearsNoReport = [];
  semestersNoReport = [];
  programsNoReport = [];
  calendarsNoReport = [];
  ddlEnrollmentNoReport = [];

  enrollmentNoPayment = [];
  enrollmentNoReport = [];
  autoRefreshRate: number = 60000;
  horizonBarChartType: ChartType = 'horizontalBar';
  verticalBarChartType: ChartType = 'bar';

  newStudentBarChartLabels: Label[] = [];
  newStudentBarChartData: ChartDataSets[] = [];
  newStudentBarChartColors : Color[] = [];
  graphNewStudentForm: FormGroup;
  yearGraph = [];
  newStudentBarChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    animation: {
      onComplete: function () {
        var ctx = this.chart.ctx;
        ctx.textAlign = "center";
        ctx.textBaseline = "center";
        var chart = this;
        var datasets = this.config.data.datasets;

        datasets.forEach(function (dataset: Array<any>, i: number) {
          ctx.font = "bold 12px Gotham, Helvetica Neue, sans-serif";
          ctx.fillStyle = "Black";
          ctx.stoke
          chart.getDatasetMeta(i).data.forEach(function (p: any, j: any) {
            var barWidth = p._model.x - p._model.base;
            var centerX = p._model.base + barWidth / 2;
            if (datasets[i].data[j] > 0) {
              ctx.fillText(datasets[i].data[j], centerX, p._model.y + 5);
            }
          });
        });
      }
    },
    legend: {
      position: "bottom",
      align:'center',
      labels: {
        boxWidth: 12
      }
    },
    plugins: {
      mode: 'single',
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        }
      }
    },
    scales: {
      yAxes: [{
          ticks: {
              min: 0,
          },
      }],
      xAxes: [{
      }]
    },
    layout: {
      padding: {
          left: 10,
          right: 10,
          top: 10,
          bottom: 0
      }
    }
  };

  facBarChartLabels: Label[] = [];
  facBarChartData: ChartDataSets[] = [];
  facBarChartColors : Color[] = [];
  facStudentForm: FormGroup;
  facBarChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    animation: {
      onComplete: function () {
        var ctx = this.chart.ctx;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        var chart = this;
        var datasets = this.config.data.datasets;

        datasets.forEach(function (dataset: Array<any>, i: number) {
          ctx.font = "bold 12px Gotham, Helvetica Neue, sans-serif";
          ctx.fillStyle = "Black";
          chart.getDatasetMeta(i).data.forEach(function (p: any, j: any) {
            if(datasets[i].data[j] > 0) 
              ctx.fillText(datasets[i].data[j], p._model.x, p._model.y - 5);
          });

        });
      }
    },
    legend: {
      position: "bottom",
      align:'center',
      labels: {
        boxWidth: 12
      }
    },
    plugins: {
      enabled: true,
      mode: 'single',
      datalabels: {
        anchor: 'end',
        align:'end',
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        }
      }
    },
    scales: {
      yAxes: [{
        display: true,
        ticks: {
          min: 0
        },
      }],
      xAxes: [{
        ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
      }],
    },
    layout: {
      padding: {
          left: 0,
          right: 0,
          top: 20,
          bottom: 0
      }
    }
  };

  programBarChartLabels: Label[] = [];
  programBarChartData: ChartDataSets[] = [];
  programBarChartColors : Color[] = [];
  programStudentForm: FormGroup;
  programBarChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    animation: {
      onComplete: function () {
        var ctx = this.chart.ctx;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        var chart = this;
        var datasets = this.config.data.datasets;

        datasets.forEach(function (dataset: Array<any>, i: number) {
          ctx.font = "bold 12px Gotham, Helvetica Neue, sans-serif";
          ctx.fillStyle = "Black";
          chart.getDatasetMeta(i).data.forEach(function (p: any, j: any) {
            if(datasets[i].data[j] > 0) 
              ctx.fillText(datasets[i].data[j], p._model.x, p._model.y - 5);
          });

        });
      }
    },
    legend: {
      position: 'bottom',
      align:'center',
      labels: {
        boxWidth: 12
      }
    },
    plugins: {
      enabled: true,
      mode: 'single',
      datalabels: {
        anchor: 'end',
        align:'end',
        font: {
          size: 20,
        }
      }
    },
    scales: {
      yAxes: [{
        display: true,
        ticks: {
          min: 0
        },
      }],
      xAxes: [{
        ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
      }],
    },
    layout: {
      padding: {
          left: 0,
          right: 0,
          top: 20,
          bottom: 0
      }
    }
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public util: FormUtilService,
    private ds: DashboardService,
    private cdRef: ChangeDetectorRef
  ) { super()  }

  ngOnInit() {
    this.createsearchGraphNewStudentForm();
    this.createSearchFacStudentForm();
    this.createSearchProgramStudentForm();
    this.route.data.subscribe((data) => {
      this.master = data.dashboard.master;
    });

    if(this.master.enrollmentDashBoard){
      this.years = this.master.years;
      this.yearGraph = this.master.years;

      this.graphNewStudentForm.controls.year.setValue(new Date().getFullYear()+543, {emitEvent: false});
      this.facStudentForm.controls.year.setValue(new Date().getFullYear()+543, {emitEvent: false});
      this.programStudentForm.controls.year.setValue(new Date().getFullYear()+543, {emitEvent: false});

      this.updateSubscription = interval(this.autoRefreshRate).subscribe(
        (val) => {
            if (this.router.url != undefined && this.router.url === '/dashboard') {
            }
          }
      );
    }


  }

  createsearchGraphNewStudentForm(){
    this.graphNewStudentForm = this.fb.group({
      year:[null,[Validators.required]],
      retrospectiveYear:[null],
      typeLevel:[null],
      status:[null,[Validators.required]],
    })

  }

  searchCharts() {
    this.util.markFormGroupTouched(this.graphNewStudentForm);
    if (this.graphNewStudentForm.invalid) {
      this.newStudentBarChartLabels = [];
      this.newStudentBarChartData = [];
      return;
    }  
    const param = Object.assign(this.graphNewStudentForm.value);
    param.lineGraph = false;
    param.graphType = 'horizontalBar';
    this.ds.getCharts(param)
      .pipe(finalize(() => { }))
      .subscribe(res => {
        this.newStudentBarChartLabels = res.newStudentLabel;
        this.newStudentBarChartData = res.newStudentData;
        //this.newStudentBarChartColors = [{ backgroundColor: res.newStudentColorData.map(co => co.color )}];
        
          
      });
  }

  createSearchFacStudentForm(){
    this.facStudentForm = this.fb.group({
      year:[null,[Validators.required]],
      retrospectiveYear:[null],
      typeLevel:[null],
      status:[null,[Validators.required]],
    })
  }

  searchFacBarCharts(){
    this.util.markFormGroupTouched(this.facStudentForm);
    if (this.facStudentForm.invalid) {
      this.facBarChartLabels = [];
      this.facBarChartData = [];
      return;
    }  
    const param = Object.assign(this.facStudentForm.value);
    param.lineGraph = true;
    param.graphType = 'verticalBar1';
    this.ds.getCharts(param)
      .pipe(finalize(() => { }))
      .subscribe(res => {
        let dataFac ;
        this.facBarChartLabels = res.studenFactLabel;
        const loop = res.studentFacData.length;
        for(let i=1 ; i <= loop ; i++){ //1,2,3,4
          res.studentFacData[i-1].backgroundColor = this.master.color[loop - i];
          res.studentFacData[i-1].hoverBackgroundColor = res.studentFacData[i-1].backgroundColor.backgroundColor;
          res.studentFacData[i-1].borderColor = res.studentFacData[i-1].backgroundColor.backgroundColor;  
          dataFac = Object.assign(res.studentFacData[i-1],res.studentFacData[i-1].backgroundColor);
          this.newDataFac.push(dataFac);
          
        }
        //console.log(this.newDataFac);
        this.facBarChartData = this.newDataFac;
        this.newDataFac = [];
      });
  }

  createSearchProgramStudentForm(){
    this.programStudentForm = this.fb.group({
      year:[null,[Validators.required]],
      retrospectiveYear:[null],
      typeLevel:[null],
      status:[null,[Validators.required]],
      facCode:[null,[Validators.required]],
    })
  }

  searchProgramBarCharts(){
    this.util.markFormGroupTouched(this.programStudentForm);
    if (this.programStudentForm.invalid) {
      this.programBarChartLabels = [];
      this.programBarChartData = [];
      return;
    }  
    const param = Object.assign(this.programStudentForm.value);
    param.lineGraph = true;
    param.graphType = 'verticalBar2';
    this.ds.getCharts(param)
      .pipe(finalize(() => { }))
      .subscribe(res => {
        let dataPro ;
        this.programBarChartLabels = res.studentProgramLabel;
        const loop = res.studentProgramData.length;
        for(let i=1 ; i <= loop ; i++){ //1,2,3,4
          res.studentProgramData[i-1].backgroundColor = this.master.color[loop - i];
          res.studentProgramData[i-1].hoverBackgroundColor = res.studentProgramData[i-1].backgroundColor.backgroundColor;
          res.studentProgramData[i-1].borderColor = res.studentProgramData[i-1].backgroundColor.backgroundColor;
          dataPro = Object.assign(res.studentProgramData[i-1],res.studentProgramData[i-1].backgroundColor);
          this.newDataPro.push(dataPro);
        }
        console.log(this.newDataPro);
        this.programBarChartData = this.newDataPro;
        this.newDataPro = [];
      });
  }

  programEvent(event){
    console.log(event);
  }


  lastestValue(formControl: AbstractControl, master: any[]) {
    formControl.setValue(
      Math.max.apply(
        Math,
        master.map((x) => x.value)
      )
    );
  }

}
