import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { DashboardService } from './dashboard.service';
import { map } from 'rxjs/operators';

@Injectable()
export class DashboardResolver implements Resolve<any> {
    constructor(private ds: DashboardService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const master = this.ds.getMaster("search");    
        return forkJoin(
            master,
        ).pipe(map((result) => {
            let master = result[0];
            return { master }
        }))
    }
}