import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseList, BaseService } from '@app/shared';

export interface DbBuilding {
  buildingId: number,
  companyCode: string,
  facCode: string,
  buildingCode: string,
  buildingNameTha: string,
  buildingNameEng: string,
  active: boolean,
  rowVersion: number

}
export interface DbBuildingDetail {
  buildingId: number,
  companyCode: string,
  facCode: string,
  buildingCode: string,
  buildingNameTha: string,
  buildingNameEng: string,
  active: boolean,
  rowVersion: number
  dbRoom: DbRoom[];
  dbPrivilegeBuilding: DbPrivilegeBuilding[];
  responsibleAGC: string;
  divCode: string;
}
export class DbRoom extends BaseList {
  roomId: number;
  buildingId: number;
  roomNo: string;
  roomNameTha: string;
  roomNameEng: string;
  floorNo: number;
  capacity: number;
  roomType: string;
  examRoom: boolean;
  active: boolean;
  roomDDL: any[];
  responsibleAGC: string;
  seatsRoomUsed: number;
  rowsNumberExam: number;
  rowsNumberTest: number;
  sumRowsNumberExam: number;
  committeeNumber: number;
  roomArea: number;
  costCenterCode: string;
  costHour: number;
  facCode: string;
  divCode: string;
}
export class DbPrivilegeBuilding extends BaseList {
  privilegeBuildingId: number;
  buildingId: number;
  divCode: string;
  active: boolean;
}

@Injectable()
export class Dbrt17Service extends BaseService {

  constructor(private http: HttpClient) { super() }

  getBuilding(search: string, page: any) {
    const filter = Object.assign({ keyword: search }, page);
    filter.sort = page.sort || 'codeLength,buildingCode';
    return this.http.get<any>('dbrt17', { params: filter });
  }

  getBuildingDetail(code) {
    return this.http.get<DbBuilding>('dbrt17/detail', { params: { buildingId: code } });
  }

  save(status: any, detail: DbBuildingDetail, dbRoomDelete: DbRoom[], dbPrivilegeBuildingDelete: DbPrivilegeBuilding[]) {
    const groupform = Object.assign({}, detail, status);

    if (groupform.rowVersion) {
      groupform.dbRoom = this.prepareSaveList(groupform.dbRoom, dbRoomDelete);
      groupform.dbPrivilegeBuilding = this.prepareSaveList(groupform.dbPrivilegeBuilding, dbPrivilegeBuildingDelete);
      groupform.dbRoom.forEach(element => {
        if (element.costCenterCode == 'null') {
          element.costCenterCode = null;
        }
      });
      return this.http.put('dbrt17', groupform);
    }
    else {
      groupform.dbRoom.forEach(element => {
        if (element.costCenterCode == 'null') {
          element.costCenterCode = null;
        }
      });
      return this.http.post('dbrt17', groupform);
    }
  }

  getRoom(code) {
    return this.http.get<DbBuildingDetail>('dbrt17/detail', { params: { buildingId: code } });
  }
  getMaster() {
    return this.http.get<any>('dbrt17/master');
  }

  delete(id, version) {
    return this.http.delete('dbrt17', { params: { buildingId: id, rowVersion: version } })
  }

}
