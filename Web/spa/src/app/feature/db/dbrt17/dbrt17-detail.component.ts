import { element } from '@angular/core/src/render3';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Dbrt17Service, DbBuilding, DbBuildingDetail, DbRoom, DbPrivilegeBuilding } from './dbrt17.service';
import { FormUtilService, ModalService, RowState, Page } from '@app/shared';
import { MessageService } from '@app/core';
import { finalize } from 'rxjs/operators';
import { log } from 'console';

@Component({
  selector: 'app-dbrt17-detail',
  templateUrl: './dbrt17-detail.component.html',
  styleUrls: ['./dbrt17-detail.component.scss']
})
export class Dbrt17DetailComponent implements OnInit {

  dbrt17Form: FormGroup;
  groupForm: FormGroup;
  building: DbBuilding = {} as DbBuilding;
  submited: boolean = false;
  saving: boolean = false;
  state: Observable<object>;
  detail: DbBuildingDetail = {} as DbBuildingDetail;
  dbRoomDelete: DbRoom[] = [];
  dbPrivilegeBuildingDelete: DbPrivilegeBuilding[] = [];
  disBtnSave: boolean;
  masterRoom: [];
  page = new Page();
  min: number;
  max: number;
  master = {
    facCode: [],
    roomType: [],
    privilegeBuilding: [],
    divCode: [],
    responsibleAGC: [],
    costCenterCode: []
  };
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public util: FormUtilService,
    private ms: MessageService,
    private modal: ModalService,
    private cs: Dbrt17Service
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.page.size = 200;
    this.route.data.subscribe((data) => {
      this.detail = data.dbrt17.detail;
      this.master = data.dbrt17.master;
      if (this.detail.facCode != null) {
        this.master.facCode = this.util.getActive(data.dbrt17.master.facCode, this.detail.facCode);
      } else {
        this.master.facCode = this.util.getActive(data.dbrt17.master.facCode, null);
      }
      this.masterRoom = data.dbrt17.master.roomType;
      this.rebuildForm();
      this.changeBuild();
    });
  }

  rebuildForm() {
    this.dbrt17Form.markAsPristine();
    if (this.detail.buildingId) {
      this.dbrt17Form.patchValue(this.detail);
      this.detail.dbRoom.map(detail => detail.form = this.createRoomForm(detail))
      this.detail.dbPrivilegeBuilding.map(detail => detail.form = this.createPrivilegeBuildingForm(detail))
      this.dbrt17Form.controls.buildingCode.disable();
    }
  }

  createForm() {
    this.dbrt17Form = this.fb.group({
      buildingCode: [null, [Validators.required, Validators.maxLength(10)]],
      facCode: [null, [Validators.required, Validators.maxLength(10)]],
      buildingNameTha: [null, [Validators.required, Validators.maxLength(100)]],
      buildingNameEng: [null, [Validators.maxLength(100)]],
      responsibleAGC: 'F',
      divCode: [null, [Validators.required, Validators.maxLength(20)]],
      active: true
    });
  }
  createRoomForm(dbRoom: DbRoom) {
    const fg = this.fb.group({
      roomNo: [null, [Validators.required, Validators.maxLength(10)]],
      roomNameTha: [null, [Validators.maxLength(100)]],
      roomNameEng: [null, [Validators.maxLength(100)]],
      facCode: [null, [Validators.required, Validators.maxLength(20)]],
      floorNo: [null, Validators.required,],
      roomType: [null, [Validators.required, Validators.maxLength(20)]],
      responsibleAGC: 'F',
      seatsRoomUsed: [null, [Validators.min(0), Validators.max(5000)]],
      rowsNumberExam: [null, [Validators.min(0), Validators.max(5000)]],
      rowsNumberTest: [null, [Validators.min(0), Validators.max(5000)]],
      sumRowsNumberExam: null,
      committeeNumber: [null, [Validators.min(0), Validators.max(5000)]],
      roomArea: [null, [Validators.min(0), Validators.max(5000)]],
      costCenterCode: null,
      costHour: null,
      divCode: [null, [Validators.required, Validators.maxLength(20)]],
      examRoom: false,
      active: true,
      assigned: false
    });

    fg.controls.costCenterCode.valueChanges.subscribe(value => {
      if (value == null) {
        fg.controls.costCenterCode.setValue(value = 'null')
      }
    })

    fg.controls.rowsNumberExam.valueChanges.subscribe(value => {
      if (value != null) {
        this.min = null;
        this.max = null;

        this.min = ((fg.controls.rowsNumberExam.value - 1) * fg.controls.rowsNumberTest.value) + 1;
        this.max = fg.controls.rowsNumberExam.value * fg.controls.rowsNumberTest.value;
        if(fg.controls.examRoom.value == true){
          fg.controls.rowsNumberExam.setValidators([Validators.required]);
          fg.controls.rowsNumberTest.setValidators([Validators.required]);
          fg.controls.sumRowsNumberExam.setValidators(null);
          fg.controls.sumRowsNumberExam.setValidators([Validators.required, Validators.min(this.min), Validators.max(this.max)]);
          fg.controls.committeeNumber.setValidators([Validators.required]);
        }
        if(fg.controls.examRoom.value == false){
          fg.controls.sumRowsNumberExam.setValidators([Validators.min(this.min), Validators.max(this.max)]);
        }
        fg.controls.sumRowsNumberExam.setValue(value * fg.controls.rowsNumberTest.value)
      }
    })

    fg.controls.rowsNumberTest.valueChanges.subscribe(value => {
      if (value != null) {
        this.min = null;
        this.max = null;

        this.min = ((fg.controls.rowsNumberExam.value - 1) * fg.controls.rowsNumberTest.value) + 1;
        this.max = fg.controls.rowsNumberExam.value * fg.controls.rowsNumberTest.value;
        if(fg.controls.examRoom.value == true){
          fg.controls.rowsNumberExam.setValidators([Validators.required]);
          fg.controls.rowsNumberTest.setValidators([Validators.required]);
          fg.controls.sumRowsNumberExam.setValidators([Validators.required, Validators.min(this.min), Validators.max(this.max)]);
          fg.controls.committeeNumber.setValidators([Validators.required]);
        }
        if(fg.controls.examRoom.value == false){
          fg.controls.sumRowsNumberExam.setValidators([Validators.min(this.min), Validators.max(this.max)]);
        }
        fg.controls.sumRowsNumberExam.setValue(value * fg.controls.rowsNumberExam.value)
      }
    })

    fg.valueChanges.subscribe((control) => {
      if (control.assigned && dbRoom.rowState == RowState.Normal) {
        dbRoom.rowState = RowState.Edit;
      }
    })
    fg.controls.responsibleAGC.valueChanges.subscribe(value => {
      if (value == 'F') {
        fg.controls.divCode.setValue(null);
        fg.controls.divCode.disable();
        fg.controls.facCode.enable();

      } else if (value == 'D') {
        fg.controls.facCode.setValue(null);
        fg.controls.divCode.enable();
        fg.controls.facCode.disable();
      }
    });

    fg.patchValue(dbRoom);
    if (dbRoom.roomType) {
      dbRoom.roomDDL = this.util.getActive(this.masterRoom, dbRoom.roomType);
    }
    if (fg.controls.responsibleAGC.value == 'F') {
      fg.controls.divCode.setValue(null);
      fg.controls.divCode.disable();
      fg.controls.facCode.enable();

    } else if (fg.controls.responsibleAGC.value == 'D') {
      fg.controls.facCode.setValue(null);
      fg.controls.divCode.enable();
      fg.controls.facCode.disable();
    }
    fg.controls.assigned.setValue(true, { emitEvent: false });
    fg.controls.costCenterCode.disable();
    fg.controls.costHour.disable();

    fg.controls.examRoom.valueChanges.subscribe(value => {
      if (value == true) {
        fg.controls.rowsNumberExam.setValidators([Validators.required]);
        fg.controls.rowsNumberTest.setValidators([Validators.required]);
        fg.controls.sumRowsNumberExam.setValidators([Validators.required, Validators.min(this.min), Validators.max(this.max)]);
        fg.controls.committeeNumber.setValidators([Validators.required]);
      }
      if (value == false) {
        fg.controls.rowsNumberExam.setValidators([]);
        fg.controls.rowsNumberTest.setValidators([]);
        fg.controls.sumRowsNumberExam.setValidators([Validators.min(this.min), Validators.max(this.max)]);
        fg.controls.committeeNumber.setValidators([]);
      }
    })
    if(fg.controls.examRoom.value){
      fg.controls.rowsNumberExam.setValidators([Validators.required]);
      fg.controls.rowsNumberTest.setValidators([Validators.required]);
      fg.controls.sumRowsNumberExam.setValidators([Validators.required, Validators.min(this.min), Validators.max(this.max)]);
      fg.controls.committeeNumber.setValidators([Validators.required]);
    } else {
      fg.controls.rowsNumberExam.setValidators([]);
      fg.controls.rowsNumberTest.setValidators([]);
      fg.controls.sumRowsNumberExam.setValidators([Validators.min(this.min), Validators.max(this.max)]);
      fg.controls.committeeNumber.setValidators([]);
    }
    return fg;
  }
  addRoom() {
    if (this.detail.buildingId != null) {
      const newGroup = new DbRoom();
      newGroup.roomDDL = this.util.getActive(this.masterRoom, null);
      newGroup.form = this.createRoomForm(newGroup);
      this.detail.dbRoom.push(newGroup);
      this.detail.dbRoom = [...this.detail.dbRoom];
      this.dbrt17Form.markAsDirty();
    } else {
      this.ms.warning("message.STD00035");
      return;
    }
  }
  removeRoom(dbRoom: DbRoom) {
    if (dbRoom.rowState !== RowState.Add) {
      dbRoom.rowState = RowState.Delete;
      this.dbRoomDelete.push(dbRoom);
    }
    this.detail.dbRoom = this.detail.dbRoom.filter(item => item.guid !== dbRoom.guid);
    this.dbrt17Form.markAsDirty();
  }

  createPrivilegeBuildingForm(dbPrivilegeBuilding: DbPrivilegeBuilding) {
    const fg = this.fb.group({
      divCode: [null, [Validators.required, Validators.maxLength(20)]],
      active: true,
      assigned: false
    });
    fg.valueChanges.subscribe((control) => {
      if (control.assigned && dbPrivilegeBuilding.rowState == RowState.Normal) {
        dbPrivilegeBuilding.rowState = RowState.Edit;
      }
    })
    fg.patchValue(dbPrivilegeBuilding);
    fg.controls.assigned.setValue(true, { emitEvent: false });
    return fg;
  }
  addPrivilegeBuilding() {
    if (this.detail.buildingId != null) {
      const newGroup = new DbPrivilegeBuilding();
      newGroup.form = this.createPrivilegeBuildingForm(newGroup);
      this.detail.dbPrivilegeBuilding.push(newGroup);
      this.detail.dbPrivilegeBuilding = [...this.detail.dbPrivilegeBuilding];
      this.dbrt17Form.markAsDirty();
    } else {
      this.ms.warning("message.STD00035");
      return;
    }
  }
  removePrivilegeBuilding(dbPrivilegeBuilding: DbPrivilegeBuilding) {
    if (dbPrivilegeBuilding.rowState !== RowState.Add) {
      dbPrivilegeBuilding.rowState = RowState.Delete;
      this.dbPrivilegeBuildingDelete.push(dbPrivilegeBuilding);
    }
    this.detail.dbPrivilegeBuilding = this.detail.dbPrivilegeBuilding.filter(item => item.guid !== dbPrivilegeBuilding.guid);
    this.dbrt17Form.markAsDirty();
  }

  save() {
    this.util.markFormGroupTouched(this.dbrt17Form);
    if (this.dbrt17Form.invalid) {
      return;
    }
    if (this.detail.dbRoom != null && this.detail.dbRoom != []) {
      this.detail.dbRoom.map(item => this.util.markFormGroupTouched(item.form));
      if (this.detail.dbRoom.some(item => item.form.invalid)) {
        this.ms.error("message.DB00001", ['label.DBRT17.RoomInformation']);
        return;
      }
    }
    if (this.detail.dbPrivilegeBuilding != null && this.detail.dbPrivilegeBuilding != []) {
      this.detail.dbPrivilegeBuilding.map(item => this.util.markFormGroupTouched(item.form));
      if (this.detail.dbPrivilegeBuilding.some(item => item.form.invalid)) {
        return;
      }
    }

    Object.assign(this.detail, this.dbrt17Form.value);
    this.detail.dbRoom.forEach(element => {
      if (element.costCenterCode == 'null') {
        element.costCenterCode = null;
      }
    });
    this.saving = true;
    this.cs.save(this.detail, this.dbrt17Form.getRawValue(), this.dbRoomDelete, this.dbPrivilegeBuildingDelete).pipe(
      finalize(() => this.saving = false)
    ).subscribe((buildingId) => {
      this.dbrt17Form.markAsPristine();
      this.rebuildForm();
      this.ms.success("message.STD00006");
      this.router.navigate(['db/dbrt17/detail'], { replaceUrl: true, state: { code: buildingId } })
    })
  }

  validate() {
    if (this.validate()) return;
    let invalid = false;
    if (this.dbrt17Form.invalid) {
      this.util.markFormGroupTouched(this.dbrt17Form);
      invalid = true;
    }
    if (this.detail.dbPrivilegeBuilding.some(item => item.form.invalid)) {
      this.detail.dbPrivilegeBuilding.map(item => this.util.markFormGroupTouched(item.form));
      invalid = true;
    }
    const seen = new Set();
    const detail = this.detail.dbPrivilegeBuilding.some((item) => {
      return seen.size === seen.add(item.form.controls.divCode.value).size && item.form.controls.divCode.value;
    });
    if (detail) {
      this.ms.error("message.STD00004", ['label.DBRT17.divCode']);
      invalid = true;
    }
    return invalid;
  }

  canDeactivate(): Observable<boolean> {
    if (!this.dbrt17Form.dirty) {
      return of(true);
    }
    return this.modal.confirm("message.STD00002");
  }
  changeBuild() {
    if (this.dbrt17Form.controls.responsibleAGC.value == 'F') {
      this.dbrt17Form.controls.facCode.enable();
      this.dbrt17Form.controls.divCode.setValue(null);
      this.dbrt17Form.controls.divCode.disable();

    } else if (this.dbrt17Form.controls.responsibleAGC.value == 'D') {
      this.dbrt17Form.controls.divCode.enable();
      this.dbrt17Form.controls.facCode.setValue(null);
      this.dbrt17Form.controls.facCode.disable();

    }
  }

  changeRoom(responsibleAGC, rowfacCode, rowdivCode) {
    if (responsibleAGC == 'F') {
      rowdivCode.setValue(null);
      rowdivCode.disable();
      rowfacCode.enable();

    } else if (responsibleAGC == 'D') {
      rowfacCode.setValue(null);
      rowdivCode.enable();
      rowfacCode.disable();
    }
  }

}
