import { Component, OnInit } from '@angular/core';
import { Page, ModalService, FormUtilService, ReportParam, ReportService } from '@app/shared';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Dbrt17Service } from './dbrt17.service';
import { MessageService, SaveDataService } from '@app/core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-dbrt17',
  templateUrl: './dbrt17.component.html',
  styleUrls: ['./dbrt17.component.scss']
})
export class Dbrt17Component implements OnInit {
  page = new Page();
  keyword: string = '';
  building = [];
  printing: boolean;
  constructor(
    private router: Router,
    private cs: Dbrt17Service,
    private modal: ModalService,
    private as: MessageService,
    private util: FormUtilService,
    private saver: SaveDataService,
    public reportService: ReportService
  ) { }

  onTableEvent() {

  }

  ngOnInit() {
    const saveData = this.saver.retrive("DBRT17");
    if (saveData) this.keyword = saveData;
    const savePage = this.saver.retrive("DBRT17Page");
    if (savePage) this.page = savePage;
    this.search();
  }

  ngOnDestroy() {
    this.saver.save(this.keyword, "DBRT17");
    this.saver.save(this.page, "DBRT17Page");
  }

  search() {
    this.cs.getBuilding(this.keyword, this.page)
      .subscribe(res => {
        this.building = res.rows;
        this.page.totalElements = res.count;
      });
  }

  enter(value) {
    this.keyword = value;
    this.page.index = 0;
    this.search();
  }


  remove(id, version) {
    this.modal.confirm("message.STD00003").subscribe(
      (res) => {
        if (res) {
          this.cs.delete(id, version)
            .subscribe(() => {
              this.as.success("message.STD00014");
              this.page = this.util.setPageIndex(this.page);
              this.search();
            });
        }
      })
  }

  add() {
    this.router.navigate(['/db/dbrt17/detail']);
  }

  print(type) {
    this.printing = true;
    const reportParam = {} as ReportParam;
    const objParam = {
      keyword: this.keyword
    };
    reportParam.paramsJson = objParam;
    reportParam.module = 'DB';
    reportParam.reportName = 'DBRP03';
    reportParam.autoLoadLabel = 'DBRP03';
    reportParam.exportType = type;

    this.reportService.generateReportBase64(reportParam).pipe(
      finalize(() => {
        this.printing = false;
      })
    ).subscribe((res: any) => {
      if (res) {
        if (type == 'PDF') {
          this.DowloadFile(`data:application/pdf;base64,${res}`, reportParam.reportName, type);
        } else {
          this.DowloadFile(res, reportParam.reportName, type)
        }
      }
    });
  }

  async DowloadFile(data, reportName, type) {
    const a = document.createElement('a');
    a.href = data;
    if (type == 'PDF') {
      a.download = reportName + '.pdf';
    }
    else {
      a.download = reportName + '.xlsx';
    }
    a.click();
  }
}
