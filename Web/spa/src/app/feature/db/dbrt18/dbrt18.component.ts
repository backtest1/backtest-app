import { Component, OnInit } from '@angular/core';
import { Page, ModalService, FormUtilService } from '@app/shared';
import { Router } from '@angular/router';
import { Dbrt18Service } from './dbrt18.service';
import { MessageService, SaveDataService } from '@app/core';

@Component({
  selector: 'app-dbrt18',
  templateUrl: './dbrt18.component.html',
  styleUrls: ['./dbrt18.component.scss']
})
export class Dbrt18Component implements OnInit {

  page = new Page();
  keyword: string = '';
  listItem = [];
  constructor(
    private router: Router,
    private cs: Dbrt18Service,
    private modal:ModalService,
    private as:MessageService,
    private util:FormUtilService,
    private saver:SaveDataService
  ) { }

  search(){
    this.cs.getListItem(this.keyword, this.page)
    .subscribe(res => {
      this.listItem = res.rows;
      this.page.totalElements = res.count;
    });
  }

  enter(value) {
    this.keyword = value;
    this.page.index = 0;
    this.search();
  }
  

  ngOnInit() {
    const saveData = this.saver.retrive("DBRT18");
    if (saveData) this.keyword = saveData;
    const savePage = this.saver.retrive("DBRT18Page");
    if (savePage) this.page = savePage;
    this.search();
  }

  ngOnDestroy() {
    this.saver.save(this.keyword, "DBRT18");
    this.saver.save(this.page, "DBRT18Page");
  }


}
