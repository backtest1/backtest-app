import { Component, OnInit } from '@angular/core';
import { Page, ModalService, FormUtilService, ReportParam, ReportService } from '@app/shared';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Dbrt08Service } from './dbrt08.service';
import { SaveDataService } from '@app/core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-dbrt08',
  templateUrl: './dbrt08.component.html',
  styleUrls: ['./dbrt08.component.scss']
})
export class Dbrt08Component implements OnInit {
  reportParam = {} as ReportParam;
  page = new Page();
  searchForm: FormGroup;
  employees = [];
  printing: boolean;
  master = {
    divlist: [],
    groupTypeCode: [],
    statusWork: []
  };
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private db: Dbrt08Service,
    public reportService: ReportService,
    private saver: SaveDataService
  ) {

  }

  ngOnInit() {
    this.createSearchForm();
    this.route.data.subscribe((data) => {
      this.master = data.dbrt08.master;
    });
    const saveData = this.saver.retrive('DBRT08');
    if (saveData) this.searchForm.patchValue(saveData);
    const savePage = this.saver.retrive("DBRT08Page");
    if (savePage) this.page = savePage;
    this.search(true);
  }
  ngOnDestroy() {
    this.saver.save(this.searchForm.value, 'DBRT08');
    this.saver.save(this.page, "DBRT08Page");
  }

  createSearchForm() {
    this.searchForm = this.fb.group({
      employeeCode: null,
      teacherCode: null,
      employeeName: null,
      divCode: null,
      groupTypeCode: null,
      turnoverDate: '2',
    })
  }

  search(resetIndex) {
    if (resetIndex) this.page.index = 0;
    this.db.getEmployees(this.searchForm.value, this.page)
      .subscribe(res => {
        this.employees = res.rows;
        this.page.totalElements = res.count;
      });
  }

  clear() {
    this.employees = [];
    this.searchForm.reset();
    this.search(true);
  }

  add() {
    this.router.navigate(['/db/dbrt08/detail']);
  }

  print() {
    this.printing = true;
    const objParam = {
      employee_code: this.searchForm.controls.employeeCode.value,
      teacher_code: this.searchForm.controls.teacherCode.value,
      employee_name: this.searchForm.controls.employeeName.value,
      div_code: this.searchForm.controls.divCode.value,
      grouptype_code: this.searchForm.controls.groupTypeCode.value,
      turnover_date: this.searchForm.controls.turnoverDate.value,
    };
    this.reportParam.paramsJson = objParam;
    this.reportParam.module = 'DB';
    this.reportParam.reportName = 'DBRP04';
    this.reportParam.exportType = 'XLSX';
    this.reportParam.autoLoadLabel = 'DBRP04';
    this.reportService.generateReportBase64(this.reportParam).pipe(
      finalize(() => {
        this.printing = false;
      })
    ).subscribe((res: any) => {
      if (res) {
        this.DowloadFile(res, this.reportParam.reportName)
      }
    });
  }

  async DowloadFile(data, reportName) {
    const a = document.createElement('a');
    a.href = data;
    a.download = reportName + '.xlsx';
    a.click();
  }
}
