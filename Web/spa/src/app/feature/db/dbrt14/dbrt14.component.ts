import { Component, OnInit } from '@angular/core';
import { Page, ModalService, FormUtilService, ReportParam, ReportService  } from '@app/shared';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Dbrt14Service } from './dbrt14.service';
import { MessageService, SaveDataService } from '@app/core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-dbrt14',
  templateUrl: './dbrt14.component.html',
  styleUrls: ['./dbrt14.component.scss']
})
export class Dbrt14Component implements OnInit {
  reportParam = {} as ReportParam;
  page = new Page();
  keyword: string = '';
  project = [];
  printing: boolean;

  constructor(
    private router: Router,
    private cs: Dbrt14Service,
    private modal: ModalService,
    private as: MessageService,
    private saver: SaveDataService,
    private util: FormUtilService,
    public reportService: ReportService
  ) { }

  onTableEvent() {

  }

  ngOnInit() {
    const saveData = this.saver.retrive("DBRT14");
    if (saveData) this.keyword = saveData;
    const savePage = this.saver.retrive("DBRT14Page");
    if (savePage) this.page = savePage;
    this.search();
  }

  ngOnDestroy() {
    this.saver.save(this.keyword, "DBRT14");
    this.saver.save(this.page, "DBRT14Page");
  }

  search() {
    this.cs.getProject(this.keyword, this.page)
      .subscribe(res => {
        this.project = res.rows;
        this.page.totalElements = res.count;
      });
  }
  enter(value) {
    this.keyword = value;
    this.page.index = 0;
    this.search();
  }

  remove(id, code, version) {
    this.modal.confirm("message.STD00003").subscribe(
      (res) => {
        if (res) {
          this.cs.delete(id, code, version)
            .subscribe(() => {
              this.as.success("message.STD00014");
              this.page = this.util.setPageIndex(this.page);
              this.search();
            });
        }
      })
  }

  add() {
    this.router.navigate(['/db/dbrt14/detail']);
  }

  print(type) {
    this.printing = true;
    const objParam = {
      keyword: this.keyword
    };
    this.reportParam.paramsJson = objParam;
    this.reportParam.module = 'DB';
    this.reportParam.reportName = 'DBRP05';
    this.reportParam.autoLoadLabel = 'DBRP05';
    this.reportParam.exportType = type;

    this.reportService.generateReportBase64(this.reportParam).pipe(
      finalize(() => {
        this.printing = false;
      })
    ).subscribe((res: any) => {
      if (res) {
        if (type == 'PDF') {
          this.DowloadFile(`data:application/pdf;base64,${res}`, this.reportParam.reportName, type);
        } else {
          this.DowloadFile(res, this.reportParam.reportName, type)
        }
      }
    });
  }

  async DowloadFile(data, reportName, type) {
    const a = document.createElement('a');
    a.href = data;
    if (type == 'PDF') {
      a.download = reportName + '.pdf';
    }
    else {
      a.download = reportName + '.xlsx';
    }
    a.click();
  }

}
