import { Component, OnInit } from '@angular/core';
import { Page, ModalService, FormUtilService } from '@app/shared';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Dbrt07Service } from './dbrt07.service';
import { MessageService, SaveDataService } from '@app/core';

@Component({
  selector: 'app-dbrt07',
  templateUrl: './dbrt07.component.html',
  styleUrls: ['./dbrt07.component.scss']
})
export class Dbrt07Component implements OnInit {
  page = new Page();
  keyword: string = '';
  prename = [];
  criteriaForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private cs: Dbrt07Service,
    private modal: ModalService,
    private saver: SaveDataService,
    private as: MessageService,
    private util: FormUtilService
  ) {

  }

  ngOnInit() {
    const saveData = this.saver.retrive("DBRT07");
    if (saveData) this.keyword = saveData;
    const savePage = this.saver.retrive("DBRT07Page");
    if (savePage) this.page = savePage;
    this.search();
  }

  ngOnDestroy() {
    this.saver.save(this.keyword, "DBRT07");
    this.saver.save(this.page, "DBRT07Page");
  }

  enter(value) {
    this.keyword = value;
    this.page.index = 0;
    this.search();
  }

  search() {
    this.cs.getPreNames(this.keyword, this.page)
      .subscribe(res => {
        this.prename = res.rows;
        this.page.totalElements = res.count;
      });
  }

  add() {
    this.router.navigate(['/db/dbrt07/detail']);
  }

  remove(code, version) {
    this.modal.confirm("message.STD00003").subscribe(
      (res) => {
        if (res) {
          this.cs.delete(code, version)
            .subscribe(() => {
              this.as.success("message.STD00014");
              this.page = this.util.setPageIndex(this.page);
              this.search();
            });
        }
      })
  }

}
