import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseList, BaseService } from '@app/shared';

export interface DbFacProgram {
  costCenterCode: any;
  companyCode: string;
  no: number;
  facCode: string;
  programCode: string;
  courseCode: string;
  active: boolean;
  rowVersion: number;
}

@Injectable()
export class Dbrt21Service extends BaseService {

  constructor(private http: HttpClient) { super(); }

  getFacProgram(search: string, page: any) {
    const filter = Object.assign({ keyword: search }, page);
    filter.sort = page.sort || 'codeLength,facCode,programCode';
    return this.http.get<any>('dbrt21', { params: filter });
  }

  getMaster() {
    return this.http.get<any>('dbrt21/master');
  }

  getFacProgramDetail(facCode, programCode) {
    return this.http.get<DbFacProgram>('dbrt21/detail', { params: { facCode: facCode, programCode: programCode } });
  }

  save(dbFacProgram: DbFacProgram, status: any, ) {
    const groupform = Object.assign({}, dbFacProgram, status);

    if (groupform.rowVersion) {
      if (groupform.costCenterCode == 'null') {
        groupform.costCenterCode = null
      }
      return this.http.put('dbrt21', groupform);
    }
    else {
      if (groupform.costCenterCode == 'null') {
        groupform.costCenterCode = null
      }
      return this.http.post('dbrt21', groupform);
    }
  }

  delete(facCode, programCode, version) {
    return this.http.delete('dbrt21', {
      params: {
        facCode: facCode
        , programCode: programCode, rowVersion: version
      }
    })
  }

}
