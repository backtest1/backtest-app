import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Dbrt21Service, DbFacProgram } from './dbrt21.service';
import { FormUtilService, ModalService } from '@app/shared';
import { MessageService } from '@app/core';
import { switchMap, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-dbrt21-detail',
  templateUrl: './dbrt21-detail.component.html',
  styleUrls: ['./dbrt21-detail.component.scss']
})
export class Dbrt21DetailComponent implements OnInit {

  dbrt21Form: FormGroup;
  submited: boolean = false;
  saving: boolean = false;
  master = { facCode: [], programCode: [], departmentCode: [], costCenterCode: [] };
  masterDepartment = [];
  detail: DbFacProgram = {} as DbFacProgram;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    public util: FormUtilService,
    private ms: MessageService,
    private modal: ModalService,
    private sv: Dbrt21Service
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.route.data.subscribe((data) => {
      this.detail = data.dbrt21.detail;
      if (this.detail.facCode != null && this.detail.programCode != null) {
        this.master.facCode = this.util.getActive(data.dbrt21.master.facCode, this.detail.facCode);
        this.master.programCode = data.dbrt21.master.programCode;
        this.master.costCenterCode = data.dbrt21.master.costCenterCode;

      } else {
        this.master.facCode = this.util.getActive(data.dbrt21.master.facCode, null);
        this.master.programCode = data.dbrt21.master.programCode;
        this.master.costCenterCode = data.dbrt21.master.costCenterCode;
      }
      this.masterDepartment = data.dbrt21.master.departmentCode;
      this.rebuildForm();
    });
  }

  rebuildForm() {
    this.dbrt21Form.markAsPristine();
    if (this.detail.facCode && this.detail.programCode) {
      this.dbrt21Form.patchValue(this.detail);
      this.disableFacProgramForEdit();
    }
    this.dbrt21Form.controls.costCenterCode.disable();
  }

  disableFacProgramForEdit() {
    this.dbrt21Form.controls.facCode.disable();
    this.dbrt21Form.controls.programCode.disable();
  }

  createForm() {
    this.dbrt21Form = this.fb.group({
      facCode: [null, [Validators.required, Validators.maxLength(2)]],
      programCode: [null, [Validators.required, Validators.maxLength(5)]],
      costCenterCode: null,
      active: true
    });

    this.dbrt21Form.controls.costCenterCode.valueChanges.subscribe(value => {
      if (value == null) {
        this.dbrt21Form.controls.costCenterCode.setValue(value = 'null')
      }
    })

  }

  save() {
    this.util.markFormGroupTouched(this.dbrt21Form);

    if (this.dbrt21Form.invalid) {
      return;
    }

    Object.assign(this.detail, this.dbrt21Form.value);
    if (this.detail.costCenterCode == 'null') {
      this.detail.costCenterCode = null
    }
    this.saving = true;
    this.sv.save(this.detail, this.dbrt21Form.getRawValue()).pipe(
      switchMap(() => this.sv.getFacProgramDetail(this.detail.facCode, this.detail.programCode)),
      finalize(() => { this.saving = false; })
    ).subscribe((result: DbFacProgram) => {
      this.dbrt21Form.markAsPristine();
      this.ms.success("message.STD00006");
      this.router.navigate(['db/dbrt21/detail'], {
        replaceUrl: true
        , state: { facCode: result.facCode, programCode: result.programCode }
      })
    })
  }

  canDeactivate(): Observable<boolean> {
    if (!this.dbrt21Form.dirty) {
      return of(true);
    }
    return this.modal.confirm("message.STD00002");
  }

}
