import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { DbEducationType, Dbrt09Service, DbEducationLevelDegree } from './dbrt09.service';
import { FormUtilService, ModalService, RowState } from '@app/shared';
import { switchMap, finalize } from 'rxjs/operators';
import { MessageService } from '@app/core';

@Component({
  selector: 'app-dbrt09-detail',
  templateUrl: './dbrt09-detail.component.html',
  styleUrls: ['./dbrt09-detail.component.scss']
})
export class Dbrt09DetailComponent implements OnInit {

  dbrt09Form: FormGroup;
  detail: DbEducationType = {} as DbEducationType;
  dbEducationLevelDegreeDelete: DbEducationLevelDegree[] = [];
  saving: boolean = false;
  master = {
    level: [],
    degree: []
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    public util: FormUtilService,
    private db: Dbrt09Service,
    private ms: MessageService,
    private modal: ModalService
  ) {
  }

  ngOnInit() {
    this.createForm();
    this.route.data.subscribe((data) => {
      this.detail = data.dbrt09.detail;
      this.master = data.dbrt09.master;
      if (this.detail.educationTypeLevel != null) {
        this.master.level = this.util.getActive(data.dbrt09.master.level, this.detail.groupLevel);
      } else {
        this.master.level = this.util.getActive(data.dbrt09.master.level, null);
      }
      this.rebuildForm();
    });
  }

  rebuildForm() {
    this.dbrt09Form.markAsPristine();
    if (this.detail.educationTypeLevel) {
      this.dbrt09Form.patchValue(this.detail);
      this.dbrt09Form.controls.educationTypeLevel.disable()
      if (this.detail.dbEducationLevelDegree) {
        this.detail.dbEducationLevelDegree.map(detail => detail.form = this.createEducationLevelDegreeForm(detail));
      }
    }
  }
  createForm() {
    this.dbrt09Form = this.fb.group({
      educationTypeLevel: [null, [Validators.required, Validators.maxLength(10)]],
      educationTypeLevelNameTha: [null, [Validators.required, Validators.maxLength(200)]],
      educationTypeLevelNameEng: [null, [Validators.maxLength(200)]],
      educationLevelShortNameTha: [null, [Validators.required, Validators.maxLength(200)]],
      educationLevelShortNameEng: [null, [Validators.maxLength(200)]],
      groupLevel: [null, [Validators.required, Validators.maxLength(10)]],
      educationTypeCodeMua: [null, [Validators.required, Validators.maxLength(10)]],
      formatCode: [null, [Validators.required, Validators.maxLength(10)]],
      active: true
    });
  }

  createEducationLevelDegreeForm(dbEducationLevelDegree: DbEducationLevelDegree) {
    const fg = this.fb.group({
      educationTypeLevel: [null, [Validators.required, Validators.maxLength(10)]],
      degreeId: [null, [Validators.required, Validators.maxLength(10)]],
      active: true,
      assigned: false
    });
    fg.valueChanges.subscribe((control) => {
      if (control.assigned && dbEducationLevelDegree.rowState == RowState.Normal) {
        dbEducationLevelDegree.rowState = RowState.Edit;
      }
    })
    fg.patchValue(dbEducationLevelDegree);
    fg.controls.assigned.setValue(true, { emitEvent: false });
    return fg;
  }

  addEducationLevelDegree() {
    if (this.detail.educationTypeLevel != null) {
      const newGroup = new DbEducationLevelDegree();
      newGroup.companyCode = this.detail.companyCode;
      newGroup.educationTypeLevel = this.detail.educationTypeLevel;
      newGroup.form = this.createEducationLevelDegreeForm(newGroup);
      this.detail.dbEducationLevelDegree.push(newGroup);
      this.detail.dbEducationLevelDegree = [...this.detail.dbEducationLevelDegree];
      this.dbrt09Form.markAsDirty();
    } else {
      this.ms.warning("message.STD00035");
      return;
    }
  }

  save() {
    this.util.markFormGroupTouched(this.dbrt09Form);
    if (this.dbrt09Form.invalid) {
      return;
    }
    if (this.detail.dbEducationLevelDegree != null && this.detail.dbEducationLevelDegree != []) {
      this.detail.dbEducationLevelDegree.map(item => this.util.markFormGroupTouched(item.form));
      if (this.detail.dbEducationLevelDegree.some(item => item.form.invalid)) {
        return;
      }
    }
    Object.assign(this.detail, this.dbrt09Form.value);
    this.saving = true;
    this.db.save(this.detail, this.dbrt09Form.getRawValue(), this.dbEducationLevelDegreeDelete).pipe(
      switchMap(() => this.db.getEducationLevelDegree(this.detail.educationTypeLevel)),
      finalize(() => { this.saving = false; })
    ).subscribe((result: DbEducationType) => {
      this.dbEducationLevelDegreeDelete = [];
      this.dbrt09Form.markAsPristine();
      this.rebuildForm();
      this.ms.success("message.STD00006");
      this.router.navigate(['db/dbrt09/detail'], { replaceUrl: true, state: { code: result.educationTypeLevel } })
    })
  }

  removeEducationLevelDegree(dbEducationLevelDegree: DbEducationLevelDegree) {
    if (dbEducationLevelDegree.rowState !== RowState.Add) {
      dbEducationLevelDegree.rowState = RowState.Delete;
      this.dbEducationLevelDegreeDelete.push(dbEducationLevelDegree);
    }
    this.detail.dbEducationLevelDegree = this.detail.dbEducationLevelDegree.filter(item => item.guid !== dbEducationLevelDegree.guid);
    this.dbrt09Form.markAsDirty();
  }

  canDeactivate(): Observable<boolean> {
    if (!this.dbrt09Form.dirty) {
      return of(true);
    }
    return this.modal.confirm("message.STD00002");
  }

}
