import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseList, BaseService } from '@app/shared';

export interface DbEducationType {
  companyCode: string;
  educationTypeLevel: string,
  educationTypeLevelCodeMua: string,
  educationTypeLevelNameTha: string,
  educationTypeLevelNameEng: string,
  educationLevelShortNameTha: string,
  educationLevelShortNameEng: string,
  educationTypeStudentCode: string,
  groupLevel: string,
  prefix: string,
  formatCode: string,
  active: boolean,
  rowVersion: number,
  dbEducationLevelDegree: DbEducationLevelDegree[]
}

export class DbEducationLevelDegree extends BaseList {
  companyCode: string;
  educationTypeLevel: string;
  degreeId: number;
  active: boolean;
}


@Injectable()
export class Dbrt09Service extends BaseService {
  constructor(private http: HttpClient) { super() }

  getEducationTypies(search: string, page: any) {
    const filter = Object.assign({ keyword: search }, page);
    filter.sort = page.sort || 'codeLength,educationTypeLevel';
    return this.http.get<any>('dbrt09', { params: filter });
  }
  
  getEducationLevelDegree(code) {
    return this.http.get<DbEducationType>('dbrt09/detail', { params: { educationTypeLevel: code } });
  }

  save(detail: DbEducationType, status: any, dbEducationLevelDegreeDelete: DbEducationLevelDegree[]) {
    const groupform = Object.assign({}, detail, status);

    if (groupform.rowVersion) {
      groupform.dbEducationLevelDegree = this.prepareSaveList(groupform.dbEducationLevelDegree, dbEducationLevelDegreeDelete);
      return this.http.put('dbrt09', groupform);
    }
    else {
      return this.http.post('dbrt09', groupform);
    }
  }

  getMaster() {
    return this.http.get<any>('dbrt09/master');
  }

  delete(code, version) {
    return this.http.delete('dbrt09', { params: { educationTypeLevel: code, rowVersion: version } })
  }

}
