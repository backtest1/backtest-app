import { Component, OnInit } from '@angular/core';
import { Page, ModalService, FormUtilService } from '@app/shared';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Dbrt16Service } from './dbrt16.service';
import { MessageService, SaveDataService } from '@app/core';

@Component({
  selector: 'app-dbrt16',
  templateUrl: './dbrt16.component.html',
  styleUrls: ['./dbrt16.component.scss']
})
export class Dbrt16Component implements OnInit {
  page = new Page();
  keyword: string = '';
  location = [];
  constructor(
    private router: Router,
    private cs: Dbrt16Service,
    private modal:ModalService,
    private as:MessageService,
    private util:FormUtilService,
    private saver:SaveDataService
  ) { }

  onTableEvent(){

  }

  ngOnInit() {
    const saveData = this.saver.retrive("DBRT16");
    if (saveData) this.keyword = saveData;
    const savePage = this.saver.retrive("DBRT16Page");
    if (savePage) this.page = savePage;
    this.search();
  }

  ngOnDestroy() {
    this.saver.save(this.keyword, "DBRT16");
    this.saver.save(this.page, "DBRT16Page");
  }

  search() {
    this.cs.getLocation(this.keyword, this.page)
      .subscribe(res => {
        this.location = res.rows;
        this.page.totalElements = res.count;
      });
  }

  enter(value) {
    this.keyword = value;
    this.page.index = 0;
    this.search();
  }
  

  remove(id, code, version) {
    this.modal.confirm("message.STD00003").subscribe(
      (res) => {
        if (res) {
          this.cs.delete(id, code, version)
            .subscribe(() => {
              this.as.success("message.STD00014");
              this.page = this.util.setPageIndex(this.page);
              this.search();
            });
        }
      })
  }

  add() {
    this.router.navigate(['/db/dbrt16/detail']);
  }

}
