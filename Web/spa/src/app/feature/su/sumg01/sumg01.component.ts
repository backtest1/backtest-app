import { Component, OnInit } from '@angular/core';
import { Sumg01Service } from './sumg01.service';
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Progress, SubscriptionDisposer } from '@app/shared';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '@app/core';

@Component({
  selector: 'app-sumg01',
  templateUrl: './sumg01.component.html',
  styleUrls: ['./sumg01.component.scss']
})
export class Sumg01Component extends SubscriptionDisposer implements OnInit {
  master = { registerYears:[] };
  disabled = false;
  progress: Progress = null;
  errorMessage = [];
  migrateForm:FormGroup;
  
  types = [{ value : 'S' ,text : 'นักศึกษา'},{ value : 'E' , text:'พนักงาน,อาจารย์ประจำ,อาจารย์พิเศษ'}]
  constructor( private route: ActivatedRoute, private fb: FormBuilder,private ms:MessageService,private ss:Sumg01Service) { super() }

  ngOnInit() {
    this.migrateForm = this.fb.group({
      registerYear:null,
      type:null
    })
    this.migrateForm.controls.type.valueChanges.pipe(
      takeUntil(this.ngUnsubscribe)
    ).subscribe(value=>{
      if(value == 'S'){
        this.migrateForm.controls.registerYear.setValidators(Validators.required);
        this.migrateForm.controls.registerYear.updateValueAndValidity();
      }
      else{
        this.migrateForm.controls.registerYear.clearValidators();
      }
    })
    this.migrateForm.controls.type.setValue('S');

    this.route.data.subscribe((data) => {
      this.master = data.sumg01;
      this.ss.connect();
      this.ss.workingEvent.pipe(
        takeUntil(this.ngUnsubscribe)
      ).subscribe(isWorking=>{
        this.disabled = isWorking;
      })
      this.ss.progressEvent.pipe(
        tap(progress=>{
          if(progress && progress.percent == null) this.ms.success('message.STD00020');
        }),
        takeUntil(this.ngUnsubscribe)
      ).subscribe(progress => {
        this.progress = progress;
        if(this.progress && this.progress.haveLog){
          this.errorMessage.push(this.progress.message);
        }
      })
    });

  }

  ngOnDestroy(){
    super.ngOnDestroy();
    this.ss.disconnect();
  }


  migrate() {
    this.errorMessage = [];
    this.ss.migrate(this.migrateForm.controls.registerYear.value,this.migrateForm.controls.type.value).subscribe();
  }

  stop() {
    this.ss.stop().subscribe();
  }
}
