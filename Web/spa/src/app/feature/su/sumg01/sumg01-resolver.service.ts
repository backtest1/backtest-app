import { Injectable } from '@angular/core';
import {
    Router, Resolve, RouterStateSnapshot,
    ActivatedRouteSnapshot
} from '@angular/router';
import {  Observable } from 'rxjs';
import { Sumg01Service } from './sumg01.service';

@Injectable()
export class Sumg01Resolver implements Resolve<any> {
    constructor(private router: Router, private ss: Sumg01Service) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
       return this.ss.getMaster();
    }
}