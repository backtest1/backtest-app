import { Component, OnInit } from '@angular/core';
import { Sumg03Service } from './sumg03.service';
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Progress, SubscriptionDisposer } from '@app/shared';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '@app/core';

@Component({
  selector: 'app-sumg03',
  templateUrl: './sumg03.component.html',
  styleUrls: ['./sumg03.component.scss']
})
export class Sumg03Component extends SubscriptionDisposer implements OnInit {
 
  disabled = false;
  progress: Progress = null;
  errorMessage = [];
  migrateForm:FormGroup;
  
  constructor( private route: ActivatedRoute, private fb: FormBuilder,private ms:MessageService,private ss:Sumg03Service) { super() }

  ngOnInit() {
    this.route.data.subscribe((data) => {
      this.ss.connect();
      this.ss.workingEvent.pipe(
        takeUntil(this.ngUnsubscribe)
      ).subscribe(isWorking=>{
        this.disabled = isWorking;
      })
      this.ss.progressEvent.pipe(
        tap(progress=>{
          if(progress && progress.percent == null) this.ms.success('message.STD00020');
        }),
        takeUntil(this.ngUnsubscribe)
      ).subscribe(progress => {
        this.progress = progress;
        if(this.progress && this.progress.haveLog){
          this.errorMessage.push(this.progress.message);
        }
      })
    });

  }

  ngOnDestroy(){
    super.ngOnDestroy();
    this.ss.disconnect();
  }


  migrate() {
    this.errorMessage = [];
    this.ss.migrate().subscribe();
  }

  stop() {
    this.ss.stop().subscribe();
  }
}
