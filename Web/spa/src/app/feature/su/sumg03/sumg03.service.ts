import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HubConnection } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';
import { environment } from '@env/environment';
import { Progress } from '@app/shared';

@Injectable()
export class Sumg03Service {
  private jobId = "SUMG03";
  private hubConnection: HubConnection | undefined;
  progressEvent = new EventEmitter<Progress>();
  workingEvent = new EventEmitter<boolean>();
  constructor(private http: HttpClient) { }
  
  migrate(){
    return this.http.post<any>('sumg03',{ jobName:this.jobId });
  }

  stop(){
    return this.http.get('sumg03',{ params : { jobName : this.jobId }});
  }

  connect() {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}job`, {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
      })
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this.hubConnection.start().then(() => {
      this.hubConnection.invoke("AssociateJob", this.jobId);
      this.hubConnection.invoke("IsWorking");
    }).catch(err => console.error(err.toString()));

    this.hubConnection.on('progress', (progress: Progress) => {
      this.progressEvent.emit(progress);
    });

    this.hubConnection.on('working', (isWorking: boolean) => {
      this.workingEvent.emit(isWorking);
    })
  }

  disconnect() {
    this.hubConnection.stop();
  }
}
