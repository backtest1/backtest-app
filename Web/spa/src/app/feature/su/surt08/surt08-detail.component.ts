import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '@app/core';
import { FormUtilService, ModalService, Pattern, RowState } from '@app/shared';
import { Category } from '@app/shared/component/attachment/category';
import { ContentUploadService, Type } from '@app/shared/component/attachment/content-upload.service';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EmailTemplate, EmailTemplateAttachment, Surt08Service } from './surt08.service';

declare var tinymce: any;

@Component({
  selector: 'app-surt08-detail',
  templateUrl: './surt08-detail.component.html',
  styleUrls: ['./surt08-detail.component.scss']
})
export class Surt08DetailComponent implements OnInit {
  emailTemplateForm: FormGroup;
  emailTemplate: EmailTemplate;
  saving: boolean = false;

  css = '.text-muted { color: #6c757d !important; } .img-fluid { max-width: 100%; height: auto; } .justify-content-center { justify-content: center !important; } .no-gutters { margin-right: 0; margin-left: 0; } .image-map { width: 100%; max-width: 550px; height: auto; display: block; margin-right: auto; margin-left: auto; } .right { text-align: right; } .background-box { padding-top: 55px; padding-left: 55px; padding-right: 55px; background-color: lightgray; border: 5px solid white; } .header { text-align: center; color: #676767; padding-top: 20px; padding-bottom: 20px; } .background-box1 { text-align: center; padding-top: 55px; padding-bottom: 55px; padding-left: 24px; padding-right: 24px; padding-bottom: 55px; background-color: lightgray; border: 5px solid white; } .background-box2 { text-align: center; padding-top: 55px; padding-bottom: 55px; padding-left: 83px; padding-right: 84px; padding-bottom: 55px; background-color: lightgray; border: 5px solid white; } .background-box3 { text-align: center; padding-top: 55px; padding-bottom: 55px; padding-left: 58px; padding-right: 58px; padding-bottom: 55px; background-color: lightgray; border: 5px solid white; } .background-box4 { text-align: center; padding-top: 55px; padding-bottom: 55px; padding-left: 30px; padding-right: 30px; padding-bottom: 55px; background-color: lightgray; border: 5px solid white; word-break: break-word; } .center { text-align: center; } .icon-size { font-size:30px; } .background-contact1 { padding-top: 20px; padding-bottom: 20px; border: 5px solid white; background-color: #2D2A4B !important; } .background-contact2 { padding-top: 20px; padding-bottom: 40px; border: 5px solid white; background-color: #2D2A4B !important; } .font-contact { color: mintcream; width:100%; } .font-color-left { text-align: left; color: #676767; } .font-color { color: #676767; } .border-right { border-right-color:#FFFFFF; border-right-style: solid; width: 60%; } .border-space { width: 15%; } .font-size-detail { font-size: 16px } .border-heading { border-bottom-color:#cecece; border-bottom-style: solid; padding-bottom: 20px; } .padding-img { padding-left: 3px; }';

  config: any = {
    height: 250,
    skin: false,
    plugins: [
      'advlist autolink link image lists preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking',
      'table contextmenu directionality template textcolor paste colorpicker textpattern tinymceEmoji',
    ],
    toolbar1:
      'bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect formatselect fontsizeselect tinymceEmoji',
    toolbar2:
      'searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor',
    toolbar3:
      'table | hr removeformat | subscript superscript | ltr rtl | visualchars visualblocks nonbreaking template pagebreak',
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css'
    ],
    content_style: [this.css],
    plugin_preview_width: 1024,
    menubar: false,
    toolbar_items_size: 'small',
    templates: [
      {
        title: 'Test template 1',
        content: 'Test 1',
      },
      {
        title: 'Test template 2',
        content: 'Test 2',
      },
    ],
    convert_urls: false,

    //upload image
    automatic_uploads: true,
    paste_data_images: true,
    file_picker_types: 'image',
    file_picker_callback: function (cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');
      input.onchange = function () {
        const file = input.files[0];
        var reader = new FileReader();
        reader.onload = function () {
          var id = 'blobid' + (new Date()).getTime();
          var blobCache = tinymce.activeEditor.editorUpload.blobCache;
          var base64 = (<string>reader.result).split(',')[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);
          cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
      };

      input.click();
    },
    images_upload_handler: (blobInfo, success, failure) => {
      this.us.upload(blobInfo.blob(), Type.Image, Category.Example).subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Response:
            const content = event.body;
            this.us.pathObserv.subscribe(path => {
              const attachment = new EmailTemplateAttachment()
             // attachment.emailTemplateCode = this.emailTemplate.emailTemplateCode;
              attachment.fileName = content.name;
              attachment.path = path.contentUrl + content.path;
              attachment.attachmentId = content.id;
              this.emailTemplate.emailTemplateAttachments.push(attachment);
              success(path.contentUrl + content.path);
            })
            break;
        }
      })
    },
  };

  attachmentDeletes: EmailTemplateAttachment[];
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public util: FormUtilService,
    private ms: MessageService,
    private modal: ModalService,
    private su: Surt08Service,
    private us: ContentUploadService
  ) { }

  ngOnInit() {
    this.attachmentDeletes = [];
    this.createForm();
    this.route.data.subscribe((data) => {
      this.emailTemplate = data.surt08.detail;
      this.rebuildForm();
    });
  }

  createForm() {
    this.emailTemplateForm = this.fb.group({
      emailTemplateCode: [null, [Validators.required, Validators.maxLength(10), Validators.pattern(Pattern.UpperOnly)]],
      emailTemplateName: [null, [Validators.required, Validators.maxLength(100)]],
      subject: [null, Validators.maxLength(100)],
      description: [null, Validators.maxLength(200)],
      content: null
    });
  }

  rebuildForm() {
    this.attachmentDeletes = [];
    this.emailTemplateForm.markAsPristine();
    if (this.emailTemplate.emailTemplateCode) {
      this.emailTemplateForm.patchValue(this.emailTemplate, { emitEvent: false });
      this.emailTemplateForm.controls.emailTemplateCode.disable({ emitEvent: false });
      this.emailTemplateForm.controls.content.markAsPristine();
    }
  }

  prepareAttachment(){
    this.emailTemplate.emailTemplateAttachments.forEach(attachment=>{
       if(!this.emailTemplateForm.controls.content.value || (this.emailTemplateForm.controls.content.value && this.emailTemplateForm.controls.content.value.indexOf(attachment.path) == -1)){
         if(attachment.rowState !== RowState.Add) this.attachmentDeletes.push(attachment);
          this.emailTemplate.emailTemplateAttachments = this.emailTemplate.emailTemplateAttachments.filter(item => item.guid !== attachment.guid);
       }
    })
  }

  save() {
    this.util.markFormGroupTouched(this.emailTemplateForm);
    if (this.emailTemplateForm.invalid) {
      return;
    }
    this.prepareAttachment();
    this.su.save(this.emailTemplate, this.emailTemplateForm.value,this.attachmentDeletes).pipe(
      switchMap(() => this.su.getTemplate(this.emailTemplate.emailTemplateCode))
    ).subscribe((result: EmailTemplate) => {
      this.emailTemplate = result;
      this.rebuildForm();
      this.ms.success("message.STD00006");
    })
  }

  canDeactivate(): Observable<boolean> {
    if (!this.emailTemplateForm.dirty) {
      return of(true);
    }
    return this.modal.confirm("message.STD00002");
  }


}
