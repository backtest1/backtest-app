import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, SaveDataService } from '@app/core';
import { FormUtilService, ModalService, Page } from '@app/shared';
import { Surt08Service } from './surt08.service';

@Component({
  selector: 'app-surt08',
  templateUrl: './surt08.component.html',
  styleUrls: ['./surt08.component.scss']
})
export class Surt08Component implements OnInit {
  page = new Page();
  keyword: string = '';
  templates = [];

  constructor(
    private su: Surt08Service,
    private router: Router,
    private modal: ModalService,
    private ms: MessageService,
    private util: FormUtilService,
    private saver: SaveDataService
  ) { }

  ngOnInit() {
    const saveData = this.saver.retrive('SURT08');
    if (saveData) this.keyword = saveData;
    const savePage = this.saver.retrive('SURT08Page');
    if (savePage) this.page = savePage;
    this.search();
  }

  ngOnDestroy() {
    this.saver.save(this.keyword, 'SURT08');
    this.saver.save(this.page, 'SURT08Page');
  }

  search() {
    this.su.getTemplates(this.keyword, this.page)
      .pipe()
      .subscribe(res => {
        this.templates = res.rows;
        this.page.totalElements = res.count;
      });
  }

  enter(value) {
    this.keyword = value;
    this.page.index = 0;
    this.search();
  }
  
}
