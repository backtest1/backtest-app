import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseList, BaseService } from '@app/shared';
import { of } from 'rxjs';

export class EmailTemplateAttachment extends BaseList{
   emailTemplateCode:string;
   attachmentId:number;
   fileName:string;
   path:string;
}

export interface EmailTemplate{
    emailTemplateCode:string,
    emailTemplateName:string,
    subject:string,
    content:string,
    description:string,
    rowVersion:number,
    emailTemplateAttachments:EmailTemplateAttachment[]
}

@Injectable()
export class Surt08Service  extends BaseService{

  constructor(private http: HttpClient) { super(); }

  getTemplates(search: string, page: any){
    const filter = Object.assign({ keyword: search }, page);
    filter.sort = page.sort || 'templateCode'
    return this.http.get<any>('surt08', { params: filter });
  }

  getTemplate(code) {
    return this.http.get<EmailTemplate>('surt08/detail', { params: { emailTemplateCode: code } });
  }

  save(template: EmailTemplate,form:any,attachmentDeletes:EmailTemplateAttachment[]) {
    const email = Object.assign({}, template, form);
    email.emailTemplateAttachments = this.prepareSaveList(email.emailTemplateAttachments, attachmentDeletes);
    if (email.rowVersion)
      return this.http.put('surt08', email);
    else
      return this.http.post('surt08', email);
  }

  delete(code, version) {
    return this.http.delete('surt08', { params: { emailTemplateCode: code, rowVersion: version } });
  }
}
