import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Surt08Service } from './surt08.service';
import { Observable, of, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class Surt08Resolver implements Resolve<any> {

  constructor(private router: Router, private su: Surt08Service) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const code = this.router.getCurrentNavigation().extras.state;
    const detail = code && code.code ? this.su.getTemplate(code.code) : of({});
    const url: string = route.url.join('/');
    if(url.includes("detail") && (!code || !code.code)){
      this.router.navigate(["/su/surt08"]);
      return of({});
    }

    return forkJoin(detail).pipe(map((result) => {
      let detail = result[0];
      return { detail };
    }))
  }
}
