import { Injectable } from '@angular/core';
import { Router, CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Route, CanActivateChild } from '@angular/router';
import { MessageService } from '@app/core';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { FnPosService } from '../fn-pos.service';

@Injectable()
export class CashierGuard implements CanActivate, CanLoad, CanActivateChild {

    constructor(private service: FnPosService, private router: Router, private ms: MessageService) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.checkCashier(state);
    }
    canActivateChild(
        childRoute: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.checkCashier(state);
    }
    canLoad(state: Route): Observable<boolean> {
        return this.checkCashier(state);
    }

    private checkCashier(state): Observable<boolean> {
        return this.service.checkCashier().pipe(
            map(cashiers => {
                if (!!cashiers && cashiers.cashierCode !== '') return true;
                else {
                    this.ms.warning('message.FN00026');
                    return false;
                }
            })
        )
    }
}