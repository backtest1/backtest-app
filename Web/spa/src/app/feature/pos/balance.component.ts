import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '@app/core';
import { ModalService, ReportParam, ReportService, Size } from '@app/shared';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { BehaviorSubject } from 'rxjs';
import { FnPosService, PosPayload } from './fn-pos.service';

@Component({
  selector: 'app-pos-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosBalanceComponent implements AfterViewInit {
  
  items = [];
  bill = new BehaviorSubject<any>(new PosPayload());
  _target = new PosPayload();

  constructor( activatedRoute: ActivatedRoute, private service: FnPosService, private router: Router, private as : MessageService, private rs: ReportService ) { 
    activatedRoute.data.subscribe( extras => {
        if(!!extras.extras) {
            this._target = extras.extras;
            this.bill.next(extras.extras);
        }
    });
  }

  @ViewChild('successSwal') private successSwal: SwalComponent;
  
  ngAfterViewInit() {
    this.successSwal.show();
  }


  goHome() {
    this.router.navigate(['/dashboard'], { replaceUrl: true });  
  }

  export(type: string) {
    if(!!this._target.receiveNo) {
      switch (type) {
        case 'email':
          this.sendEmail();
          break; 
        default:
          this.print();
          break;
      }
    }
  }

  reportParam = {} as ReportParam;
  itemToPrint = [];
  language = new FormControl('th');
  languageRadioItems = [{ value: 'th', text: 'ไทย (ค่าเริ่มต้น)' }, { value: 'en', text: 'อังกฤษ' }];

  print() {
    // this.printing = true;
    this.itemToPrint.push(this._target.receiveNo);
    let objParam = {
      "receive_no": this.itemToPrint,
      "doc_type": "O",
      "lin_id": this.language.value
    };
    this.reportParam.paramsJson = objParam;
    this.reportParam.module = 'FN';
    this.reportParam.exportType = 'PDF';
    this.reportParam.reportName = 'FNOD02';
    this.reportParam.autoLoadLabel = 'FNOD02';
    this.rs.generateReportBase64WithLang(this.reportParam).pipe(
    //   finalize(() => {
    //     this.printing = false;
    //   })
    ).subscribe((res: any) => {
      if (res) {
        this.DowloadFile(res, this.reportParam.reportName);
        this.itemToPrint = [];
      }
    });
  }
  sendEmail(){
    this.service.sendEmail(this._target.receiveNo, this.language.value).subscribe( res => {
      this.as.success('message.FN00051');
    });
  }

  async DowloadFile(data, reportName) {
    var newWindow = window.open();
    newWindow.document.write('<object width="100%" height="100%"  data="data:application/pdf;base64,' + (data) + '" type="application/pdf"></object>');
    newWindow.document.title = reportName + ".pdf";
  }

}
