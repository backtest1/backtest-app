import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';


@Component({
  selector: 'pos-customer-dialog',
  templateUrl: './customer-dialog.component.html',
  styleUrls: ['./customer-dialog.component.scss']
})
export class CustomerDialogComponent implements OnInit {
  onClose = new Subject();
  form: FormGroup;
  educationTypeLevel = [];
  fac = [];
  constructor(private bsModalRef: BsModalRef,  private fb: FormBuilder) { 
    this.form= this.fb.group({
      educationTypeLevel: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      idCard: [null, Validators.required],
      facCode: [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  close() {
    this.bsModalRef.hide();
  }

  add() {
    if(this.form.valid) {
      const value = this.form.getRawValue();
      const fullname = `${value.firstName} ${value.lastName}`;
      this.onClose.next({
        description: 'ระดับการศึกษา',
        imagePath: null,
        studentCode: null,
        text: `${value.idCard} | ${fullname}`,
        value: { educationTypeLevel: value.educationTypeLevel, idCard: value.idCard, studentId: 0, studentName: fullname, facCode: value.facCode }
      });
      this.close();
    }
  }

}

// description: "ระดับการศึกษา: ปริญญาตรี โปรแกรมวิชา:  "
// imagePath: null
// studentCode: "4008614008"
// text: "4008614008 | นางสาวชลธิชา  รักษาราษฎร์"
// value:
// educationTypeLevel: ""
// idCard: ""
// studentId: 102
// studentName: ""

//https://mynoz.wordpress.com/2006/05/01/how-to-cal-the-last-digit-of-thai-citizen-id-card/
// การตรวจสอบรหัส ตัวที่ 13 ของบัตรประจำตัวประชาชน
// 1 นำเอาตัวเลขทั้ง 12 หลักมาคูณกันตามค่าเอาไว้

// Int1 * 13
// Int2 * 12
// Int3 * 11
// Int4 * 10
// Int5 * 9
// Int6 * 8
// Int7 * 7
// Int8 * 6
// Int9 * 5
// Int10 * 4
// Int11 * 3
// Int12 * 2

// 2 นำค่าที่ได้จาก ข้อ 1 มาบวกกันทั้งหมด
// 3 แล้วจากนั้นก็นำผลบวกที่ได้มา Mod กับ 11
// 4 ค่าที่ได้จาก ข้อ 3 นำมาลบออกจาก 11

// ค่าจากข้อ 4 คือรหัสหลักที่ 13 ของบัตรประชาชน
