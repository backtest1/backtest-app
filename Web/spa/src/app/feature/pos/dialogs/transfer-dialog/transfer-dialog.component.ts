import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormUtilService, SubscriptionDisposer } from '@app/shared';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { utils } from 'protractor';

@Component({
  selector: 'pos-transfer-dialog',
  templateUrl: './transfer-dialog.component.html',
  styleUrls: ['../dialog.component.scss']
})
export class TransferDialogComponent extends SubscriptionDisposer {

    @Input() price: number = 0;
    @Input() type: string = null;
    @Output() selected = new EventEmitter<any>();

    master = {};
    resolved: any;
    paymentForm: FormGroup;
    accountModel = {};
    accountChange = new Subject<any>();
  
    constructor(private bsModalRef: BsModalRef, private fb: FormBuilder, public util: FormUtilService) { 
      super();
      this.paymentForm = this.fb.group({
        account: [null, Validators.required],
        amount: [0, [Validators.required, Validators.min(1)]],
        description: null
      });
    }
  
    ngOnInit() {
      this.accountChange.pipe(takeUntil(this.ngUnsubscribe)).subscribe( res => {
        this.accountModel = { accountName: res.text };
      });
    }

    close() {
      this.bsModalRef.hide();
    }

    complete() {
      if(this.paymentForm.valid) {
        this.selected.emit(Object.assign(this.paymentForm.getRawValue(), this.accountModel));
        this.close();
      } else {
        this.util.markFormGroupTouched(this.paymentForm);
      }
    }
}
