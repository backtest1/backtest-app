import { Injectable } from "@angular/core";
import { Observable, forkJoin } from "rxjs";
import { map } from "rxjs/operators";
import { ModalResolve } from "@app/shared/component/modal/modal-resolver";
import { FnPosService } from "../../fn-pos.service";

@Injectable()
export class TransferDialogResolverService implements ModalResolve<any> {
    constructor(
       private service: FnPosService
    ) { }
    resolve(param: any): Observable<any> {
        const master = this.service.getMaster(['nav_banks']);
        return forkJoin(master).pipe(
            map((result) => {
                let master = result[0];
                return { master };
            })
        );
    }
}