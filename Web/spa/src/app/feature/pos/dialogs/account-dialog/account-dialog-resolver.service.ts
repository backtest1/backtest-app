import { Injectable } from "@angular/core";
import { Observable, forkJoin } from "rxjs";
import { map } from "rxjs/operators";
import { ModalResolve } from "@app/shared/component/modal/modal-resolver";
import { FnPosService } from "../../fn-pos.service";

@Injectable()
export class AccountDialogResolverService implements ModalResolve<any> {
  constructor(private service: FnPosService) {}
  resolve(param: any): Observable<any> {
    const master = this.service.getTemplateMaster(['budgets', 'fundCenters', 'costCenters']);
    return forkJoin(master).pipe(
      map((result) => {
        const master = result[0];
        return { master };
      })
    );
  }
}
