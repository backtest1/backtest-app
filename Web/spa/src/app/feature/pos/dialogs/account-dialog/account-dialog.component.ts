import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';


@Component({
  selector: 'pos-account-dialog',
  templateUrl: './account-dialog.component.html',
  styleUrls: []
})
export class AccountDialogComponent implements OnInit {
    @Input() budgetCode: string = null;  
    @Input() fundCenterCode: string = null;  
    @Input() costCenterCode: string = null;
    @Output() selected = new EventEmitter<any>();
    form: FormGroup;
    resolved: any;
    master = {
        costCenters: [],
        budgets: [],
        fundCenters: []
    };

    constructor(private bsModalRef: BsModalRef,  private fb: FormBuilder) { }

    ngOnInit() {
        this.master = this.resolved.master;
        this.form = this.fb.group({
            budgetCode: this.budgetCode,
            fundCenterCode: this.fundCenterCode,
            costCenterCode: this.costCenterCode
        });
    }

    close() {
        this.bsModalRef.hide();
    }

    add() {
        if(this.form.valid) {
        const value = this.form.getRawValue();
        this.selected.emit(value);
        this.close();
        }
    }

}