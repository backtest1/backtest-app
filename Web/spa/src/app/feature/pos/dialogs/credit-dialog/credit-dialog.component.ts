import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FnPosService } from '../../fn-pos.service';
import { SubscriptionDisposer } from '@app/shared';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'pos-credit-dialog',
  templateUrl: './credit-dialog.component.html',
  styleUrls: ['../dialog.component.scss', './credit-dialog.component.scss']
})
export class CreditDialogComponent extends SubscriptionDisposer {

    @Input() price: number = 0;
    @Input() type: string = null;
    @Input() devices: any[] = [];
    @Output() selected = new EventEmitter<any>();

    cardNumber: string  = "";
    master = {};
    resolved: any;
    paymentForm: FormGroup;
    netAmt = 0;
    fee = {
      chargeAmt: 0,
      bankCharge: 0,
      vatAmt: 0
    }
  
    constructor(private bsModalRef: BsModalRef, private service: FnPosService, private fb: FormBuilder) { 
      super();
      this.paymentForm = this.fb.group({
        cardNumber: [null],
        cardProvider: [null, Validators.required],
        bank: [null, Validators.required],
        device: [null]
        // amount: [this.netAmt, Validators.required]
      });
    }
  
    ngOnInit() {
      this.netAmt = this.price;
      const pattern = new RegExp('^[0-9]+', 'g');
      this.paymentForm.get('cardNumber').valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe( value => {
        if(value != '' && value.length == 16 && pattern.test(value)) {
          let digiest = 0;
          this.cardNumber = value;
          for (let index = 0; index < (this.cardNumber.length - 1); index++) {
            if((index%2) == 0) {
              let m = Number(this.cardNumber[index]) * 2; 
              if(m > 9) {
                m -= 9;
              }
              digiest += m;
            } else {
              digiest += Number(this.cardNumber[index]);
            }                
          }
          const checkDigit = (digiest * 9).toString();
          if(checkDigit.substr(-1, 1) == value.substr(-1, 1)) {

          }
        }
      });
      this.paymentForm.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe( () => {
        this.calculateFee();
      });
    }

    calculateFee() {
      if(this.paymentForm.valid) {
        const args = this.paymentForm.getRawValue();
        this.service.calculateFee({
          amount: this.price,
          cardBankCode: args.bank,
          cardProvider: args.cardProvider,
          cardType: this.type,
          deviceCode: args.device
        }).subscribe( res => {
          this.netAmt = res.netAmt || 0;
          this.fee.chargeAmt = res.chargeAmt || 0;
          this.fee.bankCharge = res.bankCharge || 0;
          this.fee.vatAmt = res.vatAmt || 0;
        });
      }
    }

    close() {
      this.bsModalRef.hide();
    }

    validateCardNumber(target: string) {
      console.log(target);
    }

    complete() {
      this.paymentForm.markAsTouched();
      if(this.paymentForm.valid) {
        this.selected.emit(Object.assign(this.paymentForm.getRawValue(), this.fee, { amount: this.netAmt }));
        this.close();
      } else {

      }
    }
}
