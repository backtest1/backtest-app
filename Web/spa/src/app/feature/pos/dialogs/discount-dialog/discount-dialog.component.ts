import { Component } from '@angular/core';
import { BaseLookupMultipleComponent } from '@app/shared';

@Component({
  selector: 'pos-discount-dialog',
  templateUrl: './discount-dialog.component.html',
  styleUrls: ['./discount-dialog.component.scss']
})
export class DiscountDialogComponent extends BaseLookupMultipleComponent {
    resolved: any;
    items = [];
    master = {
        rows: []
    };
    systemName: string;
    displayLabel: string;
    ngOnInit() {
        this.master = this.resolved.master;
        this.items = this.excludeRows(this.master.rows);
    }
    identity(row) {
        return row.discountId;
    }
}