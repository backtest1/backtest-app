import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormBuilder,  FormGroup, Validators } from '@angular/forms';
import { FormUtilService } from '@app/shared';

@Component({
  selector: 'app-qr-dialog',
  templateUrl: './qr-dialog.component.html',
  styleUrls: ['../dialog.component.scss']
})
export class QrDialogComponent implements OnInit {


  @Input() price: number = 0;
  @Input() name: string = `รับชำระ`;
  @Output() selected = new EventEmitter<any>();
  paymentForm: FormGroup;
  
  constructor(private bsModalRef: BsModalRef, private fb: FormBuilder, public util: FormUtilService) { }

  ngOnInit() {
    this.paymentForm = this.fb.group({
      description: [null],
      amount: [this.price, [Validators.required, Validators.min(1)]]
    });
  }

  close() {
    this.bsModalRef.hide();
  }

  complete() {
    if(this.paymentForm.valid) {
      this.selected.emit(this.paymentForm.getRawValue());
      this.bsModalRef.hide();
    } else {
      this.util.markFormGroupTouched(this.paymentForm);
    }
  }

}
