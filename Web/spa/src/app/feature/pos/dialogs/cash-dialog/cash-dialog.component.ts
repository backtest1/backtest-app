import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormUtilService } from '@app/shared';

@Component({
  selector: 'app-cash-dialog',
  templateUrl: './cash-dialog.component.html',
  styleUrls: ['../dialog.component.scss']
})
export class CashDialogComponent implements OnInit {

  @Input() title: string = 'label.FNDT02.ReceiveWithCash';
  @Input() price: number = 0;
  @Output() selected = new EventEmitter<any>();
  paymentForm: FormGroup;
  
  constructor(private bsModalRef: BsModalRef, private fb: FormBuilder, private util: FormUtilService) { }

  ngOnInit() {
    this.paymentForm = this.fb.group({
      description: [null],
      amount: [this.price, [Validators.required, Validators.min(1)]]
    });
  }

  close() {
    this.bsModalRef.hide();
  }

  complete() {
    if(this.paymentForm.valid) {
      this.selected.emit(this.paymentForm.getRawValue());
      this.bsModalRef.hide();
    } else {
      this.util.markFormGroupTouched(this.paymentForm);
    }
  }


}
