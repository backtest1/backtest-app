import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '@app/core';
import { ModalService, Size, SubscriptionDisposer } from '@app/shared';
import { BehaviorSubject, Subscription } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { FnPosService, PosPayload } from './fn-pos.service';

@Component({
  selector: 'app-pos-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosSummaryComponent  extends SubscriptionDisposer implements OnInit {
  isError: boolean = false;
  receives = [];
  items = [];
  bill = new BehaviorSubject<any>(new PosPayload());
  _target = new PosPayload();
  balance = 0;
  receiveBalance = 0;
  constructor( activatedRoute: ActivatedRoute, private service: FnPosService, private router: Router, private as : MessageService, private modal: ModalService ) { 
    super();
    activatedRoute.data.subscribe( extras => {
      if(!!extras.extras) {
        this._target = extras.extras;
        this.bill.next(extras.extras);
      }
    });
  }
  ngOnInit(): void {
    this.bill.pipe(takeUntil(this.ngUnsubscribe)).subscribe( bill => {
      this._target = bill;
      this.balance = this._target.netAmt;
    });
  }
  back() {
    if(!!this._target.billNo) {
      this.router.navigate(['fn/fndt02'], { state: this._target, replaceUrl: true });
    } else {
      this.router.navigate(['fn/fndt02'], { replaceUrl: true });
    }   
  }
  pay($event: any) {   

    const type = $event.type;
    const amount = $event.amount;
    const name = $event.name;
    const cardBankCode = $event.cardBankCode || null;
    const cardProvider = $event.cardProvider || null;
    const approveCode = $event.approveCode || null;
    const feeAmt = $event.chargeAmt || 0;
    const grossAmt = $event.grossAmt || 0;
    const bankCharge = $event.bankCharge || 0;
    const vatAmt = $event.vatAmt || 0;
    const cardType = $event.cardType || '0';
    const cardNo = $event.cardNo || null;
    const bankCode = $event.bankCode || null;
    const description = $event.description || ``;
    const account = $event.accountName || ``;
    const cardIssuerName = $event.cardIssuerName || null;
    const terminalId = $event.terminalId || null;
    const traceNo = $event.traceNo || null;
    const merchantId = $event.merchantId || null;
    const cardHolderName = $event.cardHolderName || null;

    if( amount != undefined && amount != null && (!!this._target.billNo || !!this._target.templateId) ) {
      this.receiveBalance += amount;
      this.receives.push(
        { 
          receiveAmt: amount, 
          receiveTypeCode: type, 
          receiveTypeName: name,
          cardNo: cardNo,
          approveCode: approveCode,
          cardProvider: cardProvider,
          cardBankCode: cardBankCode,
          feeAmt: feeAmt,
          grossAmt : grossAmt,
          bankCharge: bankCharge,
          vatAmt: vatAmt,
          cardType: cardType,
          bankCode: bankCode,
          description: description,
          account: account,
          cardIssuerName: cardIssuerName,
          terminalId: terminalId,
          traceNo: traceNo,
          merchantId: merchantId,
          cardHolderName: cardHolderName
        }
      );
      this.makePayment();
    }
  }

  makePayment() {
    if((this.balance - this.receiveBalance) <= 0) {
      this.isError = false;
      this.service.makePayment({
          code: this._target.target.value,
          codeType: this._target.target.codeType,
          billNo: this._target.billNo,
          templateId: this._target.templateId,
          billAmt: this._target.netAmt,
          receives: this.receives,
          discountCode: this._target.discountCode
      }).subscribe( result => {
            if(!!result.receiveNo) {
              this._target.balAmt = result.billBalanceAmt || 0;
              this._target.balance = result.balance || 0;
              this._target.receives = this.receives || [];
              this._target.receiveNo = result.receiveNo;
              this._target.receiveAmt = this.receiveBalance || 0;
              this.as.success('รับชำระสำเร็จ');
              this.router.navigate(['fn/fndt02/balance'], { state: this._target, replaceUrl: true });
            } else {
              this.as.success('เกิดข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ');
            }
      }, err => {
        this.isError = true;
      }); 
    };
  }
  deleteReceive(index: number, amount: number) {
    this.modal.confirm("message.FN00085").subscribe((res) => {
      if (!!res) {
        this.receiveBalance -= amount;
        this.receives.splice(index, 1);
        this.as.success("message.STD00014");
      }
    });
  }
}
