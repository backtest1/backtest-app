import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosComponent } from './pos.component';
import { AuthorizationGuard, CanDeactivateGuard } from '@app/core';
import { PosSummaryComponent } from './summary.component';
import { PosMainComponent } from './main.component';
import { FnPosResolverService } from './fn-pos-resolver.service';
import { PosBalanceComponent } from './balance.component';
import { CashierGuard } from './guards/cashier-guard.service';
import { FnTemplateResolverService } from './fn-template-resolver.service';
import { PosTemplateComponent } from './template.component';


const routes: Routes = [
  {
    path: 'fndt02',
    canActivate: [AuthorizationGuard, CashierGuard],
    canActivateChild: [AuthorizationGuard, CashierGuard],
    component: PosComponent,
    data: {
      code: 'fndt02'
    },
    children: [
      {
        path: '',
        component: PosMainComponent,
        runGuardsAndResolvers: 'always',
        resolve: { extras: FnPosResolverService},
        data: {
			    code: 'fndt02'
		    }
      },
      {
        path: 'summary',
        component: PosSummaryComponent,
        runGuardsAndResolvers: 'always',
        resolve: { extras: FnPosResolverService},
        data: {
			    code: 'fndt02'
		    }
      },
      {
        path: 'balance',
        component: PosBalanceComponent,
        runGuardsAndResolvers: 'always',
        resolve: { extras: FnPosResolverService},
        data: {
			    code: 'fndt02'
		    }
      },
      // {
      //   path: 'template',
      //   component: TemplateDetailComponent,
      //   runGuardsAndResolvers: 'always',
      //   resolve: { extras: FnTemplateDetailResolverService },
      //   data: {
			//     code: 'fndt02'
		  //   },
			// 	canDeactivate: [CanDeactivateGuard]
      //},
      {
        path: 'template/details',
        component: PosTemplateComponent,
        runGuardsAndResolvers: 'always',
        resolve: { extras: FnTemplateResolverService },
        data: {
			    code: 'fnrt25'
		    },
				canDeactivate: [CanDeactivateGuard]
      } 
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [FnPosResolverService, CashierGuard, FnTemplateResolverService]
})
export class PosRoutingModule { }
