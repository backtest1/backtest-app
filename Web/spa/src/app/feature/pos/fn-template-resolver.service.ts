import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { FnPosService } from './fn-pos.service';

@Injectable()
export class FnTemplateResolverService implements Resolve<any> {
  constructor( private router: Router, private service: FnPosService ) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const extras = this.router.getCurrentNavigation().extras.state;
    const details = extras && extras.code ? this.service.getTemplateDetails(extras.code)  : of(null);
    return forkJoin(this.service.getTemplateMaster(['educationTypeLevels']), details).pipe(map((result) => {
      const master = result[0];
      const details = result[1];
      return { master, details }
    }));
  }
}