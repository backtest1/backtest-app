import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazyTranslationService } from '@app/core';
import { SharedModule } from '@app/shared';
import { PosRoutingModule } from './pos-routing.module';
import { PosComponent } from './pos.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { CustomerDialogComponent } from './dialogs/customer-dialog/customer-dialog.component';
import { BillItemComponent } from './components/panel/bill-item/bill-item.component';
import { PosActionComponent } from './components/panel/action/action.component';
import { FnPosService } from './fn-pos.service';
import { CategoriesComponent } from './components/categories/categories.component';
import { PosItemsComponent } from './components/pos-items/pos-items.component';
import { DiscountDialogComponent } from './dialogs/discount-dialog/discount-dialog.component';
import { DiscountDialogResolverService } from './dialogs/discount-dialog/discount-dialog-resolver.service';
import { PosSummaryComponent } from './summary.component';
import { PosMainComponent } from './main.component';
import { PosCustomerComponent } from './components/panel/customer/customer.component';
import { PosReceiveItemsComponent } from './components/receive-items/receive-items.component';
import { PosNumberComponent } from './components/pos-number/pos-number.component';
import { CashDialogComponent } from './dialogs/cash-dialog/cash-dialog.component';
import { PosBalanceComponent } from './balance.component';
import { PosReceiptItemsComponent } from './components/receipt-items/receipt-items.component';
import { AccordionModule } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { TemplatesItemsComponent } from './components/panel/template-items/template-items.component';
import { PosTemplateActionsComponent } from './components/panel/template-actions/template-actions.component';
import { AccountDialogComponent } from './dialogs/account-dialog/account-dialog.component';
import { AccountDialogResolverService } from './dialogs/account-dialog/account-dialog-resolver.service';
import { PosToolbarComponent } from './components/pos-toolbar/pos-toolbar.component';
import { CreditDialogComponent } from './dialogs/credit-dialog/credit-dialog.component';
import { CreditDialogResolverService } from './dialogs/credit-dialog/credit-dialog-resolver.service';
import { PosTemplateComponent } from './template.component';
import { TransferDialogComponent } from './dialogs/transfer-dialog/transfer-dialog.component';
import { TransferDialogResolverService } from './dialogs/transfer-dialog/transfer-dialog-resolver.service';
import { QrDialogComponent } from './dialogs/qr-dialog/qr-dialog.component';

@NgModule({
  entryComponents: [
    CustomerDialogComponent,
    DiscountDialogComponent,
    CashDialogComponent,
    AccountDialogComponent,
    CreditDialogComponent,
    TransferDialogComponent,
    QrDialogComponent
  ],
  declarations: [
    PosComponent,
    AutocompleteComponent,
    CustomerDialogComponent,
    BillItemComponent,
    PosActionComponent,
    CategoriesComponent,
    PosItemsComponent,
    DiscountDialogComponent,
    PosSummaryComponent,
    PosMainComponent,
    PosCustomerComponent,
    PosReceiveItemsComponent,
    PosNumberComponent,
    CashDialogComponent,
    PosBalanceComponent,
    PosReceiptItemsComponent,
    TemplatesItemsComponent,
    PosTemplateActionsComponent,
    AccountDialogComponent,
    PosToolbarComponent,
    CreditDialogComponent,
    PosTemplateComponent,
    TransferDialogComponent,
    QrDialogComponent
  ],
  imports: [
    CommonModule,
    PosRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    AccordionModule.forRoot(),
    TranslateModule
  ],
  providers: [
    FnPosService,
    DiscountDialogResolverService,
    AccountDialogResolverService,
    CreditDialogResolverService,
    TransferDialogResolverService
  ]
})
export class PosModule {
    constructor(private lazy: LazyTranslationService) {
		lazy.add('fn');
	}
}
