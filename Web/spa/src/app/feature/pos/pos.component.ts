import { Component, OnInit, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FnPosService, PosPayload } from './fn-pos.service';

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosComponent implements OnInit, OnDestroy {
  
  // items = [];
  // bill = new BehaviorSubject<any>(new PosPayload());
  // _target = new PosPayload();

  constructor( private service: FnPosService ) { 

  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {

  }

  // onSearchStudentFrom = (term, value) => {
  //   return this.service.getStudentAutocomplete({ value: value, keyword: term });
  // }

  // onStudentSelected(context: any) {
  //   if(!!context) {
  //     Object.assign(this._target, { target: context });
  //     this.service.getBillByCode(context.codeType, context.value).subscribe( res => {
  //       this.items = res;
  //     });
  //   } else {
  //     this.bill.next(new PosPayload());
  //     this.items = [];
  //   }
  // }

  // onItemSelected(bill) {
  //   Object.assign(this._target, bill);
  //   this.bill.next(this._target);
  // }
  
}
