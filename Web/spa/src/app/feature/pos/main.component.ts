import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService, SaveDataService } from '@app/core';
import { ModalService, SubscriptionDisposer } from '@app/shared';
import { BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FnPosService, PosPayload } from './fn-pos.service';

@Component({
  selector: 'app-pos-main',
  templateUrl: './main.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosMainComponent extends SubscriptionDisposer implements OnInit {
  
  items = [];
  bill = new BehaviorSubject<any>(new PosPayload());
  _target = new PosPayload();
  currentKeyword = null;
  categories : any[] = [
    { text: 'ใบแจ้งชำระเงิน', value: 'B', id: 1, color: '#fff' },
    { text: 'กำหนดเอง', value: 'T', id: 2, color: '#fff' },
    { text: 'ทั้งหมด', value: null, id: 0, color: '#fff' }
  ];
  categoryControl = new FormControl('B');

  constructor( activatedRoute: ActivatedRoute, private service: FnPosService, private router: Router, private as : MessageService ) { 
    super();
    activatedRoute.data.subscribe( extras => {
      if(!!extras.extras) {
        this._target = extras.extras;
        console.log(this._target);
        if(!!this._target.target) {
          this.bill.next(extras.extras);
        }      
      }
    });
  }

  getByCategory(keyword: string, categoryType: string = null) {
    if(!!this._target.target) {
      this.service.getBillByCode(this._target.target.codeType, this._target.target.value, this._target.target.educationTypeLevel, keyword, categoryType).subscribe( res => {
        this.items = res;
      });
    } else {
      this.service.getBillByCode(null, null, null, keyword, categoryType).subscribe( res => {
        this.items = res;
      });
    }

  }

  ngOnInit(): void {
    this.categoryControl.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe( category => {
      this.getByCategory(this.currentKeyword, category);
    });
    this.onCategorySelect('B');
  }

  onCategorySelect(categoryType: string = null) {
    this.categoryControl.setValue(categoryType);
    // this.getByCategory(this.currentKeyword, categoryType);
  }

  onSearch(keyword) {
    this.currentKeyword = keyword;
    this.getByCategory(keyword, this.categoryControl.value);
  }

  onSearchStudentFrom = (term, value) => {
    return this.service.getStudentAutocomplete({ value: value, keyword: term });
  }

  onStudentSelected(context: any) {
    if(!!context) {
      Object.assign(this._target, { target: context });
      this.onCategorySelect('B');
    } else {
      this.bill.next(new PosPayload());
      this.items = [];   
      this.currentKeyword = null;
      this._target = {} as PosPayload;
      this.categoryControl.reset(null, { emitEvent: false });
    }
  }

  onItemSelected(bill) {
    if(!!this._target.target && !!this._target.target.value) {
      Object.assign(this._target, bill);
      this._target.billAmt = this._target.netAmt;
      this.bill.next(this._target);
    } else {
      this.as.warning('กรุณาเลือกนักศึกษา');
    }
  }

  goHome() {
    this.router.navigate(['/dashboard'], { replaceUrl: true });  
  }

  manageTemplate() {
    this.router.navigate(['fn/fndt02/template']);   
  }
  
}
