import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable()
export class FnPosResolverService implements Resolve<any> {
  constructor( private router: Router ) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const bill = this.router.getCurrentNavigation().extras.state;
    return of(bill);
  }
}