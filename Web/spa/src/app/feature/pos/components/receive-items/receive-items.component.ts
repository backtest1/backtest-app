import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, AfterContentInit, ViewChild, OnDestroy } from '@angular/core';
import { ModalService, ParameterService, Size, SubscriptionDisposer } from '@app/shared';
import { CashDialogComponent } from '../../dialogs/cash-dialog/cash-dialog.component';
import { CreditDialogResolverService } from '../../dialogs/credit-dialog/credit-dialog-resolver.service';
import { CreditDialogComponent } from '../../dialogs/credit-dialog/credit-dialog.component';
import { FnPosService, PosPayload } from '../../fn-pos.service';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { merge, of, Subject, Subscription } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import Swal from 'sweetalert2';
import { TransferDialogComponent } from '../../dialogs/transfer-dialog/transfer-dialog.component';
import { TransferDialogResolverService } from '../../dialogs/transfer-dialog/transfer-dialog-resolver.service';
import { QrDialogComponent } from '../../dialogs/qr-dialog/qr-dialog.component';

@Component({
    selector: 'pos-receive-items',
    templateUrl: './receive-items.component.html',
    styleUrls: ['./receive-items.component.scss']
})
export class PosReceiveItemsComponent extends SubscriptionDisposer implements OnInit {

    @ViewChild('creditSwal') private creditSwal: SwalComponent;
    @ViewChild('qrcodeSwal') private qrcodeSwal: SwalComponent;
    
    items : any[] = [];
    @Input() payload: PosPayload = new PosPayload();
    @Output() onCollected = new EventEmitter();
    constructor(private service: FnPosService, private modal: ModalService, private ps: ParameterService) {
        super();
    }
    fnPosReceiveTypes = new Map<string, string>();
    edcState = EdcState.Normal;
    collection =  { 
        type: null, 
        amount: 0, 
        name: null, 
        cardBankCode: null, 
        cardProvider: null,
        chargeAmt: 0,
        bankCharge: 0,
        grossAmt: 0,
        vatAmt: 0,
        cardType: null,
        bankCode: null,
        accountName: null,
        description: null,
        terminalId: null,
        traceNo: null,
        merchantId: null,
        cardIssuerName: null,
        cardHolderName: null
    };

    @Input() balance = 0;

    ngOnInit() {
        this.service.getReceives().subscribe( receives => {
            this.items = receives || [];
        });
        this.ps.getParameters('FnPosReceiveType').subscribe( parameters => {
            if(!!parameters) {
                parameters.forEach(params => {
                    if(!!params.parameterCode && !!params.parameterValue) {
                        this.fnPosReceiveTypes.set(params.parameterCode, params.parameterValue);
                    }                       
                });
            }
        });
        this.connect();
        this.qrcodeSwal.cancel.subscribe( () => {
            this.edcState = EdcState.Normal;
        });
    }

    protected ngUnsubscribe: Subject<void> = new Subject<void>();
    protected socketReady: Subject<void> = new Subject<void>();
    protected retry: Subject<void> = new Subject<void>();
    private isConnecting = false;
    isConnected = false;
    private socket: WebSocketSubject<any>;
    private socketSubscription: Subscription;

    devices: any[] = [];

    connect() {
        merge(this.retry.asObservable()).pipe(startWith(of(true)), takeUntil(this.ngUnsubscribe)).subscribe( () => {
            try {
                this.socket = webSocket({ url: `ws://127.0.0.1:4000/`});             
                this.isConnecting = true;
                this.socketReady.next();
                this.socketReady.complete();
                this.socketSubscription = this.socket.asObservable().subscribe( res => {
                    if( res.command_name === 'initialize' && res.responseType === 'query' ) {
                        const __devivces: any[] = res.data;
                        if(!!__devivces) {
                            this.devices = __devivces.map(device => {
                                return Object.assign(device, {
                                    text: device.deviceId,
                                    value: device.deviceId,
                                });
                            });
                        }
                        this.isConnected = true;
                    } else {
                        this.creditSwal.nativeSwal.close(); 
                        this.qrcodeSwal.nativeSwal.close();
                        if(!!res.data && res.data.responseCode === '00') {
                            const feilds = res.data.fields as any[];
                            if(!!feilds) {
                                const approveCode = this.getField(feilds, '01');
                                const merchantName = this.getField(feilds, 'D0');
                                const merchantId = this.getField(feilds, 'D1');
                                const cardIssuerName = this.getField(feilds, 'D2');
                                const cardNo = this.getField(feilds, '30');  
                                const cardHolderName = this.getField(feilds, '26');
                                const traceNo = this.getField(feilds, '65');
                                const terminalId = this.getField(feilds, '16');
                                this.onCollected.emit(Object.assign(this.collection, {
                                    approveCode: approveCode,
                                    cardNo: cardNo,
                                    merchantName: merchantName,
                                    cardIssuerName: cardIssuerName,
                                    terminalId: terminalId,
                                    traceNo: traceNo,
                                    merchantId: merchantId,
                                    cardHolderName: cardHolderName
                                }));                            
                            } else {
                                this.onCollected.emit(this.collection);
                            }                         
                        } else {
                            this.showSwalError(res.data.responseCodeMsg);
                        }                       
                    }
                }, error => {
                  this.connectFailed();
                }, () => {
                    console.log('Disconnected...');
                    this.isConnected = false;
                });                
            } catch (error) {
                this.connectFailed();
            }
        });      
    }
    getField(feilds: any[], target: string) {
        const feild = feilds.find( o => o.type == target);
        if(!!feild) {
            return feild.value || null;
        }  
        return null;
    }
    send(commandName: string, type: string, data: any) {
        if(this.isConnecting === true) {
            this.socket.next({
                command_name: commandName,
                type,
                data
            });
        }
    }
  
    private retryConnect() {
        if(!this.isConnecting) {
          this.retry.next();
        }
    }

    private connectFailed() {
        this.isConnected = false;
        this.isConnecting = false;
        this.creditSwal.nativeSwal.close(); 
        this.qrcodeSwal.nativeSwal.close();
        if(!!this.socketSubscription)
            this.socketSubscription.unsubscribe();
        setTimeout(() => {
            this.retryConnect();
        }, 10000);   
    }

      
    collect(type: string, name: string = '') {
        if(this.isConnected == false && type != this.fnPosReceiveTypes.get('Cash') && type != this.fnPosReceiveTypes.get('Transfer')) {
            return;
        }
        if(type == this.fnPosReceiveTypes.get("Cash")) {
            this.modal.openComponent(CashDialogComponent, Size.medium, { price: this.balance, title: 'label.FNDT02.ReceiveWithCash' }).subscribe( response => {               
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: null, 
                    cardProvider: null,
                    chargeAmt: 0,
                    bankCharge: 0,
                    grossAmt: response.amount >= this.balance ? this.balance : response.amount,
                    vatAmt: 0,
                    cardType: null,
                    bankCode: null,
                    accountName: null,
                    description: response.description,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                }
                this.onCollected.emit(this.collection);
            });  
        } else if(type == this.fnPosReceiveTypes.get("Credit")) {
            this.modal.openComponent(CreditDialogComponent, Size.medium, { price: this.balance, type: type, devices: this.devices }, CreditDialogResolverService).subscribe( response => {                
                if(response.amount === 0) return;    
                response.amount = response.amount >= this.balance ? this.balance : response.amount; 
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: response.bank, 
                    cardProvider: response.cardProvider,
                    chargeAmt: response.chargeAmt,
                    grossAmt: response.amount,
                    bankCharge: response.bankCharge,
                    vatAmt: response.vatAmt,
                    cardType: 'CREDIT',
                    bankCode: null,
                    accountName: null,
                    description: null,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null 
                };
                this.creditSwal.show();
                this.send("transaction_sale", "credit", {
                    amount: response.amount || 0,
                    ref1: this.payload.referenceNo1 || '0',
                    ref2: this.payload.referenceNo2 || '0'
                });
            });  
        } else if(type == this.fnPosReceiveTypes.get("Debit")) {        
            this.modal.openComponent(CreditDialogComponent, Size.medium, { price: this.balance, type: type, devices: this.devices }, CreditDialogResolverService).subscribe( response => {
                if(response.amount === 0) return;    
                response.amount = response.amount >= this.balance ? this.balance : response.amount; 
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: response.bank, 
                    cardProvider: response.cardProvider,
                    chargeAmt: response.chargeAmt,
                    grossAmt: response.amount,
                    bankCharge: response.bankCharge,
                    vatAmt: response.vatAmt,
                    cardType: 'DEBIT',
                    bankCode: null,
                    accountName: null,
                    description: null,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                };
                this.creditSwal.show();
                this.send("transaction_sale", "credit", {
                    amount: response.amount || 0,
                    ref1: this.payload.referenceNo1 || '0',
                    ref2: this.payload.referenceNo2 || '0'
                });
            });  
        } else if(type == this.fnPosReceiveTypes.get("Alipay")) {
            this.modal.openComponent(QrDialogComponent, Size.medium, { price: this.balance, name: name }).subscribe( response => {  
                if(response.amount === 0) return;    
                response.amount = response.amount >= this.balance ? this.balance : response.amount;          
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: null, 
                    cardProvider: null,
                    chargeAmt: 0,
                    grossAmt: response.amount,
                    bankCharge: 0,
                    vatAmt: 0,
                    cardType: 'QR',
                    bankCode: null,
                    accountName: null,
                    description: response.description,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                };
                this.qrcodeSwal.show();
                this.send("transaction_sale", "alipay", {
                    amount: response.amount || 0,
                    ref1: this.payload.referenceNo1 || '0',
                    ref2: this.payload.referenceNo2 || '0'
                });
            }); 
        } else if(type == this.fnPosReceiveTypes.get("WeChat")) {
            this.modal.openComponent(QrDialogComponent, Size.medium, { price: this.balance, name: name }).subscribe( response => { 
                if(response.amount === 0) return;    
                response.amount = response.amount >= this.balance ? this.balance : response.amount;              
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: null, 
                    cardProvider: null,
                    chargeAmt: 0,
                    bankCharge: 0,
                    grossAmt: response.amount,
                    vatAmt: 0,
                    cardType: 'QR',
                    bankCode: null,
                    accountName: null,
                    description: response.description,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                };
                this.qrcodeSwal.show();
                this.send("transaction_sale", "wechat", {
                    amount: response.amount || 0,
                    ref1: this.payload.referenceNo1 || '0',
                    ref2: this.payload.referenceNo2 || '0'
                });
            }); 
        } else if(type == this.fnPosReceiveTypes.get("QR")) {
            this.modal.openComponent(QrDialogComponent, Size.medium, { price: this.balance, name: name }).subscribe( response => {    
                if(response.amount === 0) return;    
                response.amount = response.amount >= this.balance ? this.balance : response.amount;           
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: null, 
                    cardProvider: null,
                    chargeAmt: 0,
                    bankCharge: 0,
                    grossAmt: response.amount,
                    vatAmt: 0,
                    cardType: 'QR',
                    bankCode: null,
                    accountName: null,
                    description: response.description,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                };
                this.qrcodeSwal.show();
                this.send("transaction_sale", "qrcode", {
                    amount: response.amount || 0,
                    ref1: this.payload.referenceNo1 || '0',
                    ref2: this.payload.referenceNo2 || '0'
                }); 
            }); 
        } else if(type == this.fnPosReceiveTypes.get("Transfer")) {        
            this.modal.openComponent(TransferDialogComponent, Size.medium, { price: this.balance, type: type }, TransferDialogResolverService).subscribe( response => {
                if(response.amount === 0) return; 
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: null, 
                    cardProvider: null,
                    chargeAmt: 0,
                    bankCharge: 0,
                    grossAmt: response.amount,
                    vatAmt: 0,
                    cardType: null,
                    bankCode:  response.account,
                    accountName:  response.accountName,
                    description: response.description,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                };
                this.onCollected.emit(this.collection);
            });  
        } else if(type == this.fnPosReceiveTypes.get("QRCredit")) {
            this.modal.openComponent(QrDialogComponent, Size.medium, { price: this.balance, name: name }).subscribe( response => {    
                if(response.amount === 0) return;    
                response.amount = response.amount >= this.balance ? this.balance : response.amount;           
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: null, 
                    cardProvider: null,
                    chargeAmt: 0,
                    bankCharge: 0,
                    grossAmt: response.amount,
                    vatAmt: 0,
                    cardType: 'QR',
                    bankCode: null,
                    accountName: null,
                    description: response.description,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                };
                this.qrcodeSwal.show();
                this.send("transaction_sale", "qrcredit", {
                    amount: response.amount || 0,
                    ref1: this.payload.referenceNo1 || '0',
                    ref2: this.payload.referenceNo2 || '0'
                }); 
            }); 
        } else if(type == this.fnPosReceiveTypes.get("Installment")) {
            this.modal.openComponent(CashDialogComponent, Size.medium, { price: this.balance, title: 'label.FNDT02.ReceiveWithInstallment' }).subscribe( response => {               
                this.collection = { 
                    type: type, 
                    amount: response.amount, 
                    name: name, 
                    cardBankCode: null, 
                    cardProvider: null,
                    chargeAmt: 0,
                    bankCharge: 0,
                    grossAmt: response.amount >= this.balance ? this.balance : response.amount,
                    vatAmt: 0,
                    cardType: null,
                    bankCode: null,
                    accountName: null,
                    description: response.description,
                    terminalId: null,
                    traceNo: null,
                    merchantId: null,
                    cardIssuerName: null,
                    cardHolderName: null
                }
                this.onCollected.emit(this.collection);
            });  
        }
    }

    creditTemplate = `
    <div class="p-4">
      <i class="fas fa-credit-card fa-7x"></i>
    </div>
    <div>
        <i class="fas fa-circle-notch fa-spin"></i> Wait for inserting credit card.
    </div>
    `
    qrTemplate = `
    <div class="p-4">
      <i class="fas fa-qrcode fa-7x"></i>
    </div>
    <div>
        <i class="fas fa-circle-notch fa-spin"></i> Wait for scaning qrcode.
    </div>
    `
    showSwalError(text: string) {       
      Swal.fire({
        allowEscapeKey: true,
        allowOutsideClick: true,
        showCloseButton: false,
        showConfirmButton: true,
        showCancelButton: false,
        titleText: 'รายการถูกยกเลิก',
        text: text,
        cancelButtonColor: '#F00',
        confirmButtonColor: '#F00',
        type: 'error'
      });
    }
}

export enum EdcState {
    Wating = 0,
    Cancel = 1,
    Complete = 2,
    Normal = 4
}