import { OnInit, Input, Optional, Self } from '@angular/core';
import { ControlValueAccessor, NgControl, ControlContainer, FormGroupDirective, FormControl } from '@angular/forms';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

export class InputBaseComponent implements OnInit, ControlValueAccessor {

  @Input() formControlName;
  @Input() label: string = '';
  @Input() inline: boolean = false;
  protected defaultValue = null;
  protected innerValue = null;

  @Input()
  get required(): boolean { return this._required; }
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
  }
  private _required = false;

  @Input()
  get placeholder(): string { return this._placeholder; }
  set placeholder(value: string) {
    this._placeholder = value;
  }
  private _placeholder: string;

  @Input()
  get disabled(): boolean { return this._disabled; }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
  }
  private _disabled = false;

  constructor( @Optional() @Self() public _ngControl: NgControl, @Optional() private _fg: ControlContainer ) {
    if (!!this._ngControl) {
      this._ngControl.valueAccessor = this;
    }
  }

  get ngControl() {
    if(!!this._ngControl) {
      return this._ngControl;
    }
    return { control : new FormControl()};
  }

  ngOnInit() {
    this.setOnSubmitValidation();
  }

  protected setOnSubmitValidation() {
    this.ngControl.control.patchValue( this.innerValue, { emitEvent: false });
    const formDirective: FormGroupDirective = (<FormGroupDirective> this._fg);
    if (formDirective !== null && this._ngControl !== null) {
      this._fg.formDirective['ngSubmit'].subscribe( () => {
        this._ngControl.control.markAsTouched();
      });
    }
  }

  protected onChange = (value) => {}

  protected onTouched = () => {}

  get value() {
    if(!!this._ngControl) {
      return this._ngControl.control.value || this.defaultValue;
    } else {
      return this.defaultValue;
    }
  }

  writeValue(obj: any): void { 
    this.innerValue = obj || this.defaultValue;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}