export interface FnAutocompleteRequest<T> {
    keyword: string;
    value: T;
}