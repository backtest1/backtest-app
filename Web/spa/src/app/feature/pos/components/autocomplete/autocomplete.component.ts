import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, AfterContentInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, of, concat } from 'rxjs';
import { debounceTime, switchMap, catchError, tap, filter } from 'rxjs/operators';
import { ModalService, Size } from '@app/shared';
import { CustomerDialogComponent } from '../../dialogs/customer-dialog/customer-dialog.component';

@Component({
  selector: 'pos-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit, AfterContentInit {

  ngAfterContentInit() {
    this.open();
  }

  @Input() onSearch: (term: any, value?: any) => Observable<any[]>;
  items: Observable<any[]>;
  focus = false;
  @Input() value = null;
  searchInput = new FormControl()

  @Output() onSelected = new EventEmitter();

  constructor(private modal: ModalService) { 
    
  }

  ngOnInit() {
    this.items = concat(
      this.onSearch(null, null).pipe(
        tap(items => {
          // if (this.modelChange) this.modelChange.next(this.value ? items[0] : {})
        })
      ),
      this.searchInput.valueChanges.pipe(
        debounceTime(200),
        switchMap(term => this.onSearch(term).pipe(
          catchError(() => of([]))
        ))
      )
    )
  }

  open() {
    if(!this.focus) { 
      this.focus = true;
      setTimeout(()=>{    
        document.getElementById("search-input").focus();
      }, 0);  
    }
  }

  close(event) {
    setTimeout(()=>{ 
      event.stopPropagation();   
      this.focus = false;
      this.searchInput.reset();
    }, 300);
  }
  selected(item) {
    if(!!this.value) {
      this.modal.confirm('ต้องการที่จะเลือกข้อมูลใหม่หรือไม่?').subscribe( async (result) => {
        if(!!result) {
          this.onSelecting(item);
        } else {
          this.open()
        }
      });
    } else {
      this.onSelecting(item);
    }
  }
  onSelecting(item) {
    this.value = item;
    this.onSelected.emit(this.value);
  }
  clear() {
    this.modal.confirm('ต้องการที่จะลบข้อมูลใช่หรือไม่?').subscribe( async (result) => {
      if(!!result) {
        this.value = null;
        this.onSelected.emit(this.value);
      }     
    });
  }
  add() {
    // const modal = this.modal.open(CustomerDialogComponent, Size.medium);
    // modal.content.onClose.subscribe( result => {
    //   this.value = result;
    //   this.onSelected.emit(result);
    // });
  }
}
