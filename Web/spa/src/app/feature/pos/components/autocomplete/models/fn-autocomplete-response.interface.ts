export interface FnAutocompleteResponse<T> {
    text: string;
    value: T;
}