import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DiscountDialogResolverService } from '@app/feature/pos/dialogs/discount-dialog/discount-dialog-resolver.service';
import { DiscountDialogComponent } from '@app/feature/pos/dialogs/discount-dialog/discount-dialog.component';
import { FnPosService, PosPayload } from '@app/feature/pos/fn-pos.service';
import { ModalService, Size } from '@app/shared';
import { BehaviorSubject, Subscription } from 'rxjs';


@Component({
  selector: 'pos-panel-bill-item',
  templateUrl: './bill-item.component.html',
  styleUrls: ['./bill-item.component.scss']
})
export class BillItemComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() bill: BehaviorSubject<any> = new BehaviorSubject<any>(new PosPayload());
  @Input() state: string = 'MAIN';
  subscription: Subscription;
  details: PosPayload = new PosPayload();
  discountDialogResolverService = DiscountDialogResolverService;
  
  constructor(private modal: ModalService, private service: FnPosService) { 
    
  }

  _netAmt = null;
  _promotion = null;
  _discountAmt = null;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngAfterViewInit(): void {

  }

  ngOnInit() {
    this.subscription = this.bill.subscribe( details => {
      this.details = details;
      if(!!details.billNo || !!details.templateId) {
        if(this._netAmt == null) {
          this._netAmt = this.details.netAmt;
        }     
        if(this._promotion == null) {
          this._promotion = this.details.promotions;
        }  
        if(this._discountAmt == null) {
          this._discountAmt = this.details.discountAmt;
        }  
      } else {
        this._netAmt = null;
        this._promotion = null;
        this._discountAmt = null;
      }
    });
  }

  clear() {
    this.modal.confirm('ต้องการที่จะเลือกข้อมูลใหม่หรือไม่?').subscribe( async (result) => {
      if(!!result) {
        this.bill.next(Object.assign(new PosPayload(), { target: this.details.target }));
      }
    });    
  }
  
  addDiscount() {
    const modal = this.modal.openComponent(DiscountDialogComponent, Size.medium, {}, this.discountDialogResolverService, {});
    modal.subscribe( discounts => {
      if(!!this.details.promotions && !!discounts && !!discounts.selected) {
        const discountCodes = discounts.selected.map((o) => {
          return { 
            discountCode: o.discountCode, 
            discountRate: o.discountRate || 0
          };
        });
        this.details.discountCode = discountCodes;
        this.service.calculateDiscount(this._netAmt, discountCodes).subscribe( res => {
          this.details.netAmt = res.amount;
          this.details.promotions = this._promotion.concat(res.discounts);
          this.details.discountAmt = this._discountAmt + res.discountAmt;
          this.bill.next(this.details);
        });
      }    
    });
  }

  delDiscount(index: number, discount: any) {
    this.modal.confirm('ต้องการที่จะลบส่วนลดนี้หรือไม่?').subscribe( async (result) => {
      if(!!result) {
        const i = this.details.discountCode.findIndex( o => o == discount.discountCode );
        this.details.discountCode.splice(i, 1);
        this.service.calculateDiscount(this._netAmt, this.details.discountCode).subscribe( res => {
          this.details.netAmt = res.amount;
          this.details.promotions = this._promotion.concat(res.discounts);
          this.details.discountAmt = this._discountAmt + res.discountAmt;
          this.bill.next(this.details);
        });
      }
    });
  }

}