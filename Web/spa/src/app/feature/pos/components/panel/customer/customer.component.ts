import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from '@app/core';
import { FnPosService } from '@app/feature/pos/fn-pos.service';
import { BsModalRef } from 'ngx-bootstrap';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';


@Component({
  selector: 'pos-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class PosCustomerComponent implements OnInit {

  target: any = {};
  subscription: Subscription;
  @Input() bill: BehaviorSubject<any> = new BehaviorSubject<any>({});
  @Input() state: string = 'MAIN';

  constructor(private as: MessageService, private service: FnPosService, private router: Router) { 

  }

  ngAfterViewInit(): void {

  }

  ngOnInit() {
    this.subscription = this.bill.subscribe( details => {
      this.target = details;
    });
  }


}