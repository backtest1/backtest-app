import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '@app/core';
import { FnPosService } from '@app/feature/pos/fn-pos.service';
import { BsModalRef } from 'ngx-bootstrap';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';


@Component({
  selector: 'pos-template-actions',
  templateUrl: './template-actions.component.html',
  styleUrls: ['./template-actions.component.scss']
})
export class PosTemplateActionsComponent implements OnInit {


  @Input() summary = {
    totalAmt: 0,
    totalQty: 0
  };
  @Output() saving = new EventEmitter();


  constructor(private as: MessageService, private service: FnPosService, private router: Router) { 

  }

  ngOnInit() {

  }

  finish() {
    this.saving.emit();
  }

}