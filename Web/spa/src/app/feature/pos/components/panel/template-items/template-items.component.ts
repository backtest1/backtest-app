import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { DiscountDialogResolverService } from '@app/feature/pos/dialogs/discount-dialog/discount-dialog-resolver.service';
import { DiscountDialogComponent } from '@app/feature/pos/dialogs/discount-dialog/discount-dialog.component';
import { FnPosService, PosPayload } from '@app/feature/pos/fn-pos.service';
import { ModalService, Size } from '@app/shared';
import { BehaviorSubject, Subscription } from 'rxjs';


@Component({
  selector: 'pos-panel-template-items',
  templateUrl: './template-items.component.html',
  styleUrls: ['./template-items.component.scss']
})
export class TemplatesItemsComponent implements OnInit, AfterViewInit {

  @Input() items : FormArray = new FormArray([]);


  constructor(private modal: ModalService, private fb: FormBuilder, private service: FnPosService) { 
    
  }

  ngAfterViewInit(): void {

  }

  ngOnInit() {

  }

  clear(index: number) {
    this.modal.confirm('ต้องการที่จะเลือกข้อมูลใหม่หรือไม่?').subscribe( async (result) => {
      if(!!result) {

      }
    });    
  }
  
}