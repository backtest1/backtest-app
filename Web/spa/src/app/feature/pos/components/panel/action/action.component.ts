import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from '@app/core';
import { FnPosService } from '@app/feature/pos/fn-pos.service';
import { SubscriptionDisposer } from '@app/shared';
import { BsModalRef } from 'ngx-bootstrap';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';


@Component({
  selector: 'pos-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class PosActionComponent extends SubscriptionDisposer implements OnInit {

  target: any = {};
  @Input() bill: BehaviorSubject<any> = new BehaviorSubject<any>({});
  @Input() state: string = 'MAIN';
  amount = new FormControl();

  constructor(private as: MessageService, private service: FnPosService, private router: Router) { 
    super();
  }

  ngOnInit() {
    this.bill.pipe(takeUntil(this.ngUnsubscribe)).subscribe( details => {
      this.target = details;
    });
    this.bill.pipe(first()).subscribe( details => {
      this.amount.patchValue(this.target.netAmt || 0, { onlySelf: true, emitEvent: false });
    });
    this.amount.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe( amount => {      
      this.target.netAmt = (amount || 0) > 0 ? (amount || 0) : 0 ;
      this.bill.next(this.target);
    });
  }

  makePayment() {
    if(!!this.target.billNo || !!this.target.templateId) {
      if(this.state === 'MAIN') {
        this.router.navigate(['fn/fndt02/summary'], { state: this.target });
      } else if(this.state === 'SUMMARY') {     
 
      }
    } else {
      this.as.error('กรุณาเลือกรายการรับชำระ');
    }
  }

  finish() {
    this.router.navigate(['fn/fndt02'], { replaceUrl: true });
  }

}