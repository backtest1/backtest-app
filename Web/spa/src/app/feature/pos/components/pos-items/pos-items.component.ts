import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { FnPosService } from '../../fn-pos.service';

@Component({
    selector: 'pos-items',
    templateUrl: './pos-items.component.html',
    styleUrls: ['./pos-items.component.scss']
})
export class PosItemsComponent implements OnInit {
    
    @Input() category: string = 'ALL';
    @Input() items : any[] = [];
    @Output() selected = new EventEmitter();
    @Input() type: string = 'BILL';

    constructor(private service: FnPosService, private router: Router) {

    }
    ngOnInit() {

    }
    selectedBill( bill, billNo: string ) {
        if(bill.documentType === 'B') {
            this.service.getBillDetails({billNo, templateId: null}).subscribe( details => {
                const promotions = !!details.promotions ? details.promotions as any[] : [];
                bill.discountAmt = promotions.reduce((sum, promotion) => {
                    return sum + Number( !!promotion.discountAmt ? promotion.discountAmt : 0);
                }, 0);               
                this.selected.emit(Object.assign(bill, details));
            });
        } else {
            this.service.getBillDetails({billNo: null, templateId: bill.templateId}).subscribe( items => {
                this.selected.emit(Object.assign(bill, items, { promotions: [] }));
            });          
        }
    }
    selectedItem(item) {
        this.selected.emit(item);
    }
    goTemplate() {
        this.router.navigate(['/fn/fndt02/template/details']);  
    }
}