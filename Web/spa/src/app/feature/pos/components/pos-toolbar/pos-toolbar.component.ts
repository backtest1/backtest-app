import { Component, Input, OnInit } from '@angular/core';
import { I18nService, SupportedLanguages, AuthService, OrganizationService } from '@app/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    selector: 'pos-toolbar',
    templateUrl: './pos-toolbar.component.html',
    styleUrls: ['./pos-toolbar.component.scss']
})
export class PosToolbarComponent implements OnInit {

    @Input() showBack = false;
    
    thai = SupportedLanguages.Thai;
    eng = SupportedLanguages.Eng;
  
    constructor(
      private authService:AuthService,
      public orgService: OrganizationService,
      private router:Router,
      private location: Location,
      public i18n: I18nService) { }

    ngOnInit(): void {
 
    }
  
    ngOnDestroy(){
      this.orgService.destroy();
    }
    
    signOut() {
      this.authService.signout();
    }

    back() {
        this.location.back();
    }
  
}