export interface PosCategory {
    name: string;
    type: string;
    id: number;
    color: string;
}