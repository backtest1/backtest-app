import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, AfterContentInit } from '@angular/core';
import { PosCategory } from './categories.interface';


@Component({
    selector: 'pos-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
    @Input() current = null;
    @Output() onSelected = new EventEmitter();
    categories : PosCategory[] = [
        { name: 'ใบแจ้งชำระเงิน', type: 'B', id: 1, color: '#fff' },
        { name: 'กำหนดเอง', type: 'T', id: 2, color: '#fff' },
        { name: 'ทั้งหมด', type: null, id: 0, color: '#fff' }
    ];
    ngOnInit() {

    }
    selected(type: string) {
        this.onSelected.emit(type);
    }
}