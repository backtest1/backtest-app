import { Component,Output, EventEmitter } from '@angular/core';
import { ModalService } from '@app/shared';
import { FnPosService } from '../../fn-pos.service';

@Component({
    selector: 'pos-receipt-items',
    templateUrl: './receipt-items.component.html',
    styleUrls: ['./receipt-items.component.scss']
})
export class PosReceiptItemsComponent  {
   
    items : any[] = [
        { exportType: 'printer', exportName: 'label.FNDT02.PrintReceipt', icon: 'fas fa-print fa-5x' },
        { exportType: 'email', exportName: 'label.FNDT02.SendEmail', icon: 'fas fa-envelope fa-5x' }
    ];
    @Output() onExport = new EventEmitter();
    constructor(private service: FnPosService, private modal: ModalService) {

    }

    export(type: string) {
        this.onExport.emit(type);
    }
}