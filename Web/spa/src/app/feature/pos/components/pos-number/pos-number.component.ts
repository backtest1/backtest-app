import { Component, OnInit, AfterContentInit, Output, EventEmitter, Input } from '@angular/core';
import { CurrencyComponent } from '@app/shared/component/number/currency.component';


@Component({
  selector: 'pos-number',
  templateUrl: './pos-number.component.html',
  styleUrls: ['./pos-number.component.scss']
})
export class PosNumberComponent extends CurrencyComponent implements AfterContentInit {
  @Output() enter = new EventEmitter();
  @Input() focus = false;
  ngAfterContentInit() {
    if(this.focus) {
      setTimeout(() => {
        document.getElementById("focus-input").focus();
      },0)  
    } 
  }
  onEnter(value) {
    this.enter.emit(value);
  }
}
