import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from '@app/core';
import { FormUtilService, ModalService, RowState, Size, SubscriptionDisposer } from '@app/shared';
import { merge, Observable, of, Subscription } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { AccountDialogResolverService } from './dialogs/account-dialog/account-dialog-resolver.service';
import { AccountDialogComponent } from './dialogs/account-dialog/account-dialog.component';
import { FnPosService } from './fn-pos.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pos-template',
  templateUrl: './template.component.html',
  styleUrls: ['./pos.component.scss', './template.component.scss']
})
export class PosTemplateComponent  extends SubscriptionDisposer implements OnInit {
  template: any;
  form: FormGroup;
  selected = [];
  items = [];
  summary = {
    totalAmt: 0,
    totalQty: 0
  };
  master = {
    educationTypeLevels: []
  };
  saving = false;
  formSubscription: Subscription;
  temporaryItems = [];
  templateId = 0;
  constructor( 
    activatedRoute: ActivatedRoute, 
    private service: FnPosService, 
    private router: Router, 
    private as : MessageService, 
    private fb: FormBuilder, 
    private modal: ModalService,
    public util: FormUtilService,
    private location: Location,
  ) { 
    super();
    this.rebuildForm();
    activatedRoute.data.subscribe( extras => {
      if(!!extras.extras) {
        this.master = extras.extras.master;
        const details = extras.extras.details;
        if(!!details) {
          const items = !!details.fnTemplateDetail ? details.fnTemplateDetail.map( o => {
            const g = this.fb.group({
              templateDetailId: o.tmplDId,
              itmId: o.itmId,
              itmCode: o.itmCode,
              itmName: o.itmName,
              umsName: o.umsName,
              amount: [o.itmQty, Validators.required],
              price: [o.netAmt, Validators.required],
              fundCenterCode: o.fundCenterCode,
              costCenterCode: o.costCenterCode,
              budgetCode: o.budgetCode,
              grossAmt: (o.itmQty || 0) * (o.netAmt || 0),
              rowVersion: o.rowVersion,
              rowState: RowState.Normal
            });
            g.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
              g.get('rowState').patchValue(RowState.Edit, {  emitEvent : false, onlySelf: true });
            });      
            merge(g.get('amount').valueChanges, g.get('price').valueChanges).pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
              const amount = g.get('amount').value || 0;
              const price = g.get('price').value || 0;
              g.get('grossAmt').patchValue(amount * price, {  emitEvent : false, onlySelf: true });
            });
            return g;
          }) : [];
          this.template = {
            templateName: [details.templateName, Validators.required],
            active: details.active,
            isActive: details.isActive,
            educationTypeLevel: details.educationTypeLevel,
            effectiveDate: details.startDate,
            endDate: details.endDate,
            items: this.fb.array(items),
            rowVersion: details.rowVersion
          };
          this.templateId = details.tmplMId || 0;
          this.rebuildForm();
        }
      }
    });
  }

  ngOnInit(): void {
    this.service.getTemplateItems().subscribe( items => {
      this.items = items;
    });
  }

  rebuildForm() {
    this.temporaryItems = [];
    if(!!this.formSubscription) {
      this.formSubscription.unsubscribe();
    }
    this.form = this.fb.group(this.template || {
      templateName: [null, Validators.required],
      active: true,
      isActive: false,
      educationTypeLevel: null,
      effectiveDate: null,
      endDate: null,
      rowVersion: 0,
      items: this.fb.array([])
    });
    const items = this.form.get('items') as FormArray;
    this.formSubscription = items.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe( (items: any[]) => {
      const totalAmt = items.map( res => res.grossAmt ).reduce((prev, curr) => {
        return prev + curr;
      }, 0);
      this.summary = {
        totalAmt: totalAmt,
        totalQty: items.length
      };
    });  
    const i = items.getRawValue() as any[];
    const _qty =  i.map( res => res.grossAmt ).reduce((prev, curr) => {
      return prev + curr;
    }, 0);
    this.summary = {
      totalAmt: _qty,
      totalQty: items.length
    };
    this.form.markAsPristine();
  }

  addItems(item) {
    const items = this.form.get('items') as FormArray;
    if(!!items) {
      const g = this.fb.group({
        templateDetailId: 0,
        itmId: item.itmId,
        itmCode: item.itmCode,
        itmName: item.itmName,
        umsName: item.umsName || 'หน่วย',
        amount: [1, Validators.required],
        price: [0, Validators.required],
        fundCenterCode: null,
        costCenterCode: null,
        budgetCode: null,
        grossAmt: 0,
        rowVersion: 0,
        rowState: RowState.Add
      });
      merge(g.get('amount').valueChanges, g.get('price').valueChanges).pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
        const amount = g.get('amount').value || 0;
        const price = g.get('price').value || 0;
        g.get('grossAmt').patchValue(amount * price, {  emitEvent : false, onlySelf: true });
      });
      items.push(g);
      this.form.markAsDirty();
    }    
  }

  removeItems(index, item: FormGroup) {
    const items = this.form.get('items') as FormArray;
    if(item.get('rowState').value !== RowState.Add) {
      this.temporaryItems.push(Object.assign(item.getRawValue(), { rowState: RowState.Delete }));
    }
    if(!!items) {
      items.removeAt(index);
    } 
    this.form.markAsDirty();
  }

  get formItems() {
    return this.form.get('items') as FormArray;
  }


  goBack() {
    this.location.back();
  }

  save() {
    this.util.markFormGroupTouched(this.form);
		if (this.form.invalid) {
			return;
		}
    const values = this.form.getRawValue();
    this.saving = true;
    if(!!this.templateId) {
      const filterItems = values.items as Array<any> || [];
      values.items = this.temporaryItems.concat( filterItems.filter( o => o.rowState != RowState.Normal ) );
      this.service.updateTemplate(this.templateId, values).pipe(finalize(() => (this.saving = false)))
			.subscribe((result) => {
        this.form.markAsPristine();
				this.as.success('message.STD00006');
        this.router.navigate(['/fn/fndt02/template/details'], { replaceUrl: true, state: { code: this.templateId } });
			});
    } else {
      this.service.saveTemplate(values)
			.pipe(finalize(() => (this.saving = false)))
			.subscribe((result) => {
				this.as.success('message.STD00006');
        this.rebuildForm();
			});
    }
  }

  settingAccount(i: number) {
    const items = this.form.get('items') as FormArray;
    const fg = items.get([i]);
    this.modal.openComponent(AccountDialogComponent, Size.medium, {
      budgetCode: fg.get('budgetCode').value,
      fundCenterCode: fg.get('fundCenterCode').value,
      costCenterCode: fg.get('costCenterCode').value
    }, AccountDialogResolverService).pipe(takeUntil(this.ngUnsubscribe)).subscribe( accounts => {
      fg.get('fundCenterCode').patchValue(accounts.fundCenterCode);
      fg.get('costCenterCode').patchValue(accounts.costCenterCode, {  emitEvent : false, onlySelf: true });
      fg.get('budgetCode').patchValue(accounts.budgetCode, {  emitEvent : false, onlySelf: true });
    });  
  }

  onSearch(keyword) {
    this.service.getTemplateItems(keyword).subscribe( items => {
      this.items = items;
    });
  }

  canDeactivate(): Observable<boolean> {
		if (!this.form.dirty) {
			return of(true);
		}
		return this.modal.confirm("message.STD00002");
	}

}

interface FormTemplate {
  templateName: string,
  active: boolean,
  educationTypeLevel: string,
  effectiveDate: Date,
  endDate: Date,
  items: FormTemplateItem[],
  rowVersion: number;
}
interface FormTemplateItem {
  itmId: number;
  itmCode: string;
  itmName: string;
  umsName: string;
  amount: number;
  price: number;
  fundCenterCode: string;
  costCenterCode: string;
  budgetCode: string;
  grossAmt: number;
  rowVersion: number;
  rowState: number;
}
