
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FnAutocompleteRequest } from './components/autocomplete/models/fn-autocomplete-request.interface';
import { FnAutocompleteResponse } from './components/autocomplete/models/fn-autocomplete-response.interface';
@Injectable()
export class FnPosService { 

    constructor(private http: HttpClient) { }

    getStudentAutocomplete(request: FnAutocompleteRequest<string>) {
        let params = new HttpParams();
        if(!!request.value && request.value != '') {
            params = params.append("value", request.value);
        }
        if(!!request.keyword && request.keyword != '') {
            params = params.append("keyword", request.keyword);
        }        
        return this.http.get<FnAutocompleteResponse<string>>('fndt02/autocomplete/students', { params: params });
    }
    getBillByCode(codeType: string, code: string, educationTypeLevel: string, keyword: string = null, documentType: string =  null) {
        let params = new HttpParams();
        if(!!codeType && codeType.trim() != '' && !!code && code.trim() != '') {
            params = params.append("codeType", codeType);
            params = params.append("code", code);
        }    
        if(!!documentType) {
            params = params.append("documentType", documentType);
        }
        if(!!educationTypeLevel) {
            params = params.append("educationTypeLevel", educationTypeLevel);
        }
        if(!!keyword) {
            params = params.append("keyword", keyword);
        }
        return this.http.get<PosItems[]>('fndt02/bills', { params: params });
    }
    getBillDetails(args: {billNo, templateId}) {
        let params = new HttpParams();
        if(!!args.billNo && args.billNo.trim() != '') {
            params = params.append("billNo", args.billNo);
        } else  if(!!args.templateId)  {
            params = params.append("templateId", args.templateId);
        }
        return this.http.get<any>('fndt02/bills/details', { params: params });
    }
    getDiscount() {
        let params = new HttpParams();
        params = params.append("index", "0");   
        params = params.append("size", "10");
        return this.http.get<any>('fndt02/discounts', { params: params });
    }
    getReceives() {
        return this.http.get<any>('fndt02/receives');
    }
    calculateDiscount(amount: number, discountCode: string[]) {
        return this.http.post<any>('fndt02/discounts', {  
            amount: amount || 0,
            discountCode: discountCode
        });
    }
    calculateFee(args: { amount: number, deviceCode: string, cardBankCode: string, cardProvider: string, cardType: string }) {
        return this.http.disableLoading().post<any>('fndt02/fee', {  
            amount: args.amount || 0,
            deviceCode: args.deviceCode,
            cardBankCode: args.cardBankCode,
            cardProvider: args.cardProvider,
            cardType: args.cardType
        });
    }
    makePayment(payload: PaymentItem) {
        return this.http.post<any>('fndt02/makepayment', payload);
    }

    checkCashier() {
        return this.http.get<{ cashierCode: string }>('fndt02/cashiers');
    }

    sendEmail(receiveNo: string, language: string) {
        return this.http.post('fndt02/sendemail', {
            receiveNo: receiveNo,
            language: language || 'th'
        });
    }
    getTemplateItems(keyword: string = null) {
        let params = new HttpParams();
        if(!!keyword) {
            params = params.append("keyword", keyword);  
        }      
        return this.http.get<any>('fnrt25/template/items', { params: params });
    }
    getTemplateMaster(includes: string[], parameters?: object) {
        let params = '';
        let httpParams = new HttpParams() as any;
        includes.forEach( v => {
            params = '?';
            httpParams = httpParams.append(`includes`, v);
        });
        if(!!parameters) {
            for (const key of Object.keys(parameters)) {
                const value = parameters[key];
                if(!!value) {
                    httpParams = httpParams.append(key, value);
                }              
            }   
        }  
        params += httpParams.toString();
        return this.http.get<any>(`fnrt25/template/master${params}`);
    }
    getTemplateDetails(id: number) {
        return this.http.get<any>(`fnrt25/template/${id}`);
    }
    updateTemplate(id: number, payload: any) {
        return this.http.put<any>(`fnrt25/template/${id}`, payload);
    }
    saveTemplate(payload: any) {
        return this.http.post<any>('fnrt25/template', payload);
    }
    binLookup(binNo: string) {
        return this.http.get('fndt02/bin/' + binNo);
    }
    getMaster(includes: string[], parameters?: object) {
        let params = '';
        let httpParams = new HttpParams() as any;
        includes.forEach( v => {
            params = '?';
            httpParams = httpParams.append(`includes`, v);
        });
        if(!!parameters) {
            for (const key of Object.keys(parameters)) {
                const value = parameters[key];
                if(!!value) {
                    httpParams = httpParams.append(key, value);
                }              
            }   
        }  
        params += httpParams.toString();
        return this.http.get<any>(`fndt02/master${params}`);
    }

}
interface ReceiveTypeDTO {
    receiveTypeCode: string;
    receiveAmt: number;
}
export interface PaymentItem {
    code: string;
    codeType: string;
    billNo: string;
    templateId: number;
    billAmt: number;
    receives: ReceiveTypeDTO[]
    discountCode: string[]
}
export interface PosItems {

}
export interface PosPayerTarget {
    codeType: string;
    text: string;
    value: string;
    educationTypeLevel: string;
}
export class PosPayload {
    billAmt: number = 0;
    academicYear: number = 0;
    balAmt: number = 0;
    billDate: Date = null;
    billName: string;
    billNo: string;    
    templateId: number;
    companyCode: string;
    discountAmt: number = 0;
    documentType: string;
    dueDate: Date = null;
    fine: number = null;
    grossAmt: number = 0;
    invAmt: number = 0;
    items: any[] = [];
    netAmt: number = 0;
    interestAmt: number = 0;
    promotions: any[] = [];
    discountCode: string[] = [];
    receiveNo: string = null;
    balance: number = 0;
    receiveAmt: number = 0;
    receives: any[] = [];
    referenceNo1 = '0';
    referenceNo2 = '0';
    target: PosPayerTarget = null as {
        codeType: null,
        text: null,
        value: null,
        educationTypeLevel: null
    }
}