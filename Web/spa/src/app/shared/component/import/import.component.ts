import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AttachmentComponent } from '../attachment/attachment.component';
import { Content } from '../attachment/content-upload.service';

@Component({
  selector: 'import',
  templateUrl: './import.component.html',
  styleUrls: ['../attachment/attachment.component.scss']
})
export class ImportComponent extends AttachmentComponent {

  writeValue(file) {
    if (!file && this.control) {
      this.content.name = null;
      this.clear();
    }
    this.clearInternalError();
  }

  add(event) {
    const file = event[0];
    this.clearInternalError();
    if (!file) {
      return;
    }

    this.uploaded = false;

    if (this.isEmptySize(file)) {
      this.setInternalError();
      this.errors.push("ไม่พบเนื้อหา");
    } else if (this.isInvalidSize(file)) {
      this.setInternalError();
      this.errors.push("กรุณาเลือกไฟล์ขนาดไม่เกิน " + this.max + " KB");
    } else if (this.isInvalidFileType(file)) {
      this.setInternalError();
      this.errors.push("กรุณาเลือกไฟล์ประเภท " + this.fileType.split(',').join(', ') + " เท่านั้น");
    }

    if (this.errors.length === 0) {
      let attachment = event[0];
      this.content.name = attachment.name;
      this.setInternalError();
      this.control.setValue(attachment)
    }
  }

  protected isInvalidFileType(file) {
    if (!this.fileType) return false;
    let name = file.name.split('.');
    let type = this.fileType.split(',');
    return type.every(type => type != `.${name[name.length - 1]}`);
  }

  protected isEmptySize(file) {
    return file.size === 0;
  }

  protected isInvalidSize(file) {
    return Math.round((file.size / 1024)) >= this.max;
  }

  protected clear() {
    this.fileInput.nativeElement.value = '';
  }

  protected setInternalError() {
    if (this.control)
      this.control.setErrors({ uploading: true });
  }

  protected clearInternalError() {
    this.errors = [];
    if (this.control) {
      this.control.setErrors({ uploading: null });
      this.control.updateValueAndValidity({ onlySelf: false, emitEvent: false });
    }
  }

  remove() {
    this.onChange(null);
    this.content = {} as Content;
    this.clear();
    this.control.reset();
  }
}
