import { Component, Input, TemplateRef, ContentChild } from '@angular/core';
import { BaseFormField } from '../base-form';
import { Subject, Observable, concat, of, BehaviorSubject, Subscription, merge } from 'rxjs';
import { debounceTime, tap, switchMap, catchError, first, map, finalize, startWith, mergeMap, delay } from 'rxjs/operators';
import { TypeaheadSource } from './typeahead.source';

@Component({
  selector: 'typeahead',
  templateUrl: './typeahead.component.html',
  styleUrls: ['./typeahead.component.scss']
})
export class TypeaheadComponent extends BaseFormField {

  @Input() bindLabel = "text";
  @Input() bindValue = "value";
  @Input() bindDesc = "description";
  @Input() showDescription = false;
  @Input() customLabel = false;
  @Input() customTemplate = false;
  @Input() source: TypeaheadSource;
  @Input() labelTemplate: TemplateRef<any>;
  @Input() optionTemplate: TemplateRef<any>;

  items: Observable<any[]>;
  appendTo: string;
  loading = false;
  typeAhead = new Subject<string>();
  openState = new Subject();

  private ready = new BehaviorSubject<boolean>(false);
  private readySub: Subscription;

  @ContentChild(TemplateRef) defaultTemplate: TemplateRef<any>;
  ngOnInit() {
    this.appendTo = this.small ? 'Body' : null
    this.placeholder = this.placeholder ? this.placeholder : 'label.ALL.PleaseSelectAuto'
    if (this.customTemplate && this.optionTemplate == null) {
      this.optionTemplate = this.defaultTemplate;
    }
    this.ready.next(true);
  }

  ngOnDestroy() {
    if (this.readySub) this.readySub.unsubscribe();
  }

  writeValue(value: any): void {
    this.value = value;
    this.readySub = this.ready.pipe(first(ready => ready)).subscribe(() => {
      this.source.clearItems(this.value);
      this.loadData();
    });
  }

  trackByFn(item: any) {
    return item[this.bindValue];
  }

  onSelected(event) {
    this.onChange(event);
    this.value = event;
  }

  onOpen() {
    this.openState.next();
  }

  private loadData() {
    this.items = concat(
      this.source.getModel(this.value).pipe(
        map(item => {
          return Array.from([item]);
        })
      ),
      merge(
        this.openState.pipe(
          tap(() => this.loading = true),
          switchMap(() => concat(of([]),this.source.getData(null).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => this.loading = false)
          )))
        ),
        this.typeAhead.pipe(
          debounceTime(400),
          tap(() => this.loading = true),
          switchMap(term => this.source.getData(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => this.loading = false)
          ))
        )
      )
    );
  }
}
