import { BehaviorSubject, EMPTY, Observable, of } from "rxjs";
import { filter, finalize, first, map, mergeMap, tap } from "rxjs/operators";

export class TypeaheadSource {
    private isLoading = false;
    private loadingSubject: BehaviorSubject<any>;
    private model: any;
    private suggestItems: any[];
    private loadedItems: any[];
    private onSearch: (term: any, value?: any) => Observable<any[]>;
    private bindValue: string;
    constructor(onSearch: (term: any, value?: any) => Observable<any[]>, bindValue: string = 'value') {
        this.bindValue = bindValue;
        this.loadingSubject = new BehaviorSubject<any>(null);
        this.model = {};
        this.suggestItems = null;
        this.loadedItems = [];
        this.onSearch = onSearch;
    }

    getModel(value: any) {
        if (value) {
            if (!this.isLoading) {
                if (Object.keys(this.model).length && this.model[this.bindValue] === value) {
                    return of(this.model);
                }
                else {
                    const item = (this.suggestItems || []).concat(this.loadedItems).find(item => item[this.bindValue] === value)
                    if (item) {
                        this.model = item;
                        return of(this.model);
                    }
                    else {
                        this.isLoading = true;
                        this.loadingSubject.next(true);
                        return this.onSearch(null, value).pipe(
                            mergeMap(items => {
                                this.model = items[0] || {}
                                this.loadingSubject.next(false);
                                return of(this.model);
                            }),
                            finalize(() => this.isLoading = false)
                        )
                    }
                }
            }
            else {
                return this.loadingSubject.pipe(
                    first(loading => loading == false),
                    map(() => this.model)
                )
            }
        }
        else {
            this.model = {};
            return of(this.model);
        }
    }

    getData(term: any) {
        if (!term) {
            if (this.suggestItems) {
                return of(this.suggestItems);
            }
            else return this.onSearch(null, null).pipe(
                tap(items => {
                    this.suggestItems = items
                })
            )
        }
        else {
            return this.onSearch(term, null).pipe(
                tap(items => {
                    this.loadedItems = items
                })
            )
        }
    }

    clearItems(value){
        if (this.loadedItems && this.loadedItems.length) {
            let current = this.loadedItems.find(item => item[this.bindValue] === value);
            if (!current) this.loadedItems = [];
        }
        if (this.suggestItems) {
            let current = this.suggestItems.find(item => item[this.bindValue] === value);
            if (!current) this.suggestItems = null;
        }
    }
}