import { Injectable } from '@angular/core';
import { WeekDay } from 'calendar-utils';

export class Week {
  roomColumns: RoomRow[];
  dayColumns: DayColumn[];
}
export class Day {
  roomRows: RoomRow[];
  periodColumns: PeriodColumn[];
}

export class RoomRow {
  room: Room;
  height: number;
}
export class Room {
  text: string;
  value: number;
}
export class DayColumn {
  events: Event[];
}
export class PeriodColumn {
  events: Event[];
}
export class Period {
  startTime: string;
  endTime: string;
}
export class DayHeader {
  periods: Period[];
}
export class Event {
  event: CalendarEvent;
  height: number;
  width: number;
  top: number;
  left: number;
}
export class CalendarEvent {
  id: number;
  start: Date;
  end: Date;
  title: string;
  color: EventColor;
  date: Date;
  room: number;
  place: string;
  employeeName: string;
  startTime: string;
  endTime: string;
  allDay: boolean;
  subjectCode : string
  subjectName : string
  sectionCode: string
  reserveObjective: string
  reserveObjectiveName: string
}
export class EventColor {
  primary: string;
  secondary: string;
  title: string;
}

export class MonthCellEvent {
  objective: string;
  color: string;
}

export interface MonthViewDay<MetaType = any> extends WeekDay {
  inMonth: boolean;
  events: CalendarEvent[];
  backgroundColor?: string;
  badgeTotal: number;
  meta?: MetaType;
}


@Injectable()
export class CalendarService {

  constructor() { }
}
