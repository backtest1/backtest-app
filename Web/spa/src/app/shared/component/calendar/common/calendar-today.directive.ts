import {
  Directive,
  HostListener,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DateAdapter } from '../date-adapters/date-adapter';

/**
 * Change the view date to the current day. For example:
 *
 * ```typescript
 * <button
 *  calendarToday
 *  [(viewDate)]="viewDate">
 *  Today
 * </button>
 * ```
 */
@Directive({
  selector: '[calendarToday]'
})
export class CalendarTodayDirective {
  /**
   * The current view date
   */
  @Input() viewDate: Date;
  @Input() form: FormGroup;


  /**
   * Called when the view date is changed
   */
  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

  constructor(private dateAdapter: DateAdapter) {}

  /**
   * @hidden
   */
  @HostListener('click')
  onClick(): void {
    if(this.form.invalid)
      this.viewDateChange.emit(this.dateAdapter.startOfDay(this.viewDate));
    else
     this.viewDateChange.emit(this.dateAdapter.startOfDay(new Date()));
  }
}
