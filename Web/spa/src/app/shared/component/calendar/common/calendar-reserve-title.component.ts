import { Component, Input, TemplateRef, OnInit } from '@angular/core';
import { LazyTranslationService } from '@app/core';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
import { CalendarEvent } from '../service/calendar.service';
//import { CalendarEvent } from 'calendar-utils';
import { CalendarView } from './calendar-view.enum';

@Component({
  selector: 'calendar-reserve-title',
  templateUrl: 'calendar-reserve-title.html'
})
export class CalendarReserveTitleComponent {
  CalendarView = CalendarView;
  @Input() titleColor: string;
  @Input() calendarView: CalendarView;
  @Input() event: CalendarEvent;
  @Input() view: string;
  title: string;
  section: string;
  constructor(
    private ts: TranslateService,
    private tz: LazyTranslationService,

  ) { }
  ngOnInit() {
    if (this.calendarView === CalendarView.Month) {
      const str = 'label.ALL.Section';
      this.tz.lazyLoaded.pipe(
        filter(modules => modules.some(module => module === 'cs')),
      ).subscribe(() => {
        this.section = this.ts.instant(str);
      })
      this.setTitle();
    }
  }
  setTitle() {
    this.title = `${this.event.startTime}-${this.event.endTime}`;
    if (this.event.subjectCode) {
      this.title += ` ${this.event.subjectCode} ${this.event.subjectName}`
    }
    if (this.event.sectionCode) {
      this.title += ` ${this.section} ${this.event.sectionCode}`
    }
    if (!(this.event.subjectCode && this.event.reserveObjective) && this.event.reserveObjective) {
      this.title += ` ${this.event.reserveObjectiveName}`
    }
  }


}
