import { Component, Input, Output, EventEmitter, TemplateRef, OnInit } from '@angular/core';
import { WeekViewAllDayEvent, DayViewEvent, WeekViewHourColumn } from 'calendar-utils';
import { PlacementArray } from 'positioning';
import { CalendarView } from '../common/calendar-common.module';
import { EventColor } from '../service/calendar.service';


@Component({
  selector: 'mwl-calendar-week-view-event',
  templateUrl: 'calendar-week-view-event.html'
})
export class CalendarWeekViewEventComponent {
  
  @Input() color: EventColor;
  @Input() calendarView: CalendarView;
  
  @Input() weekEvent: WeekViewAllDayEvent | DayViewEvent;

  @Input() tooltipPlacement: PlacementArray;

  @Input() tooltipAppendToBody: boolean;

  @Input() tooltipDisabled: boolean;

  @Input() tooltipDelay: number | null;

  @Input() customTemplate: TemplateRef<any>;

  @Input() eventTitleTemplate: TemplateRef<any>;

  @Input() eventActionsTemplate: TemplateRef<any>;

  @Input() tooltipTemplate: TemplateRef<any>;

  @Input() column: WeekViewHourColumn;

  @Output() eventClicked: EventEmitter<any> = new EventEmitter();
}
