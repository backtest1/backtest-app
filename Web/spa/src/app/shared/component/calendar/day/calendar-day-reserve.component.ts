import { Component, Input } from '@angular/core';
import { WeekViewAllDayEvent, DayViewEvent } from 'calendar-utils';
import { CalendarView } from '../common/calendar-common.module';
import { EventColor } from '../service/calendar.service';

@Component({
  selector: 'calendar-day-reserve',
  templateUrl: 'calendar-day-reserve.html'
})
export class CalendarDayReserveComponent {
  @Input() color: EventColor;
  @Input() calendarView: CalendarView;
  @Input() weekEvent: WeekViewAllDayEvent | DayViewEvent;
  tooltip: string;
}
