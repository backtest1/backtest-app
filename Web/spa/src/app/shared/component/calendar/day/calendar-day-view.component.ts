import { Component, Input, Output, EventEmitter, OnChanges, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { Room, Period, Day, PeriodColumn, DayHeader, CalendarEvent, Event, EventColor } from '../service/calendar.service';
import { CalendarView } from '../common/calendar-common.module';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'calendar-day-view',
  templateUrl: 'calendar-day-view.html'
})
export class CalendarDayViewComponent implements OnInit, OnChanges, OnDestroy {

  @Input() width: number;
  @Input() viewDate: Date;
  @Output() eventClicked = new EventEmitter<{ event: CalendarEvent; }>();
  @Input() refresh: Subject<any>;
  @Input() rooms: Room[];
  @Input() events: CalendarEvent[];
  @Input() calendarView: CalendarView;
  @Input() periods: Period[];
  day = new Day();
  dayHeader = new DayHeader();
  refreshSubscription: Subscription;
  time = [];

  constructor(private datePipe: DatePipe) { }

  ngOnInit() {
    this.refreshAll();
  }

  getPeriods() {
    this.dayHeader.periods = [];
    let tempTime = this.periods[0].startTime;
    let time: string[];
    while (true) {
      time = tempTime.split(':');
      const startTime = time[0] + ':' + time[1];

      let endTime;
      if (time[0] >= '18') {
        endTime = (+time[0] + 1).toString().padStart(2, '0') + ':' + time[1];
      }
      else if (time[1] == '30') {
        endTime = (+time[0] + 1).toString().padStart(2, '0') + ':' + '00';
      }
      else {
        endTime = time[0] + ':' + '30';
      }

      if (tempTime > this.periods[0].endTime) {
        // this.dayHeader.periods.push({
        //   startTime: startTime,
        //   endTime: endTime
        // });
        break;
      } else {
        this.dayHeader.periods.push({
          startTime: startTime,
          endTime: endTime
        });
        tempTime = endTime;
      }
    }
  }

  getRooms() {
    this.day.roomRows = this.rooms.map(room => { return { room: room, height: 0 }; });
  }

  getEvents() {
    let period: PeriodColumn[] = this.dayHeader.periods.map(() => new PeriodColumn());
    for (let r = 0; r < this.rooms.length; r++) {

      // let haveOverPeriod = false;
      let beforeEvent = []; //? เก็บ event ที่ลงตารางแล้ว
      const periodFirstStartTime = +(this.dayHeader.periods[0].startTime.split(':')[0] + this.dayHeader.periods[0].startTime.split(':')[1]);
      const periodFirstEndTime = +(this.dayHeader.periods[0].endTime.split(':')[0] + this.dayHeader.periods[0].endTime.split(':')[1]);
      // const periodLastStartTime = +(this.dayHeader.periods[this.dayHeader.periods.length - 1].startTime.split(':')[0] + this.dayHeader.periods[this.dayHeader.periods.length - 1].startTime.split(':')[1]);
      const periodLastEndTime = +(this.dayHeader.periods[this.dayHeader.periods.length - 1].endTime.split(':')[0] + this.dayHeader.periods[this.dayHeader.periods.length - 1].endTime.split(':')[1]);

      for (let p = 0; p < this.dayHeader.periods.length; p++) {
        // const periodCurrentStartTime = +(this.dayHeader.periods[p].startTime.split(':')[0] + this.dayHeader.periods[p].startTime.split(':')[1]);
        const periodCurrentEndTime = +(this.dayHeader.periods[p].endTime.split(':')[0] + this.dayHeader.periods[p].endTime.split(':')[1]);

        // let hasEvents = !haveOverPeriod ? 0 : 1;
        let events;
        if (p == 0) {
          events = this.events.filter(event =>
            this.datePipe.transform(event.date, 'ddMMyyyy') === this.datePipe.transform(this.viewDate, 'ddMMyyyy') &&
            event.startTime < this.dayHeader.periods[p].endTime &&
            event.room === this.rooms[r].value);
        }
        else if (p == this.dayHeader.periods.length - 1) {
          events = this.events.filter(event =>
            this.datePipe.transform(event.date, 'ddMMyyyy') === this.datePipe.transform(this.viewDate, 'ddMMyyyy') &&
            event.startTime >= this.dayHeader.periods[p].startTime &&
            event.room === this.rooms[r].value);
        }
        else {
          events = this.events.filter(event =>
            this.datePipe.transform(event.date, 'ddMMyyyy') === this.datePipe.transform(this.viewDate, 'ddMMyyyy') &&
            event.startTime >= this.dayHeader.periods[p].startTime &&
            event.startTime < this.dayHeader.periods[p].endTime &&
            event.room === this.rooms[r].value);
        }
        let event: Event[] = [];

        for (let e = 0; e < events.length; e++) {
          const evStart = +(events[e].startTime.split(':')[0] + events[e].startTime.split(':')[1]);
          const evEnd = +(events[e].endTime.split(':')[0] + events[e].endTime.split(':')[1]);
          let eventTop = 0;
          let isOverlap = true;
          if (beforeEvent.length > 0) {
            for (var i = 0; i < beforeEvent.length; i++) {
              if (evStart < beforeEvent[i]) {
                eventTop++;
              }
              else if (evStart >= periodLastEndTime) {
                if (periodLastEndTime <= beforeEvent[i]) {
                  eventTop++;
                }
                else {
                  beforeEvent[i] = periodLastEndTime;
                  isOverlap = false;
                  break;
                }
              }
              else {
                if (evEnd <= periodCurrentEndTime)
                  beforeEvent[i] = periodCurrentEndTime;
                else {
                  for (let q = p; q < this.dayHeader.periods.length; q++) {
                    const periodTmpEndTime = +(this.dayHeader.periods[q].endTime.split(':')[0] + this.dayHeader.periods[q].endTime.split(':')[1]);
                    if (evEnd <= periodTmpEndTime) {
                      beforeEvent[i] = periodTmpEndTime;
                      break;
                    }
                    else if(q == this.dayHeader.periods.length -1){
                      beforeEvent[i] = periodTmpEndTime;
                    }
                  }
                }

                isOverlap = false;
                break;
              }
            }
            // if (p == 0) {
            //   if (evStart <= periodFirstStartTime)
            //     eventTop++;
            // }
            // if (p == (this.dayHeader.periods.length - 1) && evStart >= eventEndTime) {
            //   eventTop++;
            // }
          }
          event.push({
            event: events[e],
            height: 45,
            width: 100 + this.getOffSet(events[e], this.dayHeader, p),
            top: (eventTop * 44) + this.day.roomRows.reduce((height, room) => height + room.height, 0) - this.day.roomRows[r].height,
            left: 0
          });
          if (isOverlap) {
            if (evEnd <= periodFirstStartTime) {
              beforeEvent.push(periodFirstEndTime);
            }
            else if (evEnd >= periodLastEndTime) {
              beforeEvent.push(periodLastEndTime);
            }
            else {
              if (evEnd <= periodCurrentEndTime)
                beforeEvent.push(periodCurrentEndTime);
              else {
                for (let q = p; q < this.dayHeader.periods.length; q++) {
                  const periodTmpEndTime = +(this.dayHeader.periods[q].endTime.split(':')[0] + this.dayHeader.periods[q].endTime.split(':')[1]);
                  if (evEnd <= periodTmpEndTime) {
                    beforeEvent.push(periodTmpEndTime);
                    break;
                  }
                  else if(q == this.dayHeader.periods.length -1){
                    beforeEvent.push(periodTmpEndTime);
                  }

                }
              }
            }
            // hasEvents++;  
          }

          // //? ถ้า event ไม่ใช่ last period
          // if (!haveOverPeriod) {
          //   if ((evStart < periodLastStartTime) && (evEnd >= periodLastStartTime)) {
          //     haveOverPeriod = true;
          //   }
          // }
        }

        // const height = hasEvents == 0 ? (1 * 44) : hasEvents * 44;
        const height = beforeEvent.length == 0 ? (1 * 44) : beforeEvent.length * 44;
        if (!this.day.roomRows[r].height || height > this.day.roomRows[r].height) {
          this.day.roomRows[r].height = height;
        }

        if (!period[p].events) period[p].events = [];
        if (event.length > 0) period[p].events.push(...event);
      };
    }

    this.day.periodColumns = period;
  }

  getOffSet(event: CalendarEvent, dayHeader: DayHeader, index: number): number {
    const eventStartTime = +(event.startTime.split(':')[0] + event.startTime.split(':')[1]);
    const eventEndTime = +(event.endTime.split(':')[0] + event.endTime.split(':')[1]);
    let indexStart = 0;
    for (let p = 0; p < dayHeader.periods.length; p++) {
      const periodStartTime = +(dayHeader.periods[p].startTime.split(':')[0] + dayHeader.periods[p].startTime.split(':')[1]);
      const periodEndTime = +(dayHeader.periods[p].endTime.split(':')[0] + dayHeader.periods[p].endTime.split(':')[1]);
      if (p == 0) {
        if (eventStartTime <= periodEndTime) indexStart = p;
        if (eventEndTime >= periodStartTime && eventEndTime <= periodEndTime) {
          return (p - indexStart) * 100;
        }
        else if (eventEndTime < periodStartTime && eventEndTime <= periodEndTime) {
          return (p - indexStart) * 100;
        }
      }
      else if (p == dayHeader.periods.length - 1) {
        if (eventStartTime >= periodStartTime) indexStart = p;
        if (eventEndTime >= periodStartTime && eventEndTime >= periodStartTime) {
          return (p - indexStart) * 100;
        }
      }
      else {
        if (eventStartTime >= periodStartTime && eventStartTime <= periodEndTime) indexStart = p;
        if (eventEndTime >= periodStartTime && eventEndTime <= periodEndTime) {
          return (p - indexStart) * 100;
        }
      }
    }
  }

  ngOnChanges(changes) {
    if (changes.viewDate || changes.events) {
      this.refreshAll();
      return;
    }
  }

  ngOnDestroy() {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  refreshAll() {
    this.getPeriods();
    this.getRooms();
    this.getEvents();
  }
}
