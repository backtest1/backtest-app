import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Page } from '../component/datatable/page.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class FormUtilService {
    markFormGroupTouched(formGroup: FormGroup) {
        (<any>Object).values(formGroup.controls).forEach(control => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }

    focusInvalid(el) {
        const invalidElements = el.querySelectorAll('.ng-invalid');
        if (invalidElements.length > 0) {
            invalidElements[0].scrollIntoView(false);
        }
    }

    setPageIndex(page: Page, deletedCount: number = 1) {
        const index = Math.min(Math.ceil((page.totalElements - deletedCount) / page.size) - 1, page.index);
        page.index = index < 0 ? 0 : index;
        return page;
    }

    //hidden from dropdownlist 
    getActive(originals: any[], baseValue: any): any[] {
        let items = [];
        if (baseValue) {
            items = originals.reduce((result, row) => {
                if (row.active || baseValue == row.value) {
                    result.push(row);
                }
                return result;
            }, [])
        }
        else {
            items = originals.reduce((result, row) => {
                if (row.active) {
                    result.push(row);
                }
                return result;
            }, [])
        }
        return items;
    }

    getModel(value: any, list: any[], bindValue: string = 'value'): any {
        if (!list || list.length === 0) return []
        return list.find(item => item[bindValue] === value) || {};
    }

    getModelAsync(value: any, list: Observable<Array<any>>, bindValue: string = 'value'): Observable<any> {
        return list.pipe(
            map(items => {
                if (!items || items.length === 0) return {};
                return items.find(item => item[bindValue] === value) || {};
            })
        )
    }
}