import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BackgroundService {
  
   private showSub = new BehaviorSubject<boolean>(true);
   setStatus(status:boolean){
       this.showSub.next(status);
   }
   getStatus(){
      return this.showSub.asObservable();
   }
}