﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.Models;
using Application.Features.SU.SUMG03;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers.SU
{
    [Authorize(Roles = "SUMG03")]
    public class Sumg03Controller : BaseController
    {
        private readonly MigrateUser _migrate;
        private readonly ICurrentUserAccessor _user;
        public Sumg03Controller(MigrateUser migrate, ICurrentUserAccessor user)
        {
            _migrate = migrate;
            _user = user;
        }

        [HttpPost]
        public IActionResult Post([FromBody] Criteria model)
        {
            var currentUser = new CurrentUser();
            currentUser.CompanyCode = _user.Company;
            currentUser.UserName = _user.UserName;
            currentUser.ProgramCode = _user.ProgramCode;
            model.User = currentUser;
            this._migrate.Migrate(model);
            return Ok("Job Started");
        }

        [HttpGet]
        public IActionResult Stop([FromQuery] string jobName)
        {
            this._migrate.Stop(jobName);
            return NoContent();
        }
    }
}
