using System;
using System.Threading.Tasks;
using Application.Features.Demo;
using Microsoft.AspNetCore.Mvc;
using Application.Features;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.Net.Http.Headers;
using System.Net.Mime;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class DemoController : BaseController
    {
        private static readonly HttpClient client = new HttpClient();
        private IWebHostEnvironment _hostEnvironment;

        public DemoController(IWebHostEnvironment environment)
        {
            _hostEnvironment = environment;
        }

        [HttpGet("currentdate")]
        public IActionResult get()
        {
            var tz = TimeZoneInfo.Local;
            return Ok(new {timezone = tz, date = DateTime.Now});
        }

        [HttpPost("getdemoreport")]
        public async Task<ActionResult<string>> Post([FromBody]GenerateDemoReport model)
        {
            byte[] bytes = await Mediator.Send(model);
            string base64String;
            if (model.ExportType.Equals("pdf"))
            {
                base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            else
            {
                base64String = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            
            return Ok(base64String);
        }

        [HttpPost("createstd")]
        public async Task<IActionResult> Post()
        {
            return Ok(await Mediator.Send(new CreateUser.Command { }));
           
        }

        [HttpGet("getreport")]
        public async Task<IActionResult> report()
        {
            var path = Path.Combine(_hostEnvironment.ContentRootPath, "Log_20210118.xlsx");
            return File(System.IO.File.ReadAllBytes(path), "application/octet-stream","excel.xlsx");
        }
        [HttpGet("import")]
        public async Task<IActionResult> import()
        {
            return Ok(await Mediator.Send(new ImportExcel.Command { }));
        }

        [HttpGet("soap")]
        public async Task<IActionResult> soap([FromQuery] CallSOAP.Command model )
        {
            return Ok(await Mediator.Send(model));
        }

        [HttpGet("test")]
        public async Task<IActionResult> Get([FromQuery] Test.Query model)
        {
            return Ok(await Mediator.Send(model));
        }

        [HttpPost("upload")]
        public async Task<IActionResult> upload()
        {
            return Ok(await Mediator.Send(new UploadReportFile.Command()));
        }
    }
}
