﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.DB
{
    public class DbEducationLevelDegree : EntityBaseOracle
    {
        public string CompanyCode { get; set; }
        public string EducationTypeLevel { get; set; }
        public long DegreeId { get; set; }
        public bool? Active { get; set; }

    }
}
