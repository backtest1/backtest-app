﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.DB
{
    public class DbRoom : EntityBaseOracle
    {
        public long? RoomId { get; set; }
        public long? BuildingId { get; set; }
        public string RoomNo { get; set; }
        public string RoomNameTha { get; set; }
        public string RoomNameEng { get; set; }
        public int? FloorNo { get; set; }
        public int? Capacity { get; set; }
        public string RoomType { get; set; }
        public string ResponsibleAGC { get; set; }
        public int? SeatsRoomUsed { get; set; }
        public int? RowsNumberExam { get; set; }
        public int? RowsNumberTest { get; set; }
        public int? sumRowsNumberExam { get; set; }
        public int? CommitteeNumber { get; set; }
        public int? RoomArea { get; set; }
        public string CostCenterCode { get; set; }
        public int? CostHour { get; set; }
        public string DivCode { get; set; }
        public string FacCode { get; set; }
        public bool ExamRoom { get; set; }
        public bool Active { get; set; }
        public long? CostCenterId { get; set; }
    }
}
