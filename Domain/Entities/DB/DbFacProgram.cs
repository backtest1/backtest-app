﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.DB
{
    public class DbFacProgram : EntityBaseOracle
    {
        public string CompanyCode { get; set; }
        public string FacCode { get; set; }
        public string ProgramCode { get; set; }
        public string CourseCode { get; set; }
        public long? RoomId {get;set;}
        public bool? Active { get; set; }
        public string CostCenterCode { get; set; }
        public long? CostCenterId { get; set; }

        public ICollection<DbFacProgramDetail> dbFacProgramDetail { get; set; }

    }
}
