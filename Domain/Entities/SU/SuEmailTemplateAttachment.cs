﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.SU
{
    public class SuEmailTemplateAttachment : EntityBaseOracle
    {
        public string EmailTemplateCode {get;set;}
        public long AttachmentId { get; set; }
    }
}
