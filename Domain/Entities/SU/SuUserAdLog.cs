﻿using Domain.Types;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.SU
{
    public class SuUserAdLog : EntityBaseOracle
    {
        public long LogId {get;set;}
        public long UserId {get;set;}
        public string UserName {get;set;}
        public string AdStudentId {get;set;}
        public string AdFirstName {get;set;}
        public string AdLastName {get;set;}
        public string AdFaculty {get;set;}
        public string AdDepartment {get;set;}
        public string AdCitizenId {get;set;}
        public string AdPassportId {get;set;}
        public string AdSecondaryEmail {get;set;}
        public string AdTemplate {get;set;}
        public string AdFunction { get;set;}
        public string AdResult {get;set;}
        public string ErrorLog {get;set;}
    }
}
