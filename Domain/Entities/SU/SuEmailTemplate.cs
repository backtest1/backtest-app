﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.SU
{
    public class SuEmailTemplate : EntityBaseOracle
    {
        public string EmailTemplateCode {get;set;}
        public string EmailTemplateName{ get; set; }
        public string Subject { get; set; }
        public string Description {get;set;}
        public string Content { get; set; }
        public ICollection<SuEmailTemplateAttachment> EmailTemplateAttachments { get; set; }
    }
}
