﻿using Domain.Types;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.SU
{
    public class SuUserTemp : EntityBaseOracle
    {
        public long UserId { get; set; }
        public string UserName {get;set;}
        public string Password { get; set; }
        public string FullNameTha {get;set;}
        public string FullNameEng {get;set;}
        public string Email {get;set;}
        public bool? IsSended { get; set; }
    }
}
