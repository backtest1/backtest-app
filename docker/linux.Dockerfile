FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine AS base

# Disable the invariant mode (set in base image)
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

#installs libgdiplus to support System.Drawing for handling of graphics
RUN apk add libgdiplus --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted
#installs some standard fonts needed for Autofit columns support
RUN apk --no-cache add msttcorefonts-installer fontconfig freetype-dev libjpeg-turbo-dev libpng-dev && \
    update-ms-fonts && \
    fc-cache -f 

# Install tzdata, cultures (same approach as Alpine SDK image)
RUN apk add --update tzdata icu-libs && \
    rm -rf /var/lib/apt/lists/* && rm /var/cache/apk/*

# RUN wget http://dl-cdn.alpinelinux.org/alpine/edge/testing/x86_64/libgdiplus-6.0.5-r0.apk && \
#     apk add --allow-untrusted libgdiplus-6.0.5-r0.apk

#RUN apt-get update && \
#    apt-get install -y tzdata libicu66 libgdiplus libc6-dev libx11-dev ttf-mscorefonts-installer fontconfig

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine AS build
# FROM mcr.microsoft.com/dotnet/core/sdk:3.1-focal AS build

LABEL MAINTAINER SiritasDho<dahoba@gmail.com>

LABEL description="Smart Student, Smart Pay"

WORKDIR /source

COPY Application/*.csproj ./Application/

COPY Domain/*.csproj ./Domain/

COPY Infrastructure/*.csproj ./Infrastructure/

COPY Persistense/*.csproj ./Persistense/

COPY PersistenseOracle/*.csproj ./PersistenseOracle/

COPY PersistenseMySql/*.csproj ./PersistenseMySql/

COPY PersistenseSqlServer/*.csproj ./PersistenseSqlServer/

COPY Web/*.csproj ./Web/

COPY *.sln ./

RUN dotnet nuget add source https://www.myget.org/F/redhat-dotnet/api/v3/index.json -n rh

RUN dotnet restore

# Copy everything else and build
COPY . ./

RUN dotnet publish -c release -o /app  --no-restore
# RUN dotnet publish -c debug -o /app  --no-restore

# Build runtime image
# FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine as runtime
FROM base AS runtime
WORKDIR /app
COPY --from=build /app ./
EXPOSE 80
ENTRYPOINT ["dotnet", "Web.dll"]
