#! /usr/bin/env bash
set -e

REGISTRY="registry.gitlab.com/softsquare_dpu/registry"
IMAGE_NAME="sss-front-uat"
TAG_DATETIME=$(date +%Y%m%d)
COMMIT_SHORT_SHA=`git rev-parse --short HEAD`

# URL sss.slcm.dpu.ac.th
# cd Web/spa
# npm ci
# npm run build-staging
# cd ../..

# use if want to build inside docker; "Web/spa/docker/ngcli.Dockerfile" 

docker build --rm \
--build-arg TARGET_ENV="build-staging" \
-f "Web/spa/docker/Dockerfile" -t ${IMAGE_NAME} \
-t ${REGISTRY}/${IMAGE_NAME} \
-t ${IMAGE_NAME}:${TAG_DATETIME} \
-t ${REGISTRY}/${IMAGE_NAME}:${TAG_DATETIME} \
-t ${IMAGE_NAME}:${COMMIT_SHORT_SHA} \
-t ${REGISTRY}/${IMAGE_NAME}:${COMMIT_SHORT_SHA} \
 Web/spa 