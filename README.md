# Smart Student Smart Pay System [SSS]

[![pipeline status](https://gitlab.com/softsquare_ssru/smartu-sss/badges/develop/pipeline.svg)](https://gitlab.com/softsquare_ssru/smartu-sss/-/commits/develop)

## Config the develeopment station

 - Find `Web/spa/src/proxy.conf.js`.
 - Change `target` to the webAPI URL, if it's not match with your environment
 - And make sure that you're already start the webAPI project!
 - `logLevel` can chage to debug to investigate any problem might cause when execute the api end-point.

```js 
  "/api": {
    target: "https://localhost:5001",
    secure: false,
    "changeOrigin": true,
    "logLevel": "warn"
  }
```


## Docker build API

```
docker build --rm -f docker/Dockerfile -t sss-api -t registry.gitlab.com/softsquare_ssru/registry/sss-api .
```

## Docker build Frontend

First change directory to `Web/spa`

Need to build angular before execute docker command

For Dev 

```
npm run build-docker
```

For UAT 

```
cd Web/spa
npm ci
npm run build-staging
```

Then pack the docker image

```
cd ../..
./docker-build-api.ps1
```


