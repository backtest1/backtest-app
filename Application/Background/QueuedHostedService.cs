﻿using Application.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Background
{
    public class QueuedHostedService : BackgroundService
    {
        private readonly ILogger<QueuedHostedService> _logger;
        private readonly IHubContext<MigrateJobHub> _jobHub;
        public QueuedHostedService(IBackgroundTaskQueue taskQueue, IHubContext<MigrateJobHub> jobHub,
            ILogger<QueuedHostedService> logger)
        {
            TaskQueue = taskQueue;
            _logger = logger;
            _jobHub = jobHub;
        }

        public IBackgroundTaskQueue TaskQueue { get; }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Queued Hosted Service is running.{Environment.NewLine}");

            await BackgroundProcessing(stoppingToken);
        }

        private async Task BackgroundProcessing(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var work = await TaskQueue.DequeueAsync(stoppingToken);

                try
                {
                    this.TaskQueue.SetIsWorking(true);
                    this.TaskQueue.SetWorkingId(work.Id);
                    await _jobHub.Clients.All.SendAsync("working", true);
                    await _jobHub.Clients.All.SendAsync("id", work.Id);
                    await work.WorkItem(stoppingToken);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error occurred executing {WorkItem}.", nameof(work));
                }
                finally
                {
                    this.TaskQueue.SetIsWorking(false);
                    this.TaskQueue.SetWorkingId(null);
                    await _jobHub.Clients.All.SendAsync("working", false);
                    await _jobHub.Clients.All.SendAsync("id", null);
                }
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Queued Hosted Service is stopping.");

            await base.StopAsync(stoppingToken);
        }
    }
}
