﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Application.Background
{
    public class Work
    {
        public string Id { get; set; }
        public Func<CancellationToken, Task> WorkItem { get; set; }
    }
    public interface IBackgroundTaskQueue
    {
        void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem);
        void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem, string id);
        Task<Work> DequeueAsync(
             CancellationToken cancellationToken);
        void SetIsWorking(bool working);

        bool GetIsWorking();

        void SetWorkingId(string id);
        string GetWorkingId();
    }

    public interface IBackgroundTaskQueueSecondary : IBackgroundTaskQueue
    {

    }
    public class BackgroundTaskQueue : IBackgroundTaskQueue
    {
        private ConcurrentQueue<Work> _works =
        new ConcurrentQueue<Work>();
        private SemaphoreSlim _signal = new SemaphoreSlim(0);
        private bool _isWorking = false;
        private string _workingId = null;

        public void QueueBackgroundWorkItem(
            Func<CancellationToken, Task> workItem)
        {
            this.QueueBackgroundWorkItem(workItem, null);
        }

        public void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem, string id)
        {
            if (workItem == null)
            {
                throw new ArgumentNullException(nameof(workItem));
            }
            Work work = new Work
            {
                Id = id,
                WorkItem = workItem
            };
            _works.Enqueue(work);
            _signal.Release();
        }


        public async Task<Work> DequeueAsync(
            CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _works.TryDequeue(out var workItem);

            return workItem;
        }

        public void SetIsWorking(bool isWorking)
        {
            this._isWorking = isWorking;
        }

        public bool GetIsWorking()
        {
            return this._isWorking;
        }
        public void SetWorkingId(string id)
        {
            this._workingId = id;
        }

        public string GetWorkingId()
        {
            return this._workingId;
        }
    }


    public class BackgroundTaskQueueSecondary : BackgroundTaskQueue,IBackgroundTaskQueueSecondary
    {

    }
}
