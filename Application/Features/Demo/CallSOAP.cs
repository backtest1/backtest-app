﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Application.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Behaviors;
using Application.Features.SU.Services;
using System.Xml.Linq;
using System.Net.Http;

namespace Application.Features.Demo
{
    public class SOAPResponse
    {
       public string Template {get;set;}
       public string Result {get;set;}
       public HttpResponseMessage Message {get;set;}
    }
    public class CallSOAP
    {
        public class Command : ICommand<SOAPResponse>
        {
            public int Action { get; set; } //1 add ,2 reset password,3 disable ,4 enable
        }

        public class Handler : IRequestHandler<Command, SOAPResponse>
        {
            private readonly ISOAPService _soap;
            public Handler(ISOAPService soap)
            {
                _soap = soap;
            }

            public async Task<SOAPResponse> Handle(Command request, CancellationToken cancellationToken)
            {
                var url = "http://wsap.dpu.ac.th/adsync/usermanagementservice.asmx";

                XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
                XNamespace myns = "http://tempuri.org/";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

                var element = new XElement(ns + "Envelope");
                HttpResponseMessage result = null;
                SOAPResponse response= new SOAPResponse();
                switch (request.Action)
                {
                    case 1:
                        element = new XElement(ns + "Envelope",
                                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                                new XAttribute(XNamespace.Xmlns + "soap12", ns),
                                new XElement(ns + "Body",
                                    new XElement(myns + "UserAdd",
                                        new XElement(myns + "StudentID", "900012"),
                                        new XElement(myns + "FirstName", "testFirstName"),
                                        new XElement(myns + "LastName", "testLastName"),
                                        new XElement(myns + "Faculty", "New_Student"),
                                        new XElement(myns + "Department", "Student_64"),
                                        new XElement(myns + "CitizenID", "4444444444444"),
                                        new XElement(myns + "PassportID", ""),
                                        new XElement(myns + "BirthDate", ""),
                                        new XElement(myns + "Nationality", ""),
                                        new XElement(myns + "PreferredLanguage", ""),
                                        new XElement(myns + "PhoneNumber", ""),
                                        new XElement(myns + "SecondaryEmail","jukkarin@siamsbuy.com"),
                                        new XElement(myns + "Password", "44444444")
                                    )
                                )
                            );

                        result = await _soap.PostAsync(url, element.ToString());
                        response.Template = element.ToString();
                        break;
                    case 2:
                        element = new XElement(ns + "Envelope",
                                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                                new XAttribute(XNamespace.Xmlns + "soap12", ns),
                                new XElement(ns + "Body",
                                    new XElement(myns + "ResetPassword",
                                        new XElement(myns + "StudentID", "900012"),
                                        new XElement(myns + "Password", "test2Password")
                                    )
                                )
                            );
                        result = await _soap.PostAsync(url, element.ToString());
                        response.Template = element.ToString();
                        break;
                    case 3:
                        element = new XElement(ns + "Envelope",
                                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                                new XAttribute(XNamespace.Xmlns + "soap12", ns),
                                new XElement(ns + "Body",
                                    new XElement(myns + "UserDisable",
                                        new XElement(myns + "StudentID", "900012")
                                    )
                                )
                            );
                        result = await _soap.PostAsync(url, element.ToString());
                        response.Template = element.ToString();
                        break;
                    case 4:
                        element = new XElement(ns + "Envelope",
                                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                                new XAttribute(XNamespace.Xmlns + "soap12", ns),
                                new XElement(ns + "Body",
                                    new XElement(myns + "UserEnable",
                                        new XElement(myns + "StudentID", "900012")
                                    )
                                )
                            );
                        result = await _soap.PostAsync(url, element.ToString());
                        response.Template = element.ToString();
                        break;
                }
                response.Message = result;
                response.Result = await result.Content.ReadAsStringAsync();
                return response;
            }
        }
    }

}
