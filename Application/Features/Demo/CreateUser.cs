﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Application.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Behaviors;
using Application.Features.SU.Services;

namespace Application.Features.Demo
{
    public class CreateUser
    {
        public class Command : ICommand
        {

        }

        public class Handler : IRequestHandler<Command, Unit>
        {
            private readonly UserService _user;

            public Handler(UserService user)
            {
                _user = user;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
               //var result =  await _user.CreateStudentService(1, null, cancellationToken);
                return Unit.Value;
            }
        }
    }

}
