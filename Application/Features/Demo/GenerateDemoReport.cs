﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Application.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Behaviors;

namespace Application.Features.Demo
{
    public class GenerateDemoReport : ICommand<byte[]>
    {
        public string FacCode { get; set; }
        public string ReportName { get; set; }
        public string ExportType { get; set; }

        public class Handler : IRequestHandler<GenerateDemoReport, byte[]>
        {
            private readonly IReportService _report;
            private readonly ICurrentUserAccessor _user;

            public Handler(IReportService report, ICurrentUserAccessor user)
            {
                _report = report;
                _user = user;
            }

            public async Task<byte[]> Handle(GenerateDemoReport request, CancellationToken cancellationToken)
            {
                var contetnt = new
                {
                    langCode = _user.Language,
                    facCode = request.FacCode,
                    reportName = request.ReportName,
                    exportType = request.ExportType
                };
                return await _report.PostAsByteArrayAsync("reportDemo", contetnt);
            }
        }
    }
}
