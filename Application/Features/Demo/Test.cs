﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Application.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Behaviors;
using Application.Features.SU.Services;

namespace Application.Features.Demo
{
    public class Test
    {
        public class Query : IRequest<string>
        {

        }

        public class Handler : IRequestHandler<Query, string>
        {
            private readonly ICleanDbContext _context;

            public Handler(ICleanDbContext context)
            {
                _context = context;
            }

            public async Task<string> Handle(Query request, CancellationToken cancellationToken)
            {
                var result = await _context.ExecuteScalarAsync<string>("select country_name_tha from db_country", null, cancellationToken);
                return result;
            }
        }
    }

}
