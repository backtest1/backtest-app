﻿using Application.Common.Constants;
using Application.Interfaces;
using Domain.Entities.SU;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Application.Features.Demo.Services
{
    public class UploadService : IService
    {
        private readonly IHttpService _client;
        private readonly ICleanDbContext _context;
        private readonly IReportService _report;
        public UploadService(ICleanDbContext context,IReportService report
            , IHttpService client
            )
        {
            _context = context;
            _report = report;
            _client = client;
        }

        public async Task UploadReport()
        {
            var body = new
            {
                module = "DEMO",
                reportName = "ReportTestList",
                exportType = "PDF",
                paramsJson = new
                {
                    company_code = "000",
                    lin_id = "th",
                    user_name = "admin"
                }
            };

            var file = await _report.PostAsByteArrayAsync("/exportReport", body);


            var contentApi = await _context.GetParameterValue<string>("Path", "ShareContent");

            var multipartContent = new MultipartFormDataContent();
            ByteArrayContent byteArrayContent = new ByteArrayContent(file);
            multipartContent.Add(byteArrayContent, "file", "ReportTestList.pdf");
            multipartContent.Add(new StringContent("SU/Example"),"category");
            multipartContent.Add(new StringContent("true"), "onlyFile");

            var content = await _client.PostAsync<SuContent>($"{contentApi}api/contentinternal/File", multipartContent);
        }
    }
}
