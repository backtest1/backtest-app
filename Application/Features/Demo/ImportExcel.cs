﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Application.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Behaviors;
using ClosedXML.Excel;

namespace Application.Features.Demo
{
    public class ImportExcel
    {
        public class DemoExcel
        {
            public string Company { get; set; }
            public string Employee { get; set; }
        }
        public class Command : ICommand<IEnumerable<DemoExcel>>
        {
   
        }

        public class Handler : IRequestHandler<Command, IEnumerable<DemoExcel>>
        {
            private readonly IReportService _http;
            public Handler(IReportService http)
            {
                 _http = http;
            }

            public async Task<IEnumerable<DemoExcel>> Handle(Command request, CancellationToken cancellationToken)
            {
                var path = "https://ctsdpu.softsquare.ga/File/SU/Example/eo21gvnj.xlsx";
                var file = await _http.GetAsByteArrayAsync(path);
                var demos = new List<DemoExcel>();
                using (XLWorkbook workBook = new XLWorkbook(new MemoryStream(file)))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);
                    bool firstRow = true;
                    foreach (IXLRow row in workSheet.RowsUsed())
                    {
                        var demo = new DemoExcel();
                        if (firstRow)
                        {
                            firstRow = false;
                            continue;
                        }
                        else
                        {
                            demo.Company = row.Cell(1).Value.ToString();
                            demo.Employee = row.Cell(2).Value.ToString();
                            demos.Add(demo);
                        }
                    }
                }
                return demos;
            }
        }
    }

}
