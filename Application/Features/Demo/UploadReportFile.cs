﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using Application.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Behaviors;
using Application.Features.SU.Services;
using Application.Features.Demo.Services;

namespace Application.Features.Demo
{
    public class UploadReportFile
    {
        public class Command : ICommand
        {

        }

        public class Handler : IRequestHandler<Command, Unit>
        {
            private readonly ICleanDbContext _context;
            private readonly UploadService _upload;

            public Handler(ICleanDbContext context, UploadService upload)
            {
                _context = context;
                _upload = upload;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                await _upload.UploadReport();
                return Unit.Value;
            }
        }
    }

}
