﻿using Application.Background;
using Application.Common.Constants;
using Application.Common.Helpers;
using Application.Common.Models;
using Application.Exceptions;
using Application.Hubs;
using Application.Interfaces;
using Application.Interfaces.Email;
using Domain.Entities.DB;
using Domain.Entities.SU;
using Domain.Types;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Application.Features.SU.Services
{
    public class Result
    {
        public long Id { get; set; }
        public bool haveEmail { get; set; }
    }

    public class CreateAd
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Faculty { get; set; }
        public string Department { get; set; }
        public string CitizenID { get; set; }
        public string PassportID { get; set; }
        public string SecondaryEmail { get; set; }
        public string Password { get; set; }
        public bool IsStudent { get; set; }
    }
    public class UserService : IService
    {
        private readonly ICleanDbContext _context;
        private readonly ICurrentUserAccessor _user;
        private readonly IIdentityService _identity;
        private readonly ISOAPService _soap;
        private readonly IEmailSender _email;
        private readonly ILogger<UserService> _logger;
        private readonly IHubContext<MigrateJobHub> _jobHub;

        private CancellationToken JobToken;
        public void SetJobToken(CancellationToken token) => this.JobToken = token;
        protected virtual CancellationToken CancellationToken => default;
        private string _defaultUsername = "@dpu.ac.th";
        private string _defaultPassword = "dpu";
        public UserService(ICleanDbContext context, ICurrentUserAccessor user, IIdentityService identity, ISOAPService soap, IEmailSender email, ILogger<UserService> logger, IHubContext<MigrateJobHub> jobHub)
        {
            _context = context;
            _user = user;
            _identity = identity;
            _soap = soap;
            _email = email;
            _logger = logger;
            _jobHub = jobHub;
            _defaultUsername = _context.GetParameterValue<string>("SuSetDefault", "Username").Result;
            _defaultPassword = _context.GetParameterValue<string>("SuSetDefault", "Password").Result;
        }

        public async Task<string> GetUsernameNotTaken(bool isStudent, string firstname, string lastname, string code, bool appendDefaultKeyword = true)
        {
            if (isStudent == false && (string.IsNullOrWhiteSpace(firstname) || string.IsNullOrWhiteSpace(lastname)))
            {
                throw new RestException(HttpStatusCode.BadRequest, "message.SU00009", code);
            }

            var username = string.Empty;
            if (isStudent)
            {
                username = code + (appendDefaultKeyword ? _defaultUsername : string.Empty);
                if (await _context.Set<SuUser>().AnyAsync(o => o.UserName.ToLower() == username.ToLower(), CancellationToken))
                {
                    throw new RestException(HttpStatusCode.BadRequest, "message.SU00010");
                }
                return username;
            }
            else
            {
                var lastnameMax = lastname.Length;
                int defaultLength = 3;
                int defaultEndIndex = lastnameMax < defaultLength ? lastnameMax : defaultLength;
                for (int i = 0; i < lastnameMax; i++)
                {
                    if (defaultEndIndex + i > lastnameMax)
                    {
                        return string.Empty;
                    }
                    username = firstname.ToLower() + "." + lastname.Substring(0, defaultEndIndex - 1).ToLower() + lastname.Substring(defaultEndIndex - 1 + i, 1).ToLower() + (appendDefaultKeyword ? _defaultUsername : string.Empty);
                    if (await _context.Set<SuUser>().AnyAsync(o => o.UserName.ToLower() == username.ToLower(), CancellationToken))
                    {
                        continue;
                    }
                    else return username;
                }
                throw new RestException(HttpStatusCode.BadRequest, "message.SU00010");
            }
        }

        //public async Task<Result> CreateStudentService(long? studentId, ShStudent student, CancellationToken cancellationToken)
        //{
        //    return await this.CreateStudentService(studentId, student, true, null, cancellationToken);
        //}
        //public async Task<Result> CreateStudentService(long? studentId, ShStudent student, bool isSendEmail, CurrentUser currentUser, CancellationToken cancellationToken)
        //{
        //    bool isJobUser = currentUser != null;
        //    if (student == null)
        //        student = await _context.Set<ShStudent>().AsNoTracking().FirstOrDefaultAsync(o => o.StudentId == studentId, cancellationToken);

        //    if (isJobUser && string.IsNullOrWhiteSpace(student?.Email))
        //    {
        //        throw new RestException(HttpStatusCode.BadRequest, $"ไม่พบ email {student.StudentCode}");
        //    }

        //    var username = isJobUser ? student.Email : await GetUsernameNotTaken(true, student.FirstNameEng, student.LastNameEng, student.StudentCode);
        //    var exists = await _context.Set<SuUser>().AnyAsync(o => o.UserName.ToLower() == username.ToLower(), cancellationToken);

        //    if (exists)
        //    {
        //        throw new RestException(HttpStatusCode.BadRequest, "message.STD00004", "label.User.Username");
        //    }

        //    var studentType = await _context.GetParameterValue<string>("SuUserType", "Student", cancellationToken);
        //    var saveResult = new Result();


        //    var password = _defaultPassword + student.StudentCode;

        //    var user = new SuUser();
        //    user.UserName = username;
        //    user.DefaultLang = Lang.th;
        //    user.PasswordPolicyCode = await _context.GetParameterValue<string>("SuSetDefault", "PolicyStudent", cancellationToken);
        //    user.ForceChangePassword = true;
        //    user.LastChangePassword = DateTime.Today;
        //    user.StartEffectiveDate = student.RegisterDate;
        //    user.Active = true;
        //    user.CreatedBy = isJobUser ? currentUser.UserName : _user.UserName;
        //    user.CreatedDate = DateTime.Now;
        //    user.CreatedProgram = isJobUser ? currentUser.ProgramCode : _user.ProgramCode;
        //    user.UpdatedBy = isJobUser ? currentUser.UserName : _user.UserName;
        //    user.UpdatedDate = DateTime.Now;
        //    user.UpdatedProgram = isJobUser ? currentUser.ProgramCode : _user.ProgramCode;

        //    var result = await _identity.CreateUserAsync(user, password);
        //    var profiles = await _context.Set<SuParameter>().Where(o => o.ParameterGroupCode == "SuSetDefaultProfile" && o.ParameterCode.Contains("ProfileStudent")).AsNoTracking().ToListAsync(cancellationToken);

        //    foreach (var profile in profiles)
        //    {
        //        var userProfile = new SuUserProfile();
        //        userProfile.Id = result.UserId;
        //        userProfile.ProfileCode = profile?.ParameterValue;
        //        if (await _context.Set<SuProfile>().AnyAsync(o => o.ProfileCode == userProfile.ProfileCode, cancellationToken))
        //        {
        //            _context.Set<SuUserProfile>().Add(userProfile);
        //        }
        //    }

        //    var userType = new SuUserType
        //    {
        //        UserId = result.UserId,
        //        CompanyCode = student.CompanyCode,
        //        UserType = studentType,
        //        StudentId = student.StudentId
        //    };

        //    _context.Set<SuUserType>().Add(userType);
        //    if (isJobUser) await _context.SaveChangesAsync(currentUser.UserName, currentUser.ProgramCode, cancellationToken); else await _context.SaveChangesAsync(cancellationToken);


        //    if (isJobUser == false)
        //    {
        //        var ad = new CreateAd();
        //        ad.IsStudent = true;
        //        ad.UserId = result.UserId;
        //        ad.UserName = user.UserName;
        //        ad.Id = student.StudentCode;
        //        ad.FirstName = student?.FirstNameTha;
        //        ad.LastName = student?.LastNameTha;
        //        ad.Faculty = "New_Student";
        //        ad.Department = student.RegisterYear.HasValue ? $"Student_{student.RegisterYear.ToString().Substring(2, 2)}" : string.Empty;
        //        ad.CitizenID = student.IdCard?.ToString();
        //        ad.PassportID = student.PassportNo?.ToString();
        //        ad.SecondaryEmail = student?.EmailSecon;
        //        ad.Password = password;
        //        await this.CreateActiveDirectoryUser(ad, cancellationToken);
        //    }

        //    saveResult.haveEmail = false;
        //    if (isSendEmail && !string.IsNullOrWhiteSpace(student?.EmailSecon))
        //    {
        //        try
        //        {
        //            var param = new Dictionary<string, string>();
        //            param.Add("[UserName]", isJobUser ? currentUser.UserName : user.UserName);
        //            param.Add("[Password]", password);
        //            param.Add("[FullNameTha]", student.StudentNameTha);
        //            param.Add("[FullNameEng]", student.StudentNameEng);
        //            await _email.SendEmailWithTemplateAsysnc("SU004", student.EmailSecon, null, param);
        //            saveResult.haveEmail = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.LogError(ex, "Send email create student user fail.");
        //        }
        //    }

        //    saveResult.Id = result.UserId;
        //    return saveResult;
        //}

        public async Task UpdateStudentUserEndDate(long studentId, DateTime endDate)
        {
            var userType = await _context.Set<SuUserType>().AsNoTracking().FirstOrDefaultAsync(o => o.StudentId == studentId && o.UserType == UserType.Student, CancellationToken);
            if (userType == null)
            {
                throw new RestException(HttpStatusCode.NotFound, "message.NotFound");
            }
            await this.UpdateUserEndDate(userType.UserId, userType.UserType, endDate);
        }
        public async Task UpdateEmployeeUserEndDate(string companyCode, string employeeCode, DateTime endDate)
        {
            var userType = await _context.Set<SuUserType>().AsNoTracking().FirstOrDefaultAsync(o => o.CompanyCode == companyCode && o.EmployeeCode == employeeCode && o.UserType != UserType.Student, CancellationToken);
            if (userType == null)
            {
                throw new RestException(HttpStatusCode.NotFound, "message.NotFound");
            }
            await this.UpdateUserEndDate(userType.UserId, userType.UserType, endDate);
        }

        private async Task UpdateUserEndDate(long userId, string userType, DateTime endDate)
        {
            var user = await _context.Set<SuUser>().FindAsync(userId);
            if (user == null)
            {
                throw new RestException(HttpStatusCode.NotFound, "message.NotFound");
            }
            user.EndEffectiveDate = endDate;
            await _context.SaveChangesAsync(CancellationToken);
            var studentType = await _context.GetParameterValue<string>("SuUserType", "Student", CancellationToken);
            var visiting = await _context.GetParameterValue<string>("SuUserType", "VisitingProfessor", CancellationToken);
            if (userType != visiting && endDate.Date == DateTime.Today.Date)
            {
                await this.DisableActiveDirectoryUser(user, user.Id, user.UserName, user.UserName.Replace(_defaultUsername, string.Empty), studentType == userType,null, CancellationToken);
            }
        }

        //public async Task ChangeStudentUserName(long studentId, string userName = "")
        //{
        //    var newUserName = userName;
        //    if (string.IsNullOrWhiteSpace(newUserName))
        //    {
        //        var student = await _context.Set<ShStudent>().FirstOrDefaultAsync(o => o.StudentId == studentId, CancellationToken);
        //        newUserName = student.StudentCode;
        //    }

        //    string userNamePattern = @"^[A-Za-z0-9@_\.-]+$";
        //    Regex rg = new Regex(userNamePattern);
        //    if (!rg.IsMatch(newUserName))
        //    {
        //        throw new RestException(HttpStatusCode.BadRequest, "message.SU00021");
        //    }

        //    var userType = await _context.Set<SuUserType>().FirstOrDefaultAsync(o => o.StudentId == studentId, CancellationToken);
        //    if (userType == null)
        //    {
        //        throw new RestException(HttpStatusCode.NotFound, "message.NotFound");
        //    }
        //    var user = await _context.Set<SuUser>().FindAsync(userType.UserId);
        //    user.UserName = newUserName;
        //    await _context.SaveChangesAsync(CancellationToken);
        //}

        //public async Task CreateUserFromStudents(CurrentUser user, int? registerYear, string jobName)
        //{
        //    await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress(0, "get students"));

        //    var graduate = await _context.GetParameterValue<string>("StudentStatus", "GraduateStatus", JobToken);
        //    var retire = await _context.GetParameterValue<string>("StudentStatus", "RetireStatus", JobToken);
        //    StringBuilder sql = new StringBuilder();
        //    sql.AppendLine("select * from sh_student s");
        //    sql.AppendLine("where register_year = @Year ");
        //    sql.AppendLine("and  not exists(select 'x' ");
        //    sql.AppendLine("                from su_user_type t ");
        //    sql.AppendLine("                where t.student_id = s.student_id )");
        //    sql.AppendLine("and ( status_code not in (@Graudate,@Retire)");
        //    sql.AppendLine("      or (status_code = @Graudate and graduate_year >= 2562) ");
        //    sql.AppendLine("     )");
        //    var students = await _context.QueryAsync<ShStudent>(sql.ToString(), new { Graudate = graduate, Retire = retire, Year = registerYear }, JobToken);
        //    int percent = 0;
        //    decimal totalLoop = students.Count();
        //    foreach (var student in students)
        //    {
        //        await ResilientTransaction.New(_context).ExecuteAsync(async () =>
        //        {
        //            try
        //            {
        //                _logger.LogInformation($"Student : {student.StudentCode}");

        //                if (!this.JobToken.IsCancellationRequested)
        //                {
        //                    await this.CreateStudentService(null, student, false, user, JobToken);
        //                    ++percent;
        //                    await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress((100.00m * percent) / totalLoop, $"Student : {student.StudentCode}"));
        //                }
        //                else
        //                {
        //                    throw new OperationCanceledException();
        //                }
        //            }
        //            catch (OperationCanceledException ex)
        //            {
        //                await _jobHub.Clients.Group(jobName).SendAsync("progress");
        //                throw ex;
        //            }
        //            catch (RestException ex)
        //            {
        //                _logger.LogError(ex, $"Cannot create user account for student : {student.StudentId},{student.StudentCode}");
        //                await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress((100.00m * percent) / totalLoop, $"พบปัญหา : รหัส {student.StudentCode} { (ex.Errors as ErrorMessage).Code}", true));
        //            }
        //            catch (Exception ex)
        //            {
        //                _logger.LogError(ex, $"Cannot create user account for student : {student.StudentId},{student.StudentCode}");
        //                await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress((100.00m * percent) / totalLoop, $"พบปัญหา : รหัส {student.StudentCode} {ex.Message}", true));
        //            }
        //        });
        //    }
        //    await _jobHub.Clients.Group(jobName).SendAsync("progress");

        //}

        private async Task AddDivision(ICollection<SuUserDivision> userDivisions, string company, string divParent)
        {
            if (!string.IsNullOrEmpty(divParent))
            {
                var isExist = await _context.Set<SuDivision>().AnyAsync(o => o.CompanyCode == company && o.DivCode == divParent);
                if (isExist && !userDivisions.Any(p => p.DivCode == divParent))
                {
                    userDivisions.Add(new SuUserDivision() { DivCode = divParent });
                    var children = await _context.Set<SuDivision>().Where(o => o.DivParent == divParent).AsNoTracking().ToListAsync(CancellationToken);
                    foreach (var div in children)
                    {
                        await AddDivision(userDivisions, company, div.DivCode);
                    }
                }

            }
        }

        private async Task InsertUserTemp(CurrentUser audit, long userId, string username, string password, DbEmployee employee, CancellationToken cancellation)
        {
            SuUserTemp temp = new SuUserTemp();
            temp.UserId = userId;
            temp.UserName = username;
            temp.Password = password;
            temp.FullNameTha = employee.tNameConcat;
            temp.FullNameEng = employee.eNameConcat;
            temp.Email = employee.Email;
            temp.IsSended = false;
            _context.Set<SuUserTemp>().Add(temp);
            await _context.SaveChangesAsync(audit.UserName, audit.ProgramCode, cancellation);
        }
        private async Task CreateUserFromEmployeeService(CurrentUser audit, DbEmployee employee, string policy, IEnumerable<SuParameter> profiles, Dictionary<string, string> userTypeMap, CancellationToken cancellation)
        {
            if (string.IsNullOrEmpty(employee?.Email))
            {
                throw new RestException(HttpStatusCode.BadRequest, $"ไม่พบ email บุคคลากร ของ {employee.EmployeeCode}");
            }
            var user = new SuUser();
            user.UserName = employee.Email;
            user.DefaultLang = "th";
            user.PasswordPolicyCode = policy;
            user.ForceChangePassword = true;
            user.LastChangePassword = DateTime.Today;
            user.StartEffectiveDate = DateTime.Today;
            user.Active = true;
            user.CreatedBy = audit.UserName;
            user.CreatedDate = DateTime.Now;
            user.CreatedProgram = audit.ProgramCode;
            user.UpdatedBy = audit.UserName;
            user.UpdatedDate = DateTime.Now;
            user.UpdatedProgram = audit.ProgramCode;
            var password = _identity.GeneratePassword();
            var result = await _identity.CreateUserAsync(user, password);
            if (!result.Result.Succeeded)
            {
                throw new RestException(HttpStatusCode.BadRequest, String.Join(",", result.Result.Errors));
            }

            var userType = new SuUserType
            {
                UserId = result.UserId,
                CompanyCode = employee.CompanyCode,
                EmployeeCode = employee.EmployeeCode,
                UserType = userTypeMap[employee.GroupTypeCode],
            };
            _context.Set<SuUserType>().Add(userType);
            await _context.SaveChangesAsync(audit.UserName, audit.ProgramCode, cancellation);

            if (userTypeMap[employee.GroupTypeCode] != "E")
            {
                foreach (var profile in profiles)
                {
                    var userProfile = new SuUserProfile();
                    userProfile.Id = result.UserId;
                    userProfile.ProfileCode = profile?.ParameterValue;
                    if (await _context.Set<SuProfile>().AnyAsync(o => o.ProfileCode == userProfile.ProfileCode, cancellation))
                    {
                        _context.Set<SuUserProfile>().Add(userProfile);
                    }
                }
                await _context.SaveChangesAsync(audit.UserName, audit.ProgramCode, cancellation);
            }

            if (userTypeMap[employee.GroupTypeCode] == "P")
            {
                var companies = await _context.Set<SuCompany>().AsNoTracking().ToListAsync(cancellation);
                foreach (var company in companies)
                {
                    var userPermission = new SuUserPermission();
                    userPermission.Id = result.UserId;
                    userPermission.CompanyCode = company.CompanyCode;
                    userPermission.EduLevels = new List<SuUserEduLevel>();

                    var eduLevels = await _context.QueryAsync<string>("select education_type_level from db_education_type_level where company_code = @Company and active = true",
                    new
                    {
                        Company = company.CompanyCode,

                    }, cancellation);
                    foreach (var eduLevel in eduLevels)
                    {
                        var edu = new SuUserEduLevel();
                        edu.EducationTypeLevel = eduLevel;
                        userPermission.EduLevels.Add(edu);
                    }
                    _context.Set<SuUserPermission>().Add(userPermission);
                }
                await _context.SaveChangesAsync(audit.UserName, audit.ProgramCode, cancellation);
            }
            await InsertUserTemp(audit, result.UserId, user.UserName, password, employee, cancellation);
        }
        public async Task CreateUserFromEmployees(CurrentUser user, string jobName)
        {
            await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress(0, "get employees"));

            var professor = await _context.GetParameterValue<string>("SuEmpGroupType", "Professor", JobToken);
            var visiting = await _context.GetParameterValue<string>("SuEmpGroupType", "VisitingProfessor", JobToken);
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("select * from db_employee s");
            sql.AppendLine("where not exists(select 'x' ");
            sql.AppendLine("                from su_user_type t ");
            sql.AppendLine("                where t.company_code = s.company_code and t.employee_code = s.employee_code )");
            var employees = await _context.QueryAsync<DbEmployee>(sql.ToString(), new { Company = user.CompanyCode, Professor = professor, Visiting = visiting }, JobToken);
            int percent = 0;
            decimal totalLoop = employees.Count();
            var userTypeMap = new Dictionary<string, string>();
            var groups = await _context.QueryAsync<dynamic>("select parameter_code as code,parameter_value as value from su_parameter where parameter_group_code = 'SuEmpGroupType' ", null, JobToken);
            foreach (var group in groups)
            {
                userTypeMap[group.value] = await _context.GetParameterValue<string>("SuUserType", group.code, JobToken);
            }
            var policy = await _context.GetParameterValue<string>("SuSetDefault", "PolicyEmployee", JobToken);

            var profiles = await _context.Set<SuParameter>().Where(o => o.ParameterGroupCode == "SuSetDefaultProfile" && o.ParameterCode.Contains("ProfileProfessor")).AsNoTracking().ToListAsync(JobToken);

            foreach (var employee in employees)
            {
                await ResilientTransaction.New(_context).ExecuteAsync(async () =>
                {
                    try
                    {
                        _logger.LogInformation($"Employee : {employee.EmployeeCode}");
                        if (!this.JobToken.IsCancellationRequested)
                        {
                            await this.CreateUserFromEmployeeService(user, employee, policy, profiles, userTypeMap, JobToken);
                            ++percent;
                            await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress((100.00m * percent) / totalLoop, $"Employee : {employee.EmployeeCode}"));
                        }
                        else
                        {
                            throw new OperationCanceledException();
                        }
                    }
                    catch (OperationCanceledException ex)
                    {
                        await _jobHub.Clients.Group(jobName).SendAsync("progress");
                        throw ex;
                    }
                    catch (RestException ex)
                    {
                        _logger.LogError(ex, $"Cannot create user account for employee : {employee.EmployeeCode}");
                        await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress((100.00m * percent) / totalLoop, $"พบปัญหา : รหัส {employee.EmployeeCode} { (ex.Errors as ErrorMessage).Code}", true));
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Cannot create user account for employee : {employee.EmployeeCode}");
                        await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress((100.00m * percent) / totalLoop, $"พบปัญหา : รหัส {employee.EmployeeCode} {ex.Message}", true));
                    }
                });
            }
            await _jobHub.Clients.Group(jobName).SendAsync("progress");

        }

        public async Task DisableUser(CancellationToken cancellation)
        {
            var visiting = await _context.GetParameterValue<string>("SuUserType", "VisitingProfessor", cancellation);
            var studentType = await _context.GetParameterValue<string>("SuUserType", "Student", cancellation);

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("select *,xmin ");
            sql.AppendLine("from su_user u ");
            sql.AppendLine("where exists(select 'x' ");
            sql.AppendLine("            from su_user_type t ");
            sql.AppendLine("            where t.user_id = u.user_id ");
            sql.AppendLine("            and t.user_type <> {0} )");
            sql.AppendLine("and end_effective_date::date <= current_date and coalesce(u.is_disabled_ad,false) = false");
            var users = _context.Set<SuUser>().FromSql(sql.ToString(), visiting);
            foreach (var user in users.ToList())
            {
                var userType = await _context.Set<SuUserType>().FirstOrDefaultAsync(u => u.UserId == user.Id, cancellation);
                CurrentUser audit = new CurrentUser();
                audit.UserName = "system";
                audit.ProgramCode = "system";
                await this.DisableActiveDirectoryUser(user, user.Id, user.UserName, user.UserName.Replace(_defaultUsername, string.Empty), studentType == userType.UserType,audit, CancellationToken);
            }
        }

        public async Task SendEmailToProfessor(CurrentUser audit, string jobName)
        {
            await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress(0, "get user for send email"));
            var tempPass = await _context.Set<SuUserTemp>().Where(o => o.IsSended == null || o.IsSended == false).ToListAsync(JobToken);
            int percent = 0;
            decimal totalLoop = tempPass.Count();
            foreach (var temp in tempPass)
            {
                _logger.LogInformation($"user : {temp.UserName}");
                if (!this.JobToken.IsCancellationRequested)
                {
                    if (!string.IsNullOrEmpty(temp.Email))
                    {
                        try
                        {
                            var param = new Dictionary<string, string>();
                            param.Add("[UserName]", temp.UserName);
                            param.Add("[Password]", temp.Password);
                            param.Add("[FullNameTha]", temp.FullNameTha);
                            param.Add("[FullNameEng]", temp.FullNameEng);
                            await _email.SendEmailWithTemplateAsysnc("SU002", temp.Email, null, param);
                            temp.IsSended = true;
                            await _context.SaveChangesAsync(audit.UserName, audit.ProgramCode, JobToken);
                            ++percent;
                            await _jobHub.Clients.Group(jobName).SendAsync("progress", new Progress((100.00m * percent) / totalLoop, $"user : {temp.UserName}"));
                            Thread.Sleep(18000);
                        }
                        catch (OperationCanceledException ex)
                        {
                            await _jobHub.Clients.Group(jobName).SendAsync("progress");
                            throw ex;
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, $"SUMG03 job send email. username : {temp.UserName},email : {temp.Email}");
                        }
                    }

                }
                else
                {
                    throw new OperationCanceledException();
                }
            }
            await _jobHub.Clients.Group(jobName).SendAsync("progress");
        }
        public async Task CreateActiveDirectoryUser(CreateAd model, CancellationToken cancellation)
        {
            bool enable = await _context.GetParameterValue<bool>("ExternalService", "UserManagementEnable", cancellation);
            if (enable)
            {
                string forEmp = model.IsStudent ? string.Empty : "Emp";
                var url = await _context.GetParameterValue<string>("ExternalService", $"UserManagementUrl{forEmp}", cancellation);

                XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
                XNamespace myns = "http://tempuri.org/";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

                var element = new XElement(ns + "Envelope",
                                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                                new XAttribute(XNamespace.Xmlns + "soap12", ns),
                                new XElement(ns + "Body",
                                    new XElement(myns + "UserAdd",
                                        new XElement(myns + "StudentID", model.Id),
                                        new XElement(myns + "FirstName", model.FirstName),
                                        new XElement(myns + "LastName", model.LastName),
                                        new XElement(myns + "Faculty", model.Faculty),
                                        new XElement(myns + "Department", model.Department),
                                        new XElement(myns + "CitizenID", model.CitizenID),
                                        new XElement(myns + "PassportID", model.PassportID),
                                        new XElement(myns + "BirthDate", ""),
                                        new XElement(myns + "Nationality", ""),
                                        new XElement(myns + "PreferredLanguage", ""),
                                        new XElement(myns + "PhoneNumber", ""),
                                        new XElement(myns + "SecondaryEmail", model.SecondaryEmail),
                                        new XElement(myns + "Password", model.Password)
                                    )
                                )
                            );

                var log = new SuUserAdLog();
                log.UserId = model.UserId;
                log.UserName = model.UserName;
                log.AdStudentId = model.Id;
                log.AdFirstName = model.FirstName;
                log.AdLastName = model.LastName;
                log.AdFaculty = model.Faculty;
                log.AdDepartment = model.Department;
                log.AdCitizenId = model.CitizenID;
                log.AdPassportId = model.PassportID;
                log.AdSecondaryEmail = model.SecondaryEmail;
                log.AdFunction = "UserAdd";
                log.AdTemplate = Regex.Replace(element.ToString(),@"<Password>(.*)</Password>","<Password>******</Password>");
                try
                {
                    var response = await _soap.PostAsync(url, element.ToString());
                    var result = await response.Content.ReadAsStringAsync();
                    _logger.LogInformation("create user AD result : " + result);

                    log.AdResult = result;
                    _context.Set<SuUserAdLog>().Add(log);
                    await _context.SaveChangesAsync(cancellation);
                }
                catch (Exception ex)
                {
                    log.ErrorLog = ex.Message + " " + ex.InnerException.Message;
                    _context.Set<SuUserAdLog>().Add(log);
                    await _context.SaveChangesAsync(cancellation);
                }
            }
        }

        public async Task ResetPasswordActiveDirectoryUser(long userId, string username, string password, string adStudentId, bool isStudent, CancellationToken cancellation)
        {
            bool enable = await _context.GetParameterValue<bool>("ExternalService", "UserManagementEnable", cancellation);
            if (enable)
            {
                string forEmp = isStudent ? string.Empty : "Emp";
                var url = await _context.GetParameterValue<string>("ExternalService", $"UserManagementUrl{forEmp}", cancellation); //"http://wsap.dpu.ac.th/adsync/usermanagementservice.asmx";
                XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
                XNamespace myns = "http://tempuri.org/";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

                var element = new XElement(ns + "Envelope",
                                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                                new XAttribute(XNamespace.Xmlns + "soap12", ns),
                                new XElement(ns + "Body",
                                    new XElement(myns + "ResetPassword",
                                        new XElement(myns + "StudentID", adStudentId),
                                        new XElement(myns + "Password", password)
                                    )
                                )
                            );

                var log = new SuUserAdLog();
                log.UserId = userId;
                log.UserName = username;
                log.AdStudentId = adStudentId;
                log.AdFunction = "ResetPassword";
                log.AdTemplate = Regex.Replace(element.ToString(),@"<Password>(.*)</Password>","<Password>******</Password>");
                try
                {
                    var response = await _soap.PostAsync(url, element.ToString());
                    var result = await response.Content.ReadAsStringAsync();
                    _logger.LogInformation("reset password user AD result : " + result);
                    log.AdResult = result;
                    _context.Set<SuUserAdLog>().Add(log);
                    await _context.SaveChangesAsync(cancellation);
                }
                catch (Exception ex)
                {
                    log.ErrorLog = ex.Message + " " + ex.InnerException.Message;
                    _context.Set<SuUserAdLog>().Add(log);
                    await _context.SaveChangesAsync(cancellation);
                }
            }
        }

        public async Task DisableActiveDirectoryUser(SuUser user, long userId, string userName, string adStudentId, bool isStudent, CurrentUser audit, CancellationToken cancellation)
        {
            bool enable = await _context.GetParameterValue<bool>("ExternalService", "UserManagementEnable", cancellation);
            if (enable)
            {
                string forEmp = isStudent ? string.Empty : "Emp";
                var url = await _context.GetParameterValue<string>("ExternalService", $"UserManagementUrl{forEmp}", cancellation); //"http://wsap.dpu.ac.th/adsync/usermanagementservice.asmx";

                XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
                XNamespace myns = "http://tempuri.org/";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace xsd = "http://www.w3.org/2001/XMLSchema";
                var element = new XElement(ns + "Envelope",
                                new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                                new XAttribute(XNamespace.Xmlns + "soap12", ns),
                                new XElement(ns + "Body",
                                    new XElement(myns + "UserDisable",
                                        new XElement(myns + "StudentID", adStudentId)
                                    )
                                )
                            );
                var log = new SuUserAdLog();
                log.UserId = userId;
                log.UserName = userName;
                log.AdStudentId = adStudentId;
                log.AdFunction = "UserDisable";
                log.AdTemplate = element.ToString();
                try
                {
                    var response = await _soap.PostAsync(url, element.ToString());
                    response.EnsureSuccessStatusCode();
                    var result = await response.Content.ReadAsStringAsync();
                    _logger.LogInformation("disable user AD result : " + result);
                    log.AdResult = result;
                    _context.Set<SuUserAdLog>().Add(log);
                    await _context.SaveChangesAsync(cancellation);

                    Regex regex = new Regex("<UserDisableResult>(.*)</UserDisableResult>");
                    Match v = regex.Match(result);
                    string disableResult = v.Groups[1].ToString();
                    if (disableResult?.ToLower() == "success")
                    {
                        if (user == null)
                        {
                            user = await _context.Set<SuUser>().FindAsync(userId);
                        }
                        if (_context.Entry(user).State == EntityState.Detached)
                        {
                            _context.Set<SuUser>().Attach(user);
                        }
                        user.IsDisabledAd = true;
                        if (audit != null)
                        {
                            user.UpdatedBy = audit.UserName;
                            user.UpdatedProgram = audit.ProgramCode;
                        }
                        else
                        {
                            user.UpdatedBy = _user.UserName;
                            user.UpdatedProgram = _user.ProgramCode;
                        }
                        user.UpdatedDate = DateTime.Now;

                        await _context.SaveChangesAsync(cancellation);
                    }
                }
                catch (Exception ex)
                {
                    log.ErrorLog = ex.Message + " " + ex.InnerException.Message;
                    _context.Set<SuUserAdLog>().Add(log);
                    await _context.SaveChangesAsync(cancellation);
                }

            }

        }
    }
}
