﻿using Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT08
{
    public class List
    {
        public class Query : RequestPageQuery, IRequest<PageDto>
        {
            public string Keyword { get; set; }
        }

        public class Handler : IRequestHandler<Query, PageDto>
        {
            private readonly ICleanDbContext _context;
            private readonly ICurrentUserAccessor _user;

            public Handler(ICleanDbContext context, ICurrentUserAccessor user)
            {
                _context = context;
                _user = user;
            }

            public async Task<PageDto> Handle(Query request, CancellationToken cancellationToken)
            {
                StringBuilder sql = new StringBuilder();

                sql.AppendLine("        select 	email_template_code as \"templateCode\",");
                sql.AppendLine("		        email_template_name as \"templateName\",");
                sql.AppendLine("		        subject,");
                sql.AppendLine("		        active,");
                sql.AppendLine("                xmin as \"rowVersion\"");
                sql.AppendLine("        from 	su_email_template");

                if (!string.IsNullOrWhiteSpace(request.Keyword))
                {
                    sql.AppendLine("    where   concat(email_template_code,");
                    sql.AppendLine("                   email_template_name,");
                    sql.AppendLine("                   subject,description)");
                    sql.AppendLine("            ilike concat('%', @Keyword, '%')");
                }

                return await _context.GetPage(sql.ToString(), new { Keyword = request.Keyword }, (RequestPageQuery)request, cancellationToken);
            }
        }
    }
}
