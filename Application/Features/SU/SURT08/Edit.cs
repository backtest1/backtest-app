﻿using Application.Behaviors;
using Application.Interfaces;
using Domain.Entities;
using Domain.Entities.SU;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT08
{
    public class Edit
    {
        public class Command : SuEmailTemplate, ICommand
        {

        }

        public class Handler : IRequestHandler<Command, Unit>
        {
            private readonly ICleanDbContext _context;
            public Handler(ICleanDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                _context.Set<SuEmailTemplateAttachment>().RemoveRange(request.EmailTemplateAttachments.Where(o => o.RowState == RowState.Delete));
                await _context.SaveChangesAsync(cancellationToken);

                request.EmailTemplateAttachments = request.EmailTemplateAttachments.Where(o => o.RowState != RowState.Delete).ToList();

                _context.Set<SuEmailTemplate>().Attach((SuEmailTemplate)request);
                _context.Entry((SuEmailTemplate)request).State = EntityState.Modified;
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
