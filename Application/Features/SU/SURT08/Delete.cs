﻿using Application.Behaviors;
using Application.Interfaces;
using Domain.Entities.SU;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT08
{
    public class Delete
    {
        public class Command : SuEmailTemplate, ICommand
        {

        }

        public class Handler : IRequestHandler<Command, Unit>
        {
            private readonly ICleanDbContext _context;
            private readonly ICurrentUserAccessor _user;

            public Handler(ICleanDbContext context, ICurrentUserAccessor user)
            {
                _context = context;
                _user = user;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                SuEmailTemplate emailTemplate = new SuEmailTemplate { EmailTemplateCode = request.EmailTemplateCode, RowVersion = request.RowVersion };
                _context.Set<SuEmailTemplate>().Remove(emailTemplate);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
