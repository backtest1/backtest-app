﻿using Application.Common.Mapping;
using Application.Exceptions;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities.SU;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT08
{
    public class Detail
    {
        public class TemplateVm : SuEmailTemplate, IMapFrom<SuEmailTemplate>
        {
            public new IEnumerable<AttachmentVm> EmailTemplateAttachments { get; set; }
        }

        public class AttachmentVm : SuEmailTemplateAttachment ,IMapFrom<SuEmailTemplateAttachment>
        {
            public string Path { get; set; }
            public string FileName { get; set; }
        }
        public class Query : IRequest<SuEmailTemplate>
        {
            public string EmailTemplateCode { get; set; }
        }

        public class Handler : IRequestHandler<Query, SuEmailTemplate>
        {
            private readonly ICleanDbContext _context;
            private readonly ICurrentUserAccessor _user;
            private readonly IMapper _mapper;

            public Handler(ICleanDbContext context, IMapper mapper, ICurrentUserAccessor user)
            {
                _context = context;
                _mapper = mapper;
                _user = user;
            }
            public async Task<SuEmailTemplate> Handle(Query request, CancellationToken cancellationToken)
            {
                var template = await _context.Set<SuEmailTemplate>().Where(i => i.EmailTemplateCode == request.EmailTemplateCode).ProjectTo<TemplateVm>(_mapper.ConfigurationProvider).AsNoTracking().FirstOrDefaultAsync(cancellationToken);
                
                if (template == null)
                    throw new RestException(HttpStatusCode.NotFound, "message.NotFound");
                StringBuilder sql = new StringBuilder();
                sql.AppendLine("SELECT a.*,c.name as file_name,c.path,a.xmin as row_version");
                sql.AppendLine("from su_email_template_attachment a ");
                sql.AppendLine("inner join su_content c on c.id = a.attachment_id ");
                sql.AppendLine("where a.email_template_code = @Code ");
                template.EmailTemplateAttachments = await _context.QueryAsync<AttachmentVm>(sql.ToString(), new { Code = request.EmailTemplateCode }, cancellationToken);

                return template;
            }
        }
    }
}
