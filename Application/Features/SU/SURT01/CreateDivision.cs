﻿using Application.Behaviors;
using Application.Exceptions;
using Application.Interfaces;
using Domain.Entities.SU;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT01
{
    public class CreateDivision
    {
        public class Command : SuDivision, ICommand
        {

        }

        public class Handler : IRequestHandler<Command, Unit>
        {
            private readonly ICleanDbContext _context;
            private readonly ICurrentUserAccessor _user;

            public Handler(ICleanDbContext context, ICurrentUserAccessor user)
            {
                _context = context;
                _user = user;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                if (_context.Set<SuDivision>().Any(i => i.CompanyCode == request.CompanyCode && i.DivCode.ToUpper() == request.DivCode.ToUpper()))
                {
                    throw new RestException(HttpStatusCode.BadRequest, "message.STD00004", "label.SURT01.DivCode");
                }

                _context.Set<SuDivision>().Add((SuDivision)request);
                if (!string.IsNullOrEmpty(request.DivParent))
                {
                    await GenerateDivision(request.DivParent, request.DivCode, cancellationToken);
                }

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }

            private async Task GenerateDivision(string parent, string divCode, CancellationToken cancellationToken)
            {
                var users = await _context.Set<SuUserDivision>().Where(o => o.DivCode == parent).AsNoTracking().ToListAsync(cancellationToken);
                foreach (var user in users)
                {
                    var userDivision = new SuUserDivision();
                    userDivision.CompanyCode = user.CompanyCode;
                    userDivision.UserId = user.UserId;
                    userDivision.DivCode = divCode;
                    _context.Set<SuUserDivision>().Add(userDivision);
                }
            }
        }
    }
}
