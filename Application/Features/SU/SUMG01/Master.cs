﻿using Application.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SUMG01
{
    public class Master
    {
        public class MasterData
        {
            public IEnumerable<dynamic> RegisterYears { get; set; }
        }

        public class Query : IRequest<MasterData>
        {

        }

        public class Handler : IRequestHandler<Query, MasterData>
        {
            private readonly ICleanDbContext _context;

            private readonly ICurrentUserAccessor _user;

            public Handler(ICleanDbContext context, ICurrentUserAccessor user)
            {
                _context = context;
                _user = user;
            }

            public async Task<MasterData> Handle(Query request, CancellationToken cancellationToken)
            {
                var graduate = await _context.GetParameterValue<string>("StudentStatus", "GraduateStatus", cancellationToken);
                var retire = await _context.GetParameterValue<string>("StudentStatus", "RetireStatus", cancellationToken);
                var master = new MasterData();
                var sql = new StringBuilder();
                sql.AppendLine("select  distinct register_year as text,register_year as value");
                sql.AppendLine("from     sh_student s");
                sql.AppendLine("	 where  not exists(select 'x' 	");
                sql.AppendLine("	                          from su_user_type t 	");
                sql.AppendLine("	                          where t.student_id = s.student_id )	");
                sql.AppendLine("	               and ( status_code not in (@Graudate,@Retire)	");
                sql.AppendLine("	                or (status_code = @Graudate and graduate_year >= 2562) 	");
                sql.AppendLine("	               )	");
                sql.AppendLine("order by register_year desc");
                master.RegisterYears = await _context.QueryAsync<dynamic>(sql.ToString(), new { Graudate = graduate,Retire = retire}, cancellationToken);

                return master;
            }
        }
    }
}
