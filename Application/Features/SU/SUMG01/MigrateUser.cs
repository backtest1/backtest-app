﻿using Application.Background;
using Application.Common.Models;
using Application.Features.SU.Services;
using Application.Hubs;
using Application.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SUMG01
{
    public class Criteria
    {
        public CurrentUser User { get; set; }
        public int? RegisterYear { get; set; }
        public bool IsStudent { get; set; }
        public string JobName { get; set; }
    }
    public class MigrateUser : IService
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly ILogger _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private static ConcurrentDictionary<string, CancellationTokenSource> _tokens = new ConcurrentDictionary<string, CancellationTokenSource>();
        public MigrateUser(IBackgroundTaskQueue taskQueue,
            ILogger<MigrateUser> logger,
            IServiceScopeFactory serviceScopeFactory)
        {
            _taskQueue = taskQueue;
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public void Migrate(Criteria criteria)
        {
            CancellationTokenSource newToken = new CancellationTokenSource();

            _tokens.TryAdd(criteria.JobName, newToken);
            // Enqueue a background work item
            _taskQueue.QueueBackgroundWorkItem(async token =>
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var user = scopedServices.GetRequiredService<UserService>();
                    user.SetJobToken(newToken.Token);
                    var guid = Guid.NewGuid().ToString();
                    
                    try
                    {

                        _logger.LogInformation("Queued Background migrate user {guid} Task start", guid);
                        //if (criteria.IsStudent) await user.CreateUserFromStudents(criteria.User,criteria.RegisterYear, criteria.JobName);
                        //else await user.CreateUserFromEmployees(criteria.User, criteria.JobName);
                        _logger.LogInformation("Queued Background migrate user {guid} Task finish", guid);

                    }
                    catch (OperationCanceledException)
                    {

                        // Prevent throwing if the Delay is cancelled
                    }

                }

            });
        }
        public void Stop(string jobName)
        {
            _tokens.TryRemove(jobName, out var token);
            if (token != null)
            {
                token.Cancel();
                token.Dispose();
            }
        }
    }
}
