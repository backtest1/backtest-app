﻿using Application.Behaviors;
using Application.Common.Constants;
using Application.Exceptions;
using Application.Features.Services;
using Application.Features.SU.Services;
using Application.Interfaces;
using Application.Interfaces.Email;
using Domain.Entities.DB;
using Domain.Entities.SU;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT06
{
    public class ResetPassword
    {
        public class Command : ICommand<bool>
        {
            public long userId { get; set; }
            public uint? RowVersion { get; set; }
        }

        public class Handler : IRequestHandler<Command, bool>
        {
            private readonly ICleanDbContext _context;
            private readonly ICurrentUserAccessor _user;
            private readonly IIdentityService _identity;
            private readonly IEmailSender _email;
            private readonly ILogger<Handler> _logger;
            private readonly UserService _ad;
            private string _defaultPassword = "dpu";
            public Handler(ICleanDbContext context, UserService ad, ICurrentUserAccessor user, IIdentityService identity, IEmailSender email, ILogger<Handler> logger)
            {
                _context = context;
                _user = user;
                _ad = ad;
                _identity = identity;
                _email = email;
                _logger = logger;
                _defaultPassword = _context.GetParameterValue<string>("SuSetDefault", "Password").Result;
            }

            public async Task<bool> Handle(Command request, CancellationToken cancellationToken)
            {
                //var user = await _context.Set<SuUser>().Where(o => o.Id == request.userId).FirstOrDefaultAsync(cancellationToken);
                //if (user == null)
                //{
                //    throw new RestException(HttpStatusCode.NotFound, "message.NotFound");
                //}
                //_context.Entry(user).Property("RowVersion").OriginalValue = request.RowVersion;

                //var userType = await _context.Set<SuUserType>().FirstOrDefaultAsync(o => o.UserId == user.Id, cancellationToken);

                //var studentType = await _context.GetParameterValue<string>("SuUserType", "Student", cancellationToken);

                //DbEmployee employee = null;
                //ShStudent student = null;
                //string password = string.Empty;
                //if (userType.UserType == studentType)
                //{
                //    student = await _context.Set<ShStudent>().FirstOrDefaultAsync(o => o.StudentId == userType.StudentId, cancellationToken);
                //    password = _defaultPassword + student.StudentCode;
                //}
                //else
                //{
                //    employee = await _context.Set<DbEmployee>().FirstOrDefaultAsync(o => o.CompanyCode == userType.CompanyCode && o.EmployeeCode == userType.EmployeeCode, cancellationToken);
                //    password = _identity.GeneratePassword();
                //}

                //user.LastChangePassword = DateTime.Today;
                //user.UpdatedBy = _user.UserName;
                //user.UpdatedDate = DateTime.Now;
                //user.UpdatedProgram = _user.ProgramCode;
                //var result = await _identity.ResetPassword(user, password);
                //bool canSendEmail = false;
                //if (result.Succeeded)
                //{
                //    var visiting = await _context.GetParameterValue<string>("SuUserType", "VisitingProfessor", cancellationToken);

                //    if (userType.UserType != visiting)
                //    {
                //        var defaultUsername = await _context.GetParameterValue<string>("SuSetDefault", "Username", cancellationToken);
                //        await _ad.ResetPasswordActiveDirectoryUser(user.Id, user.UserName, password, user.UserName.Replace(defaultUsername, string.Empty), studentType == userType.UserType, cancellationToken);
                //    }

                //    var email = userType.UserType == studentType ? student?.EmailSecon : employee?.EmailSecon;
                //    var emailTemplate = userType.UserType == studentType ? "SU004" : "SU002";
                //    var fullNameTha = userType.UserType == studentType ? student.StudentNameTha : employee.tNameConcat;
                //    var fullNameEng = userType.UserType == studentType ? student.StudentNameEng : employee.eNameConcat;
                //    if (!string.IsNullOrWhiteSpace(email))
                //    {
                //        try
                //        {
                //            var param = new Dictionary<string, string>();
                //            param.Add("[UserName]", user.UserName);
                //            param.Add("[Password]", password);
                //            param.Add("[FullNameTha]", fullNameTha);
                //            param.Add("[FullNameEng]", fullNameEng);
                //            await _email.SendEmailWithTemplateAsysnc(emailTemplate, email, null, param);
                //            canSendEmail = true;
                //        }
                //        catch (Exception ex)
                //        {
                //            _logger.LogError(ex, "SURT06 send reset password email failed.");
                //        }
                //    }
                //}

                return false;
            }
        }

    }
}
