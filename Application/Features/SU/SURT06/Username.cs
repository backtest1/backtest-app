﻿using Application.Common.Mapping;
using Application.Exceptions;
using Application.Features.SU.Services;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities.DB;
using Domain.Entities.SU;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT06
{
    public class Username
    {
        public class Query : IRequest<Object>
        {
            public string UserType { get; set; }
            public string CompanyCode { get; set; }
            public string EmployeeCode { get; set; }
            public long? StudentId { get; set; }
        }

        public class Handler : IRequestHandler<Query, Object>
        {
            private readonly UserService _service;
            private readonly ICleanDbContext _context;
            public Handler(ICleanDbContext context, UserService service)
            {
                _context = context;
                _service = service;
            }
            public async Task<Object> Handle(Query request, CancellationToken cancellationToken)
            {
                //string username = string.Empty;
                //if (request.UserType == "S")
                //{
                //    var student = await _context.Set<ShStudent>().FirstOrDefaultAsync(o => o.StudentId == request.StudentId, cancellationToken);
                //    username = await _service.GetUsernameNotTaken(true,student.FirstNameEng, student.LastNameEng, student.StudentCode);
                //}
                //else
                //{
                //    var employee = await _context.Set<DbEmployee>().FirstOrDefaultAsync(o => o.CompanyCode == request.CompanyCode && o.EmployeeCode == request.EmployeeCode, cancellationToken);
                //    username = await _service.GetUsernameNotTaken(false,employee.eFirstName, employee.eLastName, employee.EmployeeCode);
                //}
                return new { Username = "" };
            }
        }
    }
}
