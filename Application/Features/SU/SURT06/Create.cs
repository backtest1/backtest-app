﻿using Application.Behaviors;
using Application.Common.Constants;
using Application.Exceptions;
using Application.Features.Services;
using Application.Features.SU.Services;
using Application.Interfaces;
using Application.Interfaces.Email;
using Domain.Entities.DB;
using Domain.Entities.SU;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.SURT06
{
    public class Create
    {
        public class Result
        {
            public long Id { get; set; }
            public bool haveEmail { get; set; }
        }
        public class Command : SuUser, ICommand<Result>
        {
            public long? StudentId { get; set; }
            public string EmployeeCode { get; set; }
            public new string UserType { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result>
        {
            private readonly ICleanDbContext _context;
            private readonly ICurrentUserAccessor _user;
            private readonly IIdentityService _identity;
            private readonly UserService _ad;
            private readonly IEmailSender _email;
            private readonly ILogger<Handler> _logger;
            private string _defaultPassword = "dpu";
            public Handler(ICleanDbContext context, ICurrentUserAccessor user, IIdentityService identity, UserService ad, IEmailSender email, ILogger<Handler> logger)
            {
                _context = context;
                _user = user;
                _identity = identity;
                _email = email;
                _ad = ad;
                _logger = logger;
                _defaultPassword = _context.GetParameterValue<string>("SuSetDefault", "Password").Result;
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var exists = await _context.Set<SuUser>().AnyAsync(o => o.UserName.ToLower() == request.UserName.ToLower(), cancellationToken);

                if (exists)
                {
                    throw new RestException(HttpStatusCode.BadRequest, "message.STD00004", "label.SURT06.Username");
                }

                var studentType = await _context.GetParameterValue<string>("SuUserType", "Student", cancellationToken);
                var visiting = await _context.GetParameterValue<string>("SuUserType", "VisitingProfessor", cancellationToken);
                var saveResult = new Result();
                DbEmployee employee = null;
                //ShStudent student = null;
                string password = string.Empty;
                if (request.UserType == studentType)
                {
                    //student = await _context.Set<ShStudent>().FirstOrDefaultAsync(o => o.StudentId == request.StudentId, cancellationToken);
                    //password = _defaultPassword + student.StudentCode;
                }
                else
                {
                    employee = await _context.Set<DbEmployee>().FirstOrDefaultAsync(o => o.CompanyCode == _user.Company && o.EmployeeCode == request.EmployeeCode, cancellationToken);
                    password = _identity.GeneratePassword();
                }

                request.CreatedBy = _user.UserName;
                request.CreatedDate = DateTime.Now;
                request.CreatedProgram = _user.ProgramCode;
                request.UpdatedBy = _user.UserName;
                request.UpdatedDate = DateTime.Now;
                request.UpdatedProgram = _user.ProgramCode;
                var result = await _identity.CreateUserAsync(request, password);

                var userType = new SuUserType
                {
                    UserId = result.UserId,
                    CompanyCode = _user.Company,
                    UserType = request.UserType
                };
                if (request.UserType == studentType)
                {
                    userType.StudentId = request.StudentId;
                }
                else
                {
                    userType.EmployeeCode = request.EmployeeCode;
                }
                _context.Set<SuUserType>().Add(userType);
                await _context.SaveChangesAsync(cancellationToken);

                var ad = new CreateAd();
                ad.UserId = result.UserId;
                ad.UserName = request.UserName;
                var defaultUsername = await _context.GetParameterValue<string>("SuSetDefault", "Username", cancellationToken);
                if (userType.UserType == studentType)
                {
                    //ad.Id = request.UserName.Replace(defaultUsername, string.Empty);
                    //ad.FirstName = student?.FirstNameTha;
                    //ad.LastName = student?.LastNameTha;
                    //ad.Faculty = "New_Student";
                    //ad.Department = student.RegisterYear.HasValue ? $"Student_{student.RegisterYear.ToString().Substring(2, 2)}" : string.Empty;
                    //ad.CitizenID = student.IdCard?.ToString();
                    //ad.PassportID = student.PassportNo?.ToString();
                    //ad.SecondaryEmail = student?.EmailSecon;
                    //ad.Password = password;
                    //ad.IsStudent = true;
                }
                else
                {
                    ad.Id = request.UserName.Replace(defaultUsername, string.Empty);
                    ad.FirstName = employee?.tFirstName;
                    ad.LastName = employee?.tLastName;
                    ad.Faculty = "Employee";
                    ad.Department = "Staff";
                    ad.CitizenID = employee.PersonalId?.ToString();
                    ad.SecondaryEmail = employee?.EmailSecon;
                    ad.Password = password;
                    ad.IsStudent = false;
                }

                //if (userType.UserType != visiting) await _ad.CreateActiveDirectoryUser(ad, cancellationToken);
                //if (userType.UserType != visiting && request.EndEffectiveDate?.Date == DateTime.Today.Date)
                //{
                //    await _ad.DisableActiveDirectoryUser(null,result.UserId,request.UserName,ad.Id,userType.UserType == studentType,null,cancellationToken);
                //}

                // saveResult.haveEmail = false;
                //var email = userType.UserType == studentType ? student?.EmailSecon : employee?.EmailSecon;
                //var emailTemplate = userType.UserType == studentType ? "SU004" : "SU002";
                //var fullNameTha = userType.UserType == studentType ? student.StudentNameTha : employee.tNameConcat;
                //var fullNameEng = userType.UserType == studentType ? student.StudentNameEng : employee.eNameConcat;
                //if (!string.IsNullOrWhiteSpace(email))
                //{
                //    try
                //    {
                //        var param = new Dictionary<string, string>();
                //        param.Add("[UserName]", request.UserName);
                //        param.Add("[Password]", password);
                //        param.Add("[FullNameTha]", fullNameTha);
                //        param.Add("[FullNameEng]", fullNameEng);
                //        await _email.SendEmailWithTemplateAsysnc(emailTemplate, email, null, param);
                //        saveResult.haveEmail = true;
                //    }
                //    catch (Exception ex)
                //    {
                //        _logger.LogError(ex, "SURT06 send create user email failed.");
                //    }
                //}

                //saveResult.Id = result.UserId;
                //return saveResult;
                return new Result();
            }
        }

    }
}
