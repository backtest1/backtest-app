﻿using Application.Features.SU.Services;
using Application.Interfaces;
using Application.Schedule;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Features.SU.Content
{
    public class DisableUserTask : ScheduledProcessor
    {
        public DisableUserTask(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {

        }

        protected override string Schedule => "0 0 * * *";

        public override async Task ProcessInScope(IServiceProvider serviceProvider, CancellationToken stoppingToken)
        {
            Console.WriteLine("Process start");

            var contentService = serviceProvider.GetRequiredService<UserService>();

            await contentService.DisableUser(stoppingToken);

            Console.WriteLine("Process complete");
        }
    }
}
