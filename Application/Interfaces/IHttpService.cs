﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IHttpService
    {
        Task<T> GetAsync<T>(string requestUrl);
        Task<byte[]> GetAsByteArrayAsync(string requestUrl);
        Task<T> PostAsync<T>(string requestUrl, object bodyContent);
        Task<T> PostAsync<T>(string requestUrl, HttpContent content);
        Task<byte[]> PostAsByteArrayAsync(string requestUrl, object bodyContent);

        Task<dynamic> PostAsAsync(string requestUrl, object bodyContent);
        Task<string> GetAsyncReturnString(string requestUrl);
    }
}
