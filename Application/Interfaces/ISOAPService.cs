﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Application.Interfaces
{
    public interface ISOAPService
    {
        Task<HttpResponseMessage> PostAsync(string requestUrl, string bodyContent);
    }
}
