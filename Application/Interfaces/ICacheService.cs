﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICacheService
    {
        Task SetAsync<T>(string key, T value, TimeSpan expiration = default);
        Task<T> GetAsync<T>(string key);
        Task RemoveAsync(string key);
        Task FlushAsync();
    }
}
