﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Constants
{
    public enum RunningTypeCode
    {
        PetitionNo,
        StudentCode,
        BillNo,
        PricelistCode,
        RegisterNo,
        SystemNo,
        ReceiveNo,
        ReserveNo,        
        TemplateCode,
        EnCode,
        RequestFileNo,
        AgentCode,
        PricelistSubjCode,
        LoadTextDocumentNo,
        StudentLoan,
        LimitCode,
        ItmCode,
        CsRequestNo,
        ExamSeatNumber,
        ReqDocCode,
        CsReserveNo,
        InvoiceNo,
        StudentRunning,
        DiscountCode,
        ApplyNo,
        RgPetitionNo,
        DocRunningNoGrad,
        DocRunningNoNotGrad,
        RequestNo,
        CommCode,
        PvAgentNo,
        GdRatioNo,
        GdModelNo,
        FnPayEmNo,
        DepositCode,
        MatchingCode,
        FnAsTransNo,
        WfCode,
        CommandNo,
        FnVoucherNo,
        FnGLDocNo,
        MatchingCodeAs,
    }
}
