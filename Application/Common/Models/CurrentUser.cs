﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common.Models
{
    public class CurrentUser
    {
        public string CompanyCode { get; set; }
        public string UserName { get; set; }
        public string ProgramCode { get; set; }
    }
}
