﻿using Application.Common.Models;
using Application.Interfaces;
using Domain.Entities.SU;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenseOracle.Identity
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<SuUser> _userManager;

        public IdentityService(UserManager<SuUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<string> GetUserNameAsync(long userId)
        {
            var user = await _userManager.Users.FirstAsync(u => u.Id == userId);

            return user.UserName;
        }
        public async Task<(Result Result, long UserId)> CreateUserAsync(SuUser user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);

            return (result.ToApplicationResult(), user.Id);
        }

        public async Task<Result> DeleteUserAsync(long userId)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

            if (user != null)
            {
                return await DeleteUserAsync(user);
            }

            return Result.Success();
        }

        public async Task<Result> DeleteUserAsync(SuUser user)
        {
            var result = await _userManager.DeleteAsync(user);

            return result.ToApplicationResult();
        }

        public async Task<Result> UpdateUserAsync(SuUser user)
        {
            var result = await _userManager.UpdateSecurityStampAsync(user);
            return result.ToApplicationResult();
        }

        public async Task<Result> ResetPassword(SuUser user, string newPassword)
        {
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, code, newPassword);
            return result.ToApplicationResult();
        }

        public string GeneratePassword()
        {
            var options = _userManager.Options.Password;
            int length = options.RequiredLength;
            string randomPassword = string.Empty;
            int passwordLength = length;

            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            Random random = new Random();
            while (randomPassword.Length != passwordLength)
            {
                int randomNumber = random.Next(valid.Length);  // >= 48 && < 122 

                char c = valid[randomNumber];
                randomPassword += c;
            }
            return randomPassword;
        }
    }
}
