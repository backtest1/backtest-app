﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PersistenseOracle.Configurations;

namespace PersistenseOracle
{
    //public class PropertyBuilder<TProperty> : PropertyBuilder
    //public static NeedExtension<T> DoSomething<T>(this NeedExtension<T> obj)
    public static class PropertyBuilderExtension
    {
        public static PropertyBuilder<T> UseSequence<T>(this PropertyBuilder<T> builder)
        {
            builder.ValueGeneratedOnAdd()
                .HasValueGenerator((p, e) => new SequenceValueGenerator("BACKTEST", $"{e.ClrType.Name.ToUpperNamingConvention(prefix: "WP_")}_{p.Name.ToUpperNamingConvention()}_SEQ"));
            return builder;
        }
    }
}
