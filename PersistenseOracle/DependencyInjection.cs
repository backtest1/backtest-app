﻿using Application.Interfaces;
using Domain.Entities.SU;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PersistenseOracle.Identity;

namespace PersistenseOracle
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistenceOracle(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ICleanDbContext, OracleDbContext>(opt => opt.UseOracle(configuration.GetConnectionString("ODB"), b => b.UseOracleSQLCompatibility("11")), ServiceLifetime.Scoped);
            services.AddScoped<ICleanDbContext>(provider => provider.GetService<OracleDbContext>());
            services.AddIdentityCore<SuUser>().AddEntityFrameworkStores<OracleDbContext>().AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                // Default Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;
            });
            services.AddScoped<IIdentityService, IdentityService>();
            return services;
        }
    }
}
