﻿using Domain.Entities.SU;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.SU
{
    public class EmailTemplateAttachmentConfiguration : BaseOracleConfiguration<SuEmailTemplateAttachment>
    {
        public override void Configure(EntityTypeBuilder<SuEmailTemplateAttachment> builder)
        {
            base.Configure(builder);
            builder.HasKey(e => new { e.EmailTemplateCode,e.AttachmentId });
        }
    }
}
