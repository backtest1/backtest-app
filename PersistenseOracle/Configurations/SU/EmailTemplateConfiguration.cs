﻿using Domain.Entities.SU;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.SU
{
    public class EmailTemplateConfiguration : BaseOracleConfiguration<SuEmailTemplate>
    {
        public override void Configure(EntityTypeBuilder<SuEmailTemplate> builder)
        {
            base.Configure(builder);
            builder.HasKey(e => e.EmailTemplateCode);
            builder.HasMany(e => e.EmailTemplateAttachments).WithOne().HasForeignKey(s => s.EmailTemplateCode);
        }
    }
}
