﻿using Domain.Entities.SU;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.SU
{
    public class UserAdLogConfiguration : BaseOracleConfiguration<SuUserAdLog>
    {
        public override void Configure(EntityTypeBuilder<SuUserAdLog> builder)
        {
            base.Configure(builder);
            builder.HasKey(e => e.LogId);
        }
    }
}
