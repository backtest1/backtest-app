﻿using Domain.Entities.SU;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.SU
{
    public class UserTempConfiguration : BaseOracleConfiguration<SuUserTemp>
    {
        public override void Configure(EntityTypeBuilder<SuUserTemp> builder)
        {
            base.Configure(builder);
            builder.HasKey(e => e.UserId);
        }
    }
}
