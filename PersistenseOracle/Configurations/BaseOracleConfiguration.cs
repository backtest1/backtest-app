﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations
{
    public class BaseOracleConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : EntityBaseOracle
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.Ignore(e => e.Guid);
            builder.Ignore(e => e.RowState);
            builder.Ignore(e => e.RowVersion);
            builder.Property(e => e.CreatedBy).Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;
            builder.Property(e => e.CreatedDate).Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;
            builder.Property(e => e.CreatedProgram).Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;
        }
    }
}
