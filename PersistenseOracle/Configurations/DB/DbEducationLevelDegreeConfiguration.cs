﻿using Domain.Entities.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.DB
{
    public class DbEducationLevelDegreeConfiguration : BaseOracleConfiguration<DbEducationLevelDegree>
    {
        public override void Configure(EntityTypeBuilder<DbEducationLevelDegree> builder)
        {
            base.Configure(builder);
            builder.ToTable("db_education_level_degree");
            builder.HasKey(e => new { e.CompanyCode, e.EducationTypeLevel, e.DegreeId });
        }
    }
}
