﻿using Domain.Entities.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.DB
{
    public class DbRunningNoConfiguration :BaseOracleConfiguration<DbRunningNo>
    {
        public override void Configure(EntityTypeBuilder<DbRunningNo> builder)
        {
            base.Configure(builder);
            builder.HasKey(e => e.Id);
        }
    }
}
