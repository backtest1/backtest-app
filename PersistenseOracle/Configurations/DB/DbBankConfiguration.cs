﻿using Domain.Entities.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.DB
{
    public class DbBankConfiguration : BaseOracleConfiguration<DbBank>

    {
        public override void Configure(EntityTypeBuilder<DbBank> builder)
        {
            base.Configure(builder);
            builder.ToTable("db_bank");
            builder.HasKey(e => e.BankCode);
        }
    }
}
