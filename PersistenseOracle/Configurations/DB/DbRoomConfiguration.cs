﻿using Domain.Entities.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistenseOracle.Configurations.DB
{
    public class DbRoomConfiguration : BaseOracleConfiguration<DbRoom>
    {
        public override void Configure(EntityTypeBuilder<DbRoom> builder)
        {
            base.Configure(builder);
            builder.ToTable("db_room");
            builder.HasKey(e => e.RoomId);
        }
    }
}
