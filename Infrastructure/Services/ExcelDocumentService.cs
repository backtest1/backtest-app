﻿using Application.Interfaces;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using Org.BouncyCastle.Asn1.Cmp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System;
using System.IO;

namespace Infrastructure
{
    public class ExcelDocumentService : IExcelDocumentService
    {
        public async void ReadFileAsync()
        {
            //var dtContent = await FileProcessAsync("C:\\Users\\CMNB-63-002\\Desktop\\ทดสอบ กยศ.xlsx");
            //foreach (DataRow dr in dtContent.Rows)
            //{
            //    foreach(DataColumn column in dr.ItemArray)
            //    {
            //        Console.WriteLine(dr[0]);
            //    }              
            //}
        }

        public async Task<DataTable> FileProcessAsync(IFormFile file, bool hasHeader = false, int hasHeaderRowStart = 1, bool hasFooter = false, int hasFooterEnd = 1)
        {
            DataTable tbl = new DataTable();
            //if (File.Exists(path))
            //{
                string extension = Path.GetExtension(file.FileName).ToLower();
                if(extension.Equals(".xlsx"))
                {
                var filePath = GetFileInfo("ExcelDocument", "en_uat_data.xlsx").FullName;
                Console.WriteLine("Reading column 2 of {0}", filePath);
                Console.WriteLine();
                FileInfo existingFile = new FileInfo(filePath);
                using (var pck = new ExcelPackage(existingFile))
                {
                    //
                    //{
                    //    pck.Load(stream);
                    //}
                    var ws = pck.Workbook.Worksheets.First();

                    foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                    {
                        Console.WriteLine(firstRowCell.Text);
                    }
                    //var startRow = hasHeader ? hasHeaderRowStart : 1;
                    //var endRow = hasFooter ? (ws.Dimension.End.Row - hasFooterEnd) : ws.Dimension.End.Row;
                    //for (int rowNum = startRow; rowNum <= endRow; rowNum++)
                    //{
                    //    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    //    DataRow row = tbl.Rows.Add();
                    //    foreach (var cell in wsRow)
                    //    {
                    //        row[cell.Start.Column - 1] = cell.Text;
                    //    }
                    //}
                }
            }

            //}
            return tbl;
        }

        public DataTable FileProcess(IFormFile file, bool hasHeader = false, int hasHeaderRowStart = 1, bool hasFooter = false, int hasFooterEnd = 1)
        {
            DataTable tbl = new DataTable();
            string extension = Path.GetExtension(file.FileName).ToLower();
            if (extension.Equals(".xlsx"))
            {
                var filePath = GetFileInfo("ExcelDocument", "en_uat_data.xlsx").FullName;
                Console.WriteLine("Reading column 2 of {0}", filePath);
                Console.WriteLine();
                FileInfo existingFile = new FileInfo(filePath);
                using (var pck = new ExcelPackage(existingFile))
                {
                    var ws = pck.Workbook.Worksheets.First();

                    foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                    {
                        Console.WriteLine(firstRowCell.Text);
                    }
                }
            }
            return tbl;
        }

        public static FileInfo GetFileInfo(string directory, string file)
        {
            var rootDir = GetRootDirectory().FullName;
            return new FileInfo(Path.Combine(rootDir, directory, file));
        }

        public static DirectoryInfo GetRootDirectory()
        {
            var currentDir = AppDomain.CurrentDomain.BaseDirectory;
            while (!currentDir.EndsWith("bin"))
            {
                currentDir = Directory.GetParent(currentDir).FullName.TrimEnd('\\');
            }
            return new DirectoryInfo(currentDir).Parent;
        }

        public static DirectoryInfo GetSubDirectory(string directory, string subDirectory)
        {
            var currentDir = GetRootDirectory().FullName;
            return new DirectoryInfo(Path.Combine(currentDir, directory, subDirectory));
        }

        public byte[] FileExcelExport(IEnumerable<dynamic> datas, bool hasHeader = true)
        {
            byte[] fileContents;

            using (var pkg = new OfficeOpenXml.ExcelPackage())
            {
                var worksheet = pkg.Workbook.Worksheets.Add("Sheet1");

                int rowDetail = 1;
                if (hasHeader)
                {
                    int headColumn = 1;
                    foreach (KeyValuePair<string, object> entry in datas.FirstOrDefault())
                    {
                        worksheet.Cells[1, headColumn].Value = entry.Key;
                        headColumn++;
                    }
                    rowDetail++;
                }

                foreach (var detail in datas)
                {
                    int colDetail = 1;
                    foreach (KeyValuePair<string, object> entry in detail)
                    {
                        worksheet.Cells[rowDetail, colDetail].Value = entry.Value;
                        colDetail++;
                    }
                    rowDetail++;
                }
                fileContents = pkg.GetAsByteArray();
            }

            return fileContents;
        }

        public string CheckSum()
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(""))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }
    }
}
