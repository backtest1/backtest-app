﻿using Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Threading;
using System.Net;
using System.Xml.Linq;

namespace Infrastructure.Services
{
    public class SOAPService : ISOAPService
    {
        public HttpClient Client { get; }
        public SOAPService(HttpClient client)
        {
            Client = client;
        }
        public async Task<HttpResponseMessage> PostAsync(string requestUrl, string bodyContent)
        {
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
            var content = new StringContent(bodyContent, Encoding.UTF8, "text/xml");
            var response = await Client.PostAsync(requestUrl, content);
            response.EnsureSuccessStatusCode(); // throws an Exception if 404, 500, etc.
            return response;
        }

       
    }
}
