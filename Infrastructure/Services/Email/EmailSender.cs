﻿using Application.Interfaces.Email;
using Domain.Entities.SU;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Services.Email
{

    public class EmailSender : IEmailSender
    {
        private readonly IEmailSetting _emailSettings;

        public EmailSender(IEmailSetting setting)
        {
            _emailSettings = setting;
        }

        private async Task SendEmailAsync(string email, string subject, string message, IEnumerable<Attachment> attachments=null,IEnumerable<string> attachmentUrls=null)
        {
            try
            {
                var mimeMessage = new MimeMessage();

                mimeMessage.From.Add(new MailboxAddress(_emailSettings.SenderName, _emailSettings.UserName));

                mimeMessage.To.Add(new MailboxAddress(email));

                mimeMessage.Subject = subject;

                var builder = new BodyBuilder { HtmlBody = message };

                if (attachments != null)
                {
                    foreach (var attachment in attachments)
                    {
                        builder.Attachments.Add(attachment.Name, attachment.Content);
                    }
                }
                if (attachmentUrls != null)
                {
                    foreach (var attachment in attachmentUrls)
                    {
                        builder.Attachments.Add(attachment);
                    }
                }

                mimeMessage.Body = builder.ToMessageBody();

                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    //  client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    var security = SecureSocketOptions.StartTlsWhenAvailable;
                    switch (_emailSettings.Security)
                    {
                        case 1:
                            security = SecureSocketOptions.StartTlsWhenAvailable;
                            break;
                        case 2:
                            security = SecureSocketOptions.Auto;
                            break;
                        default:
                            security = SecureSocketOptions.Auto;
                            break;
                    }

                    await client.ConnectAsync(_emailSettings.Host, _emailSettings.Port, security);
                    // Note: only needed if the SMTP server requires authentication
                    await client.AuthenticateAsync(_emailSettings.UserName, _emailSettings.Password);

                    await client.SendAsync(mimeMessage);

                    await client.DisconnectAsync(true);
                }

            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            await this.SendEmailAsync(email, subject, message, null, null);
        }
        public async Task SendEmailAsync(string email, string subject, string message, IEnumerable<string> attachmentUrls)
        {
           await this.SendEmailAsync(email, subject, message, null, attachmentUrls);
        }
        public async Task SendEmailAsync(string email, string subject, string message, IEnumerable<Attachment> attachments)
        {
           await this.SendEmailAsync(email, subject, message, attachments, null);
        }
        public Task SendEmailAsync(string email, string emailCC, string subject, string message)
        {
            throw new NotImplementedException();
        }

        public Task SendEmailAsync(IEnumerable<string> emails, string subject, string message)
        {
            throw new NotImplementedException();
        }

        private async Task SendEmailWithTemplateAsysnc(string templateCode, string email, IReadOnlyDictionary<string, string> headerParam, IReadOnlyDictionary<string, string> bodyParam, IEnumerable<Attachment> attachments = null, IEnumerable<string> attachmentUrls = null)
        {
            SuEmailTemplate template = await _emailSettings.GetTemplate(templateCode);
            if (template == null)
            {
                throw new Exception($"No template code{templateCode}");
            }

            if (headerParam?.Count > 0)
            {
                foreach (var key in headerParam.Keys)
                {
                    template.Subject = template.Subject.Replace(key, headerParam[key]);
                }
            }
            if (bodyParam?.Count > 0)
            {
                foreach (var key in bodyParam.Keys)
                {
                    template.Content = template.Content.Replace(key, bodyParam[key]);
                }
            }

            await this.SendEmailAsync(email, template.Subject, template.Content,attachments,attachmentUrls);
        }
        public async Task SendEmailWithTemplateAsysnc(string templateCode, string email, IReadOnlyDictionary<string, string> headerParam, IReadOnlyDictionary<string, string> bodyParam)
        {
            await this.SendEmailWithTemplateAsysnc(templateCode, email, headerParam, bodyParam, null, null);
        }
        public async Task SendEmailWithTemplateAsysnc(string templateCode, string email, IReadOnlyDictionary<string, string> headerParam, IReadOnlyDictionary<string, string> bodyParam, IEnumerable<Attachment> attachments)
        {
            await this.SendEmailWithTemplateAsysnc(templateCode, email, headerParam, bodyParam, attachments, null);
        }
        public async Task SendEmailWithTemplateAsysnc(string templateCode, string email, IReadOnlyDictionary<string, string> headerParam, IReadOnlyDictionary<string, string> bodyParam, IEnumerable<string> attachmentUrls)
        {
            await this.SendEmailWithTemplateAsysnc(templateCode, email, headerParam, bodyParam, null, attachmentUrls);
        }
    }
}
