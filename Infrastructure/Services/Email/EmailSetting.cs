﻿using Application.Interfaces;
using Application.Interfaces.Email;
using Domain.Entities.SU;
using System.Threading.Tasks;

namespace Infrastructure.Services.Email
{
    public class EmailSetting : IEmailSetting
    {
        public string Host { get; private set; }
        public int Port { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string SenderName { get; private set; }
        public int Security { get; private set; }

        private readonly ICleanDbContext _context;
        public EmailSetting(ICleanDbContext conn)
        {
            _context = conn;
            this.Host = _context.GetParameterValue<string>("Email","Host").Result;
            this.Port = _context.GetParameterValue<int>("Email","Port").Result;
            this.UserName = _context.GetParameterValue<string>("Email","UserName").Result;
            this.Password = _context.GetParameterValue<string>("Email","Password").Result;
            this.SenderName = _context.GetParameterValue<string>("Email","SenderName").Result;
            this.Security = _context.GetParameterValue<int>("Email","Security").Result;
        }

        public async Task<SuEmailTemplate> GetTemplate(string templateCode)
        {
            return await _context.QueryFirstOrDefaultAsync<SuEmailTemplate>("SELECT subject,content FROM su_email_template WHERE email_template_code = @Code", new { Code = templateCode });
        }
    }
}
