﻿using Application.Interfaces;
using Domain.Types;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Infrastructure
{
    public class CurrentUserAccessor : ICurrentUserAccessor
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
      

        public CurrentUserAccessor(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private bool IsInHttpContext()
        {
            return _httpContextAccessor.HttpContext != null;
        }
        public long UserId
        {
            get => IsInHttpContext() ? long.Parse(_httpContextAccessor.HttpContext.User?.FindFirstValue(ClaimTypes.NameIdentifier) ?? "0") : 0;

        }
        public string UserName
        {
            get => IsInHttpContext() ? _httpContextAccessor.HttpContext.User?.FindFirstValue(JwtClaimTypes.Name) : "system";
        }

        private StringValues _company;
        public string Company
        {
            get
            {
                if (IsInHttpContext()) _httpContextAccessor.HttpContext.Request?.Headers.TryGetValue("company", out _company);
                else _company = string.Empty;
                return _company;
            }
        }

        private StringValues _division;
        public string Division
        {
            get
            {
                if (IsInHttpContext()) _httpContextAccessor.HttpContext.Request?.Headers.TryGetValue("division", out _division);
                else _division = string.Empty;
                return _division;
            }
        }


        private StringValues _program;
        public string ProgramCode
        {
            get
            {
                if (IsInHttpContext()) _httpContextAccessor.HttpContext.Request?.Headers.TryGetValue("program", out _program);
                else _program = "system";
                return _program;
            }
        }
        private StringValues _language;
        public string Language {
            get
            {
                if (IsInHttpContext()) _httpContextAccessor.HttpContext.Request?.Headers.TryGetValue("language", out _language);
                else _language = "th";
                return _language;
            }
        }

        Lang _lang;
        public Lang Lang
        {
            get
            {
                Enum.TryParse(_language, out _lang);
                return _lang;
            }
        }
    }
}
